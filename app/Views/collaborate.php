<div class="col-md-12"> 



    <?php require_once(APPPATH . 'Views/layout/header.php');?>





<!-- End Header -->



  <main id="main">



    <!-- ======= Our Services Section ======= -->

    <section class="breadcrumbs">

      <div class="container">



        <div class="d-flex justify-content-between align-items-center">

          <h2>Our Services</h2>

          <ol>

          <li><a href="http://teamapp.in/">Home</a></li>

            <li>Our Services</li>

          </ol>

        </div>



      </div>

    </section><!-- End Our Services Section -->



    <!-- ======= Services Section ======= -->

    <section class="services">

      <div class="container">



        <div class="row">

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up">

            <div class="icon-box icon-box-pink">

              <div class="icon"><i class="bx bxl-dribbble"></i></div>

              <h4 class="title"><a href="">Collaborate</a></h4>

              <p class="description"> In the Team App of the module of collaborate is the process of two or more people or organizations working together to complete a task or achieve a goal. Collaboration is similar to cooperation.  From this module Teams that work collaboratively often access greater resources, recognition and rewards when facing competition for finite resources.
In Team App of the module of “collaborate” consist of three sub module (Support, Chat App and User guide). Each module has different functionality. From all these module users can take support from all members of his team and working together to complete a task or achieve a goal.
</p>

            </div>

          </div>



          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">

            <div class="icon-box icon-box-cyan">

              <div class="icon"><i class="bx bx-file"></i></div>

              <h4 class="title"><a href="">Support</a></h4>

              <p class="description">Support module further consists of sub modules (Raise ticket, IT Development, IT Feedback, User session Log, Team Quality Reports) all these modules have different functionality.</p>

            </div>

          </div>



          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">

            <div class="icon-box icon-box-green">

              <div class="icon"><i class="bx bx-tachometer"></i></div>

              <h4 class="title"><a href="">Raise Ticket</a></h4>

              <p class="description">with the “Raise Ticket” module user can take help/support from all members of his Team. In another words we can say it’s a support form. For using this module user have required to fill some field – select user name in the field of “User”, select Support type from the drop down (IT Web support, Account Support, Admin Support, HR Support and Mobile App Support), select function from the drop down, write Summary related to his issues on the write Box, write Description on the “Description box”, write Expected Deliverable on the write Box, choose files on the Attachment and finally click on submit button. When user successfully raise ticket then his/her issue will display on the Bug list</p>

            </div>

          </div>



          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">

            <div class="icon-box icon-box-blue">

              <div class="icon"><i class="bx bx-world"></i></div>

              <h4 class="title"><a href="">IT Development </a></h4>

              <p class="description">When user click on IT Development module then user can find all the list of assign Bug of the whole team of members and from this module user can Assign new Bug from the “Assign New Bug” button. Some fields have required to Assign the new Bug like – By selecting project type, function, categories, Reproducibility, Severity, Urgency, Important and select all another required fields finally click on “Submit issue” button. When submit issue button will successfully submit then Assigned issue will display on the “Bug List” page. If Assign issue/bug successfully fixed from the side of developer, then user have right to change the status of the Assign Bug</p>

            </div>

          </div>



        </div>



      </div>

    </section><!-- End Services Section -->



    <!-- ======= Why Us Section ======= -->

    <section class="why-us section-bg" data-aos="fade-up" date-aos-delay="200">

      <div class="container">



        <div class="row">

          <div class="col-lg-6 video-box">

            <img src="public/img/why-us.jpg" class="img-fluid" alt="">

            <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>

          </div>



          <div class="col-lg-6 d-flex flex-column justify-content-center p-5">



            <div class="icon-box">

              <div class="icon"><i class="bx bx-fingerprint"></i></div>

              <h4 class="title"><a href="">Lorem Ipsum</a></h4>

              <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>

            </div>



            <div class="icon-box">

              <div class="icon"><i class="bx bx-gift"></i></div>

              <h4 class="title"><a href="">Nemo Enim</a></h4>

              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>

            </div>



          </div>

        </div>



      </div>

    </section><!-- End Why Us Section -->



    <!-- ======= Service Details Section ======= -->

    <section class="service-details">
      <div class="container">
        <div class="row">
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-1.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">WHY CHOOSE US</a></h5>
                <p class="card-text">There is many resons  to choose team app because there is very type of facility provide in team app for manage a team in an origination like attendance ,daily report as timesheet , employee salary slip ,bank statement ,all project on which he working and also there showing offer letter which is offer to employee and company policy and which type of benefits offer company to employee  everything is transparent in team app . And team app is developed by high skills and well knowledge employee.</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-2.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">WHO WE ARE</a></h5>
                <p class="card-text">We have high skill developer who have excellent knowledge of PHP, larval, java, css etc programming languages and already we have done many project of our client which are web application ,web application and websites and also we have received  good feedback from our client side for our work and products which we have deliver to our clients."</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-3.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">Our Mission</a></h5>
                <p class="card-text">Our mission is to develop  a team in which we have high skills more experience employee and who can develop  high quality software, mobile app, website and logo etc. with maximum functionality and security with thin time in estimated cost .</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-4.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">Our Vission</a></h5>
                <p class="card-text">Our vision we more popular in whole country with high rated for our work and what facility we provide to our client and user via our software developing team and develop maximum to maximum software and website in very year.</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
        </div>    
      </div>

    </section>



    <!-- ======= Pricing Section ======= -->

    

  </main><!-- End #main -->







<?php  require_once(APPPATH . 'Views/layout/footer.php'); ?>

