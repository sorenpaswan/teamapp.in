<div class="col-md-12"> 



    <?php require_once(APPPATH . 'Views/layout/header.php');?>





<!-- End Header -->



  <main id="main">



    <!-- ======= Our Services Section ======= -->

    <section class="breadcrumbs">

      <div class="container">



        <div class="d-flex justify-content-between align-items-center">

          <h2>Our Services</h2>

          <ol>

          <li><a href="http://teamapp.in/">Home</a></li>
            <li>Our Services</li>

          </ol>

        </div>



      </div>

    </section><!-- End Our Services Section -->



    <!-- ======= Services Section ======= -->

    <section class="services">

      <div class="container">



        <div class="row">

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up">

            <div class="icon-box icon-box-pink">

              <div class="icon"><i class="bx bxl-dribbble"></i></div>

              <h4 class="title"><a href="">Travel</a></h4>

              <p class="description">With the help of Team App User can check his/her Travel or Trip details by the click on My Trip module. User can filter his/her data as per the date, as per the status or as per the particular keyword. All functionalities that are related to trip Like- view Trip, create booking, boking request, create Trip, create TEC, delete Trip done with the help of Action button.
User can create Travel or Trip with the help Add module. After creating the travel & Trip, your travel & Trip will appear on the list View of My Trips and user can create Travel & Trips with the help of Action Button and user can Edit Both Travel & Trips from the action button. 
Travel related data will be pre-filled in the form. User can edit some of the pre-filled data and rest of the data we have to fill by your own. Then save to create trip.
The functionality of Team trip is for Admin/HR/Project or Reporting Manager. With the help of Team Trip module admin/HR/Project or Reporting Manager can view all employee trips on the Team Trip module and Authorize (Admin/HR/Project or Reporting Manager) person can Create Trip, Create Booking and Delete Trip for the Team.
With the help of My Booking Module user can check his/her Booking details with the My Booking Module. User can also filter his/her data as per the data, as per the status or as per the particular keyword. User can view Booking, Edit Booking, Edit Payment, Add Payment and Create TEC with the Action Button. 
User can Create Self-Booking against the trip with the My booking module and with the action button. Created Booked will appear on the page of “My Booking” Page. If user want the create booking then user has to fill the detail of the Booking like booking of (Hotel/Lodge, Train, Bus, Flight) and then fill rest of the details as per the selected option. And fill the details of Payment on the Click of Add Payment Option and then fill the Payment Details of the Booking like Amount, date and other mandatory Details.
User can check his/her TEC Details in a List View on the My TEC module and user can filter his/her TEC data as per the date, as per the status or as per the particular keyword. When user want to create TEC then user will go to My Trip page and click on Action Button select “Create TEC” fill the form of “CREATE TEC REQUEST” when user successfully create TEC Request then user have to fill the TEC form when user successfully fill TEC form and click on save button then TEC Request successfully create and user can able to Edit again the Form details.
If user wants to see TEC then click on My TEC module, click on Action Button, select view option on the action button. After click on view option- User can view the TEC in details. If user want to edit TEC then user click on My TEC. User can edit the TEC by click on action button, selecting View option or by selecting Fill TEC option from the Action Button or select edit option Edits TEC Entries. 
With the help of Team App of the module of “Travel” is also called as Tourism Technology which is System of information Travel technology or hospitality Technology in the travel, tourism industry to manage information of accommodation or travel activity of hotel booking, tour booking, flight booking and brand visibility for travel agent to increase booking and boost sales with best customer experience. 
Travel Technology is used to describe of accommodation management, booking management, price and reviewing travel experience.</p>

            </div>

          </div>



          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">

            <div class="icon-box icon-box-cyan">

              <div class="icon"><i class="bx bx-file"></i></div>

              <h4 class="title"><a href="">Sed ut perspiciatis</a></h4>

              <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>

            </div>

          </div>



          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">

            <div class="icon-box icon-box-green">

              <div class="icon"><i class="bx bx-tachometer"></i></div>

              <h4 class="title"><a href="">Magni Dolores</a></h4>

              <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>

            </div>

          </div>



          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">

            <div class="icon-box icon-box-blue">

              <div class="icon"><i class="bx bx-world"></i></div>

              <h4 class="title"><a href="">Nemo Enim</a></h4>

              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>

            </div>

          </div>



        </div>



      </div>

    </section><!-- End Services Section -->



    <!-- ======= Why Us Section ======= -->

    <!-- <section class="why-us section-bg" data-aos="fade-up" date-aos-delay="200">

      <div class="container">



        <div class="row">

          <div class="col-lg-6 video-box">

            <img src="public/img/why-us.jpg" class="img-fluid" alt="">

            <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>

          </div>



          <div class="col-lg-6 d-flex flex-column justify-content-center p-5">



            <div class="icon-box">

              <div class="icon"><i class="bx bx-fingerprint"></i></div>

              <h4 class="title"><a href="">Lorem Ipsum</a></h4>

              <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>

            </div>



            <div class="icon-box">

              <div class="icon"><i class="bx bx-gift"></i></div>

              <h4 class="title"><a href="">Nemo Enim</a></h4>

              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>

            </div>



          </div>

        </div>



      </div>

    </section>   End Why Us Section -->



    <!-- ======= Service Details Section ======= -->

    <section class="service-details">

      <div class="container">



        <div class="row">

          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">

            <div class="card">

              <div class="card-img">

                <img src="public/img/service-details-1.jpg" alt="...">

              </div>

              <div class="card-body">

                <h5 class="card-title"><a href="#">Our Mission</a></h5>

                <p class="card-text">Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>

                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>

              </div>

            </div>

          </div>

          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">

            <div class="card">

              <div class="card-img">

                <img src="public/img/service-details-2.jpg" alt="...">

              </div>

              <div class="card-body">

                <h5 class="card-title"><a href="#">Our Plan</a></h5>

                <p class="card-text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo</p>

                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>

              </div>

            </div>



          </div>

          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">

            <div class="card">

              <div class="card-img">

                <img src="public/img/service-details-3.jpg" alt="...">

              </div>

              <div class="card-body">

                <h5 class="card-title"><a href="#">Our Vision</a></h5>

                <p class="card-text">Nemo enim ipsam voluptatem quia voluptas sit aut odit aut fugit, sed quia magni dolores eos qui ratione voluptatem sequi nesciunt Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet</p>

                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>

              </div>

            </div>

          </div>

          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">

            <div class="card">

              <div class="card-img">

                <img src="public/img/service-details-4.jpg" alt="...">

              </div>

              <div class="card-body">

                <h5 class="card-title"><a href="#">Our Care</a></h5>

                <p class="card-text">Nostrum eum sed et autem dolorum perspiciatis. Magni porro quisquam laudantium voluptatem. In molestiae earum ab sit esse voluptatem. Eos ipsam cumque ipsum officiis qui nihil aut incidunt aut</p>

                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>

              </div>

            </div>

          </div>

        </div>

        

      </div>

    </section><!-- End Service Details Section -->



    <!-- ======= Pricing Section ======= -->

    

  </main><!-- End #main -->







<?php  require_once(APPPATH . 'Views/layout/footer.php'); ?>

