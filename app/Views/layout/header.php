<html lang="en">


<head>

  <meta charset="utf-8">

  <meta content="width=device-width, initial-scale=1.0" name="viewport">



  <title>TeamApp</title>

  <meta content="" name="descriptison">

  <meta content="" name="keywords">



  <!-- Favicons -->

  <link href="public/img/favicon.png" rel="icon">

  <link href="public/img/apple-touch-icon.png" rel="apple-touch-icon">



  <!-- Google Fonts -->

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,700,700i&display=swap" rel="stylesheet">



  <!-- Vendor CSS Files -->

  <link href="public/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <link href="public/vendor/animate.css/animate.min.css" rel="stylesheet">

  <link href="public/vendor/icofont/icofont.min.css" rel="stylesheet">

  <link href="public/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">

  <link href="public/vendor/venobox/venobox.css" rel="stylesheet">

  <link href="public/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <link href="public/vendor/aos/aos.css" rel="stylesheet">



  <!-- Template Main CSS File -->

  <link href="public/css/style.css" rel="stylesheet">



  <!-- =======================================================

  * Template Name: Moderna - v2.1.0

  * Template URL: https://bootstrapmade.com/free-bootstrap-template-corporate-moderna/

  * Author: BootstrapMade.com

  * License: https://bootstrapmade.com/license/

  ======================================================== -->

</head>



<body>



  <!-- ======= Header ======= -->

 <header id="header" class="fixed-top ">

    <div class="container">



      <div class="logo float-left">

        <h1 class="text-light"><a href="index.html"><span>TEAMAPP</span></a></h1>

        <!-- Uncomment below if you prefer to use an image logo -->

        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      </div>



      <nav class="nav-menu float-right d-none d-lg-block">

        <ul>

          <li><a href="home">Home</a></li>
          <li><a href="about">About Us</a></li>
          <li class="drop-down"><a href="#">Services</a>
            <ul>
               <li><a href="employee-self-service">Employee Self Service</a></li>
               <li><a href="performance">Performance</a></li>   
              <li class="drop-down"><a href="#">Attendance</a>
                <ul>
                  <li><a href="attendance-attendance">Attendance</a></li>
                  <li><a href="attendance-leave">Leave</a></li>
                 <li><a href="timesheet">Timesheet</a></li>
                </ul>
              </li>
               <li class="drop-down"><a href="#">Project And Activity</a>
                <ul>
                  <li><a href="project_and_activity-project">Project</a></li>
                  <li><a href="project_and_activity-activity">Activity</a></li>
                  <li><a href="project_and_activity-task">Task</a></li>
                </ul>
              </li>
              <li class="drop-down"><a href="#">Travel</a>
                <ul>
                  <li><a href="travel-trips">Trips</a></li>
                  <li><a href="travel-bookings">Bookings</a></li>
                </ul>
              </li>
              <li><a href="salary-and-benefits">Salary & Benefits</a></li>
                 <li class="drop-down"><a href="#"> Expenses</a>
                <ul>
                  <li><a href="expenses-monthly-payroll">Monthly Payroll</a></li>
                  <li><a href="expenses-project-expenses">Project Expenses</a></li>
                  <li><a href="expenses-contract-freelancer-expenses">Contract/FreelancerExpenses</a></li>
                  <li><a href="expenses-booking-payments">Booking Payments</a></li>
                  <li><a href="expenses-purchases">Purchases </a></li>
                  <li><a href="expenses-marketing-and-sales">Marketing & Sales</a></li>
                  <li><a href="expenses-vendors">Vendors</a></li>
                </ul>
              </li>
              <li class="drop-down"><a href="#">Human Capital Mgmt</a>
                <ul>
                  <li><a href="hcm-talent-recruitment">Talent Recruitment</a></li>
                  <li><a href="hcm-managing-payroll">Managing Payroll</a></li>
                  <li><a href="hcm-feedbacks">Feedbacks</a></li>
                </ul>
              </li>

                 <li class="drop-down"><a href="#">CRM</a>
                <ul>
                  <li><a href="crm-targets-and-sales">Targets & Sales</a></li>
                  <li><a href="crm-customer-accounts">Customer Accounts</a></li>
                  <li><a href="about">Opportunities</a></li>
                  <li><a href="crm-prospects">Prospects</a></li>
                  <li><a href="about">Leads</a></li>
                  <li><a href="about">Cold calls</a></li> 
                  <li><a href="crm-campaigns">Campaigns</a></li>  
                  <li><a href="about">Offers catalogue</a></li>            
                </ul>
              </li>
              <li class="drop-down"><a href="#">Collaborate</a>
                <ul>
                  <li><a href="collaborate-support">Support</a></li>
                  <li><a href="collaborate-team-chat">Team Chat</a></li>
                   <li><a href="collaborate-user-guide">User Guide</a></li>
                  <li><a href="collaborate-files">Files</a></li>               
                </ul>
              </li>
              <li class="drop-down"><a href="#">Revenue</a>
                <ul>
                  <li><a href="revenue-invoices">Invoices</a></li>
                  <li><a href="revenue-payments-received">Payments Received</a></li>
                   <li><a href="revenue-billable-expenses">Billable Expenses</a></li>
                  <li><a href="revenue-monthly-billable-expenses">Monthly Billable Expenses</a></li>               
                </ul>
              </li>
               <li class="drop-down"><a href="#">Books</a>
                <ul>
                  <li><a href="books-payment-made">Payment Made</a></li>
                  <li><a href="books-payment-match">Payment Match</a></li>
                   <li><a href="books-assets">Assets</a></li>
                  <li><a href="books-match-invoice">Match Invoice</a></li>
                  <li><a href="books-team-statement-soa">Team Statement SoA</a></li> 
                  <li><a href="books-petty-cash">Petty Cash</a></li>                
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="contact.html">Contact Us</a></li>
        </ul>

      </nav><!-- .nav-menu -->



    </div>

  </header><!-- End Header -->