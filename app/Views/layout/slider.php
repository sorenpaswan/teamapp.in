    <link rel="stylesheet" href="public/css/owl.carousel.min.css">
    <script src="public/js/jquery.min.js"></script>
    <script src="public/js/owl.carousel.js"></script>

    <section id="demos">
      <div class="container slid_set">
      <div class="section-title">
          <h2> Features of TeamApp</h2>
      </div>
        <div class="row">
          <div class="col-md-12">
            <div class="owl-carousel owl-theme">
              <div class="item">
                <h1>Employee self service</h1>
                <p>In Team App Employee Self Service module is a self-service portal for every employee of the company and its a part of the software that all of your employees .</p>
              </div>
  
              <div class="item">
                <h1>To Do List</h1>
                <p>First click on main module of Employee Self Services then click on “To Do List” module with the To Do List user can view his/her approve task when user add new task on the daily basis.</p>
              </div>
  
              <div class="item">
                <h1>Performance</h1>
                <p>The accomplishment of a given task measured against preset known standards of accuracy, completeness, cost, and speed. In a contract, performance is deemed to be the fulfillment of an obligation.</p>
              </div>
  
              <div class="item">
                <h1>Attendance</h1>
                <p>Team App Attendance module is designed for all Employees of the organisation to be able punch in & punch out their own attendance and for Employees to be able to view their own attendance record.</p>
              </div>
  
              <div class="item">
                <h1>MY Geo Attendance</h1>
                <p>In the module of “My geo Attendance” User can easily see our own Attendance status. And the location of Employee during Punch in & Punch out time is successfully done then the location of Employee.</p>
              </div>
  
              <div class="item">
                <h1>Manual Attendance</h1>
                <p>If the user forgets to make his attendance, then with the help of Team App of the “Manual Attendance module we can easily make our Manual Attendance and we have only 3 manual attendance allowed per month.</p>
              </div>
  
              <div class="item">
                <h1>Approve Attendance</h1>
                <p>When 3 Manual Attendance are completed in a one month and if user want to make more manual Attendance then our manual attendance goes to HR/Admin/Project manager to Approve first. .</p>
              </div>
  
              <div class="item">
                <h1>Approve Location</h1>
                <p>This Functionality is for Admin/HR/Project or Reporting Manager. Whenever an employee sent Location Request for approval, Request will display in Approve location. Authorize Person has to approve that location.</p>
              </div>
  
              <div class="item">
                <h1>Leaves</h1>
                <p>With the help of Team App of the module of “Leaves” user can able to apply his/her Leave. when user apply his/her Leave Then Leaves goes for Approval to the HR/ Admin/Project manager.</p>
              </div>
  
              <div class="item">
                <h1>My Leaves</h1>
                <p>User can check his/her Past Leaves details by click on “MY Leaves” module. User can filter his/her leave data as per the date, as per the status or as per the Particular keywords.All The Approve Leaves from the HR/ Admin/Project manager is successfully showing .</p>
              </div>
  
              <div class="item">
                <h1>Leaves Apply</h1>
                <p>User can directly apply leave by using “Leave Apply” module when user click on “Leaves Apply” Module then then Page of “Leaves Apply” opens then user required to fill Leave type like Paid Leave or any other from three listed in the dropdown,  .</p>
              </div>
  
              <div class="item">
                <h1>Team Leaves</h1>
                <p>This functionality is for HR/Admin/Project or Reporting Manager. Records of the leave Applied by Employees with the status of Approved & Rejected in the details will showing on the page of “Team Leaves” Page.</p>
              </div>
  
              <div class="item">
                <h1>Assign Leaves</h1>
                <p>This functionality is for HR/Admin/Project or Reporting Manager. If HR/Admin/Project or Reporting Manager want to assign Leaves to his/her employee, then HR/Admin/Project or Reporting Manager can access directly Assign Leave to his/her employee.</p>
              </div>
  
              <div class="item">
                <h1>Timesheet</h1>
                <p>The Team App of the “Timesheet” module is a data table in which all the records about Working hours of the employee are showing on the Page of “My Timesheet”. User can see his/her Log Hours (duration) as per “Yearly view”,.</p>
              </div>
              
              <div class="item">
                <h1>My Timesheet</h1>
                <p>With the help My Timesheet module user can check his/her Timesheet details by the click on Timesheet. User can see his/her Log Hours (duration) as per “Yearly view”, “Monthly view” or “Detailed view” and User can filter his/her data as per the date or as per the particular keyword.</p>
              </div>
  
              <div class="item">
                <h1>Add timesheet</h1>
                <p>To update the timesheet as per today then user, will have to required Add timesheet first. when user update his/her timesheet then user can easily see his/her log/working hours on the page of My Timesheet.So, user has to update hours in the Task first.</p>
              </div>
  
              <div class="item">
                <h1>My Project</h1>
                <p>User can check his/her assigned project details by the click on My Projects module. and user can able to check all details about “Active Projects”, “Inactive Projects”, “Unapproved Projects” when user click on My Projects.</p>
              </div>
  
              <div class="item">
                <h1>Add Project</h1>
                <p>When user click on Add Project module then user should be required to fill the details like select “Project Type” on the field of project type, choose “Client name” on the field of client name and finally enter customer name and click on search button.</p>
              </div>
              
              <div class="item">
                <h1>Team Projects</h1>
                <p>When User click on Team Project module then all the details about “Team Project” are showing on the page of “Team Project”and user can able to check all details about “Active Projects”,.</p>
              </div>
              
              <div class="item">
                <h1>Assign Project</h1>
                <p>This functionality is for Admin/HR/Project or Reporting Manager. Functionality used to assign the project to the employee as per the project type.</p>
              </div>
  
              <div class="item">
                <h1>My Activities</h1>
                <p>User can check his/her Activities details by the click on My Activities and all the Activities that are assigned to his/her will be showing on the page of My Activities.</p>
              </div>
  
              <div class="item">
                <h1>Add Activities</h1>
                <p>With the help of “Add Activities” module User can add new activities. Fill the form to add a new activity like select “project” on the field of project select “Activity.</p>
              </div>
  
              <div class="item">
                <h1>Assign Activities</h1>
                <p>This functionality is for Admin/HR/Project or Reporting Manager. Functionality used to assign the Activities to the employee. Fill the Form to assign the project Select “Project Type”.</p>
              </div>
  
              <div class="item">
                <h1>Task</h1>
                <p>Team App easily manages their employees task created by employee through our centralized Task system.Team App lets manage all task created by user through modern technologies like mobile and web for project.</p>
              </div>
              <div class="item">
                <h1>My Task</h1>
                <p>When user successfully add task if Project manager/Team lead gives approval from own side then user will be able to see his/her approved task on the page of “My task” as well as “To Do List”.</p>
              </div>
  
              <div class="item">
                <h1>Add Task</h1>
                <p>When user add the task on the daily basis “Add task” goes to project manager/Team lead for approval. when project manager/Team lead gives approval.</p>
              </div>
  
              <div class="item">
                <h1>My Trip</h1>
                <p>With the help of Team App User can check his/her Trip details by the click on My Trip module. User can filter his/her data as per the date, as per the status or as per the particular keyword.</p>
              </div>
  
              <div class="item">
                <h1>Add Travel & Trip</h1>
                <p>When user click on Add Travel module then create Travel form open and user have to require fill all details on the fields like- select the project Type, project name, from location.</p>
              </div>
  
              <div class="item">
                <h1>Team Trip</h1>
                <p>The functionality of Team trip is for Admin/HR/Project or Reporting Manager. With the help of Team Trip module admin/HR/Project or Reporting Manager can view all employee.</p>
              </div>
  
              <div class="item">
                <h1>Team Booking</h1>
                <p>   In team booking module of team app HR/Admin or Project manager check all the booking request done by site engineers for hotel/lodge, train and flight etc. on the basis of site location of engineers.</p>
              </div>
  
              <div class="item">
                <h1>Booking payment</h1>
                <p>In side Booking payment module add payment and vendor payment and team payment request from add payment sub module user request for his all own booking.</p>
              </div>
  
              <div class="item">
                <h1>Add payment</h1>
                <p>In Add payment sub module of Booking payment of team app when site engineers self payment for all booking like Train ,flight and hotel Loading for site project.</p>
              </div>
  
              <div class="item">
                <h1>Salary & Benefits</h1>
                <p>In the Team app of the module of the “Salary & Benefits” from this module user can check his/her salary status and check all details about salary slip.</p>
              </div>
              
              <div class="item">
                <h1>My salary slip</h1>
                <p>With the help of My salary slip module user can check his/her salary slip by click on My salary slip module and user can check his/her salary according to month wise.</p>
              </div>
  
              <div class="item">
                <p>From the Module of “My TEC” user can check his/her TEC. User can filter his/her TEC for filter TEC user required to select some fields – select claim start date on the field of “Claim start date”,</p>
              </div>
  
              <div class="item">
                <h1>Team TEC</h1>
                <P>This functionality is for Admin/HR/Project or Reporting Manager. Form here Admin/HR/Project or Reporting Manager can check his/her employee</P>
              </div>
  
              <div class="item">
                <h1>Contract Freelancer Expenses</h1>
                <p>A freelance contract is a document that explains the relationship between a contractor and the company that is hiring them. It sets clear expectations between the two parties for a specific project over a set period of time. </p>
              </div>
  
              <div class="item">
                <h1>Booking Payments</h1>
                <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
              </div>
  
              <div class="item">
                <h1>  Vendor Payments</h1>
                <p>The process of paying vendors is one of the final steps in the Purchase to Pay cycle. Briefly, when a company orders goods from a supplier</p>
              </div>
  
              <div class="item">
                <h1>Add Payments</h1>
                <p>With the help of Team App from the module of “Add Payments” user can Add his/her Payments For doing this user will require to fill some field – select Booking ID</p>
              </div>
  
              <div class="item">
                <h1>Purchases</h1>
                <p>In team App from the module of “Purchases” A purchase is a routinely operation carried by both individuals and corporations. The purpose of this financial transaction is to transfer the ownership of a piece of property physical, intellectual, virtual or else.</p>
              </div>
  
              <div class="item">
                <h1>Marketing-and-sales</h1>
                <p>Marketing and sales differ greatly, but they generally have the same goal. Selling is the final stage in marketing which puts the plan into effect. A marketing plan includes pricing, promotion, place, and product (the 4 P's).</p>
              </div>
  
              <div class="item">
                <h1>Add customer</h1>
                <p>With the help of an Add customer module user can Add “customer”. If the User wants to Add customer then user will require</p>
              </div>
  
              <div class="item">
                 <h1>Pending customer</h1>
                 <p>When user click on “Pending customer” module then the entire Pending caller customer List will be showing on the page of “Pending contact List”. </p>
              </div>
  
              <div class="item">
                <h1>Approve customer</h1>
                <p>When user click on “Approve customer” module then all the approve caller customer list will be showing on the page of “Approve customer”.</p>
              </div>
  
              <div class="item">
                <h1>Support</h1>
                <p>Team App easily manages their employees Issues face by our users during use our app through our centralized Support (Raise Ticket) system.</p>
              </div>
  
              <div class="item">
                <h1>Team chat</h1>
                <p>Team App easily coordinate with their employees through our centralized Team chat system.Team App coordinate with their employees through modern technologies</p>
              </div>
  
              <div class="item">
                <h1>User guide</h1>
                <p>Team App easily guide to our employee to use our application through our centralized User Guide system.Team app Guide our employees through modern technologies </p>
              </div>
  
              <div class="item">
                <h1>Organization Files</h1>
                <p>Organization files are common files that are shared throughout the organization based on organization entities and locations.Organization files might include your employee handbook or policy documents.</p>
              </div>
  
              <div class="item">
                <h1>Employee Files</h1>
                <p>An employee file, or personnel record, is a group of documents that contain all relevant information about an employee’s time in your business, from their job application to their resignation letter.</p>
              </div>
  
              <div class="item">
                <h1>Monthly Invoice</h1>
                <p>Monthly invoicing is a payment method for ad costs that is available to qualified businesses. With monthly invoicing, you still create and run ads like you normally do</p>
              </div>
  
              <div class="item">
                <h1>Monthly Invoice TEC</h1>
                <p>The ability to create and send invoices is the most basic and important feature of billing software. This might be simple or quite complex, depending on the business needs. </p>
              </div>
  
              <div class="item">
                <h1>Upload Payment Received</h1>
                <p>Any payment that you receive from a customer can be recorded and applied across their various outstanding invoices. You can also view the payments recorded for the invoices</p>
              </div>
  
              <div class="item">
                <h1>Billable expanse</h1>
                <p>A billable expense is basically an expense that you have incurred on behalf of your client customer for performing work, services, supplies.</p>
              </div>
  
              <div class="item">
                <h1>Monthly billable expanse</h1>
                <p>Monthly invoice billing is a payment option where Microsoft Advertising extends you a credit line to accrue advertising charges. You then receive a monthly invoice and pay based on the terms and conditions of your contract. </p>
              </div>
  
              <div class="item">
                <h1>Upload Payments made</h1>
                <p>With the help of an “Upload Payments Made” module user can Upload Csv when user clicking on choose file and click on submit button.</p>
              </div>
  
              <div class="item">
                <h1>Match Invoice</h1>
                <p>Accounts payable invoice matching is the process of matching vendor invoice, purchase order, and product receipt information.</p>
              </div>
  
              <div class="item">
                <h1>Asset register</h1>
                <p>In assets register sub module of asset module Admin register all assets that is Purchase Company for office work or to give siteengineers to work on site project Like Laptop,</p>
              </div>
  
              <div class="item">
                <h1>Asset allocation</h1>
                <p>In assets allocation sub module of asset module admin assign asset to site engineers or office employee By selection employee name , date and assure ass specification and whenever site engineers</p>
              </div>
  
              <div class="item">
                <h1>Team quality report</h1>
                <p>From team quality report Team lead/Project manager can check every employee’s as per date all task assign to them and resolved by them. That is show in productivity of that employee</p>
              </div>
  
              <div class="item">
                <h1>Statement of account </h1>
                <p>In My statement of account sub module of books module User can check all the trisection history as per date and amount from Technitab company account to user salary account. </p>
              </div>
  
              <div class="item">
                <h1>   Vendor payment </h1>
                <p>In vendor payment sub module of booking payment module of team app show details of all the payment done to vendor of different – different type of booking like for train IRCTC ,for flight  Goibibo app and for hotel Krishna hotel etc.</p>
              </div>
  
            </div>
          </div>
        </div>
      </div>
    </section>

    <script>
      $(document).ready(function() {
        var owl = $('.owl-carousel');
        owl.owlCarousel({
          items: 4,
          loop: true,
          margin: 20,
          autoplay: true,
          autoplayTimeout: 1000,
          autoplayHoverPause: true
        });
        $('.play').on('click', function() {
          owl.trigger('play.owl.autoplay', [1000])
        })
        $('.stop').on('click', function() {
          owl.trigger('stop.owl.autoplay')
        })
      })


        $('.owl-carousel').owlCarousel({
      loop:true,
      margin:20,
      autoplay: true,
      autoplayTimeout: 200000,
      autoplayHoverPause: true,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
              nav:false
              
          },
          520:{
              items:2,
              nav:false
          },
          768:{
              items:3,
              nav:false
          },
          1024:{
              items:4,
              nav:false
          },
        
      }

     }) 
    </script> 
