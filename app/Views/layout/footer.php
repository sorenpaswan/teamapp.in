<footer id="footer" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">



     <div class="footer-top">

      <div class="container">

        <div class="row">



          <div class="col-lg-3 col-md-6 footer-links">

            <h4>Useful Links</h4>

            <ul>

              <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>

              <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>

              <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>

              <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>

              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>

            </ul>

          </div>



          <div class="col-lg-3 col-md-6 footer-links">

            <h4>Our Services</h4>

            <ul>

              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>

              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>

              <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>

              <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>

              <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>

            </ul>

          </div>



          <div class="col-lg-3 col-md-6 footer-contact">

            <h4>Contact Us</h4>

            <p>
       
            Alpha 1 Commercial Belt Near ICICI Bank <br>

            Greater Noida, 201310<br>

            Uttar Pradesh <br><br>

              <strong>Phone:</strong> +91 88005 35588<br>

              <strong>Email:</strong>kavinder.kohli@technitab.com<br>

            </p>



          </div>



          <div class="col-lg-3 col-md-6 footer-info">
            <h3>About TeamApp</h3>
            <p>Team App is a mobile and web app in which a company manage all information of Employees.</p>

            <div class="social-links mt-3">

              <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>

              <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>

              <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>

              <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>

            </div>

          </div>



        </div>

      </div>

    </div>



    <div class="container">

      <div class="copyright">

        &copy; Copyright <strong><span>TeamApp</span></strong>. All Rights Reserved

      </div>

      

    </div>

  </footer><!-- End Footer -->



  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>



  <!-- Vendor JS Files -->

  <script src="public/vendor/jquery/jquery.min.js"></script>

  <script src="public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script src="public/vendor/jquery.easing/jquery.easing.min.js"></script>

  <script src="public/vendor/php-email-form/validate.js"></script>

  <script src="public/vendor/venobox/venobox.min.js"></script>

  <script src="public/vendor/waypoints/jquery.waypoints.min.js"></script>

  <script src="public/vendor/counterup/counterup.min.js"></script>

  <script src="public/vendor/owl.carousel/owl.carousel.min.js"></script>

  <script src="public/vendor/isotope-layout/isotope.pkgd.min.js"></script>

  <script src="public/vendor/aos/aos.js"></script>



  <!-- Template Main JS File -->

  <script src="public/js/main.js"></script>
    <link rel="stylesheet" href="public/assets/owl.carousel.min.css">
    <script src="public/assets/jquery.min.js"></script>
    <script src="public/assets/owl.carousel.js"></script>


</body>



</html>