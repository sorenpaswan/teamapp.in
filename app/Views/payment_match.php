<div class="col-md-12"> 



    <?php require_once(APPPATH . 'Views/layout/header.php');?>





<!-- End Header -->



  <main id="main">



    <!-- ======= Our Services Section ======= -->

    <section class="breadcrumbs">

      <div class="container">



        <div class="d-flex justify-content-between align-items-center">

          <h2>Our Services</h2>

          <ol>

          <li><a href="http://teamapp.in/">Home</a></li>
            <li>Our Services</li>

          </ol>

        </div>



      </div>

    </section><!-- End Our Services Section -->



    <!-- ======= Services Section ======= -->

    <section class="services">

      <div class="container">



        <div class="row">

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up">

            <div class="icon-box icon-box-pink">

              <div class="icon"><i class="bx bxl-dribbble"></i></div>

              <h4 class="title"><a href="">Lorem Ipsum</a></h4>

              <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>

            </div>

          </div>



          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">

            <div class="icon-box icon-box-cyan">

              <div class="icon"><i class="bx bx-file"></i></div>

              <h4 class="title"><a href="">Sed ut perspiciatis</a></h4>

              <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>

            </div>

          </div>



          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">

            <div class="icon-box icon-box-green">

              <div class="icon"><i class="bx bx-tachometer"></i></div>

              <h4 class="title"><a href="">Magni Dolores</a></h4>

              <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>

            </div>

          </div>



          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">

            <div class="icon-box icon-box-blue">

              <div class="icon"><i class="bx bx-world"></i></div>

              <h4 class="title"><a href="">Nemo Enim</a></h4>

              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>

            </div>

          </div>



        </div>



      </div>

    </section><!-- End Services Section -->



    <!-- ======= Why Us Section ======= -->

    <section class="why-us section-bg" data-aos="fade-up" date-aos-delay="200">

      <div class="container">



        <div class="row">

          <div class="col-lg-6 video-box">

            <img src="public/img/why-us.jpg" class="img-fluid" alt="">

            <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>

          </div>



          <div class="col-lg-6 d-flex flex-column justify-content-center p-5">



            <div class="icon-box">

              <div class="icon"><i class="bx bx-fingerprint"></i></div>

              <h4 class="title"><a href="">Lorem Ipsum</a></h4>

              <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>

            </div>



            <div class="icon-box">

              <div class="icon"><i class="bx bx-gift"></i></div>

              <h4 class="title"><a href="">Nemo Enim</a></h4>

              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>

            </div>



          </div>

        </div>



      </div>

    </section><!-- End Why Us Section -->
    <section class="service-details">
      <div class="container">
        <div class="row">
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-1.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">WHY CHOOSE US</a></h5>
                <p class="card-text">There is many resons  to choose team app because there is very type of facility provide in team app for manage a team in an origination like attendance ,daily report as timesheet , employee salary slip ,bank statement ,all project on which he working and also there showing offer letter which is offer to employee and company policy and which type of benefits offer company to employee  everything is transparent in team app . And team app is developed by high skills and well knowledge employee.</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-2.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">WHO WE ARE</a></h5>
                <p class="card-text">We have high skill developer who have excellent knowledge of PHP, larval, java, css etc programming languages and already we have done many project of our client which are web application ,web application and websites and also we have received  good feedback from our client side for our work and products which we have deliver to our clients."</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-3.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">Our Mission</a></h5>
                <p class="card-text">Our mission is to develop  a team in which we have high skills more experience employee and who can develop  high quality software, mobile app, website and logo etc. with maximum functionality and security with thin time in estimated cost .</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-4.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">Our Vission</a></h5>
                <p class="card-text">Our vision we more popular in whole country with high rated for our work and what facility we provide to our client and user via our software developing team and develop maximum to maximum software and website in very year.</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
        </div>    
      </div>

    </section>


<section class="facts section-bg" data-aos="fade-up">
        <div class="container">
          <div class="row counters">
            <div class="col-lg-3 col-6 text-center">
              <span data-toggle="counter-up">232</span>
              <p>Clients</p>
            </div>
            <div class="col-lg-3 col-6 text-center">
              <span data-toggle="counter-up">521</span>
              <p>Projects</p>
            </div>
            <div class="col-lg-3 col-6 text-center">
              <span data-toggle="counter-up">1,463</span>
              <p>Hours Of Support</p>
            </div>
            <div class="col-lg-3 col-6 text-center">
              <span data-toggle="counter-up">15</span>
              <p>Hard Workers</p>
            </div>
          </div>
        </div>
      </section>
<?php  require_once(APPPATH . 'Views/layout/footer.php'); ?>

