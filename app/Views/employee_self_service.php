<div class="col-md-12"> 



    <?php require_once(APPPATH . 'Views/layout/header.php');?>





<!-- End Header -->



  <main id="main">



    <!-- ======= Our Services Section ======= -->

    <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <h2>Our Services</h2>
          <ol>
          <li><a href="http://teamapp.in/">Home</a></li>
            <li>Our Services</li>
          </ol>
        </div>
      </div>
    </section><!-- End Our Services Section -->



    <!-- ======= Services Section ======= -->

    <section class="services">
      <div class="container">
        <div class="row">
          <div class="col-md-6  d-flex align-items-stretch" data-aos="fade-up">
            <div class="icon-box icon-box-pink">
              <div class="icon"><i class="bx bxl-dribbble"></i></div>
              <h4 class="title"><a href="">Employee self service</a></h4>
              <p class="description">In Team App Employee Self Service module is a self-service portal for every employee of the company and its a part of the software that all of your employees have access to, not just for Admin/HR/Project or Reporting Manager professionals. Employees can access their “To Do List” My Attendance and My Timesheet, make updates to their own details, and more.
                  With the Employee Self Service module user can check his/her Add Tasks When user add the task on the daily basis “Add task” goes to project manager/Team lead for approval. when project manager/Team lead gives approval from own side then user will be able to view his/her approved task on the page of “To Do List”. if approve task is displaying on the “To do List” then user can able to update his/her task when task update successfully then user can check his/her working hours/Log Hours on the My timesheet module of the calendar view. 
                      In the module of My Attendance user can check his/her Attendance Accordingly to calendar views such as yearly View (user can able to check his/her presence or absence on the yearly calendar) and Monthly view ((user can able to check his/her presence or absence on the monthly calendar).
                 In the module of My Timesheet user can check his/her working hours’/log hours the user has three options to view his/her Timesheet from this module such as – yearly view (from this user can view his/her Timesheet hours from the year calendar), monthly view (from this user can view his/her Timesheet hours with Project wise from the Monthly calendar) and Detailed view (from this user can view all details like – Date, Project, Activity, Task name or Timesheet Hours Duration).
                        </p>
            </div>
          </div>

          <div class="col-md-6  d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="icon-box icon-box-cyan">
              <div class="icon"><i class="bx bx-file"></i></div>
              <h4 class="title"><a href="">To Do List </a></h4>
              <p class="description">First click on main module of Employee Self Services then click on “To Do List” module with the To Do List user can view his/her approve task when user add new task on the daily basis “Add task” goes to project manager/Team lead for approval. when project manager/Team lead gives approval from own side then user will be able to view his/her approved task on the page of “To Do List”. if approve task is displaying on the “To do List” then user can able to update his/her task when task update successfully then user can check his/her working hours/Log Hours on My timesheet module of the calendar view. </p>

            </div>

          </div>
        </div>

        <div class="row">
         <div class="col-md-6  d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
           <div class="icon-box icon-box-green">
         <div class="icon"><i class="bx bx-tachometer"></i></div>
                <h4 class="title"><a href="">My Attendance</a></h4>
            <p class="description">In the module of My Attendance user can check his/her Attendance Accordingly to calendar views such as yearly View (User can able to check his/her presence or absence on the yearly calendar) and Monthly view (User can able to check his/her presence or absence on the monthly calendar).          </p>
          </div>
          </div>

<div class="col-md-6  d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
  <div class="icon-box icon-box-blue">
      <div class="icon"><i class="bx bx-world"></i></div>
      <h4 class="title"><a href="">My Timeshet</a></h4>
      <p class="description">In the module of My Timesheet user can check his/her working hours’/log hours the user has three options to view his/her Timesheet from this module such as – yearly view (From Yearly view user can view his/her Timesheet hours from the year calendar), monthly view (From Monthly view user can view his/her Timesheet hours with Project wise from the Monthly calendar) and Detailed view (From the Detailed view user can view all details like – Date, Project, Activity, Task name or Timesheet Hours Duration).       </p>
    </div>
    </div>
  </div>
</div>

    </section><!-- End Services Section -->



    <!-- ======= Why Us Section ======= -->

    <!-- <section class="why-us section-bg" data-aos="fade-up" date-aos-delay="200">

      <div class="container">



        <div class="row">

          <div class="col-lg-6 video-box">

            <img src="public/img/why-us.jpg" class="img-fluid" alt="">

            <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>

          </div>



          <div class="col-lg-6 d-flex flex-column justify-content-center p-5">



            <div class="icon-box">

              <div class="icon"><i class="bx bx-fingerprint"></i></div>

              <h4 class="title"><a href="">Lorem Ipsum</a></h4>

              <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>

            </div>



            <div class="icon-box">

              <div class="icon"><i class="bx bx-gift"></i></div>

              <h4 class="title"><a href="">Nemo Enim</a></h4>

              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>

            </div>



          </div>

        </div>



      </div>

    </section>   End Why Us Section -->


 <!-- ======= Service Details Section ======= -->
 <section class="service-details">
      <div class="container">
        <div class="row">
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-1.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">WHY CHOOSE US</a></h5>
                <p class="card-text">There is many resons  to choose team app because there is very type of facility provide in team app for manage a team in an origination like attendance ,daily report as timesheet , employee salary slip ,bank statement ,all project on which he working and also there showing offer letter which is offer to employee and company policy and which type of benefits offer company to employee  everything is transparent in team app . And team app is developed by high skills and well knowledge employee.</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-2.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">WHO WE ARE</a></h5>
                <p class="card-text">We have high skill developer who have excellent knowledge of PHP, larval, java, css etc programming languages and already we have done many project of our client which are web application ,web application and websites and also we have received  good feedback from our client side for our work and products which we have deliver to our clients."</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-3.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">Our Mission</a></h5>
                <p class="card-text">Our mission is to develop  a team in which we have high skills more experience employee and who can develop  high quality software, mobile app, website and logo etc. with maximum functionality and security with thin time in estimated cost .</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-4.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">Our Vission</a></h5>
                <p class="card-text">Our vision we more popular in whole country with high rated for our work and what facility we provide to our client and user via our software developing team and develop maximum to maximum software and website in very year.</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
        </div>    
      </div>

    </section>
    <!-- End Service Details Section -->

    <!-- ======= Facts Section ======= -->



    <!-- ======= Tetstimonials Section ======= -->

    <section class="testimonials" data-aos="fade-up">
      <div class="container">
        <div class="section-title">
          <h2>Tetstimonials</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>
        <div class="owl-carousel testimonials-carousel">
          <div class="testimonial-item">
            <img src="public/img/testimonials/amir1.jpg" class="testimonial-img" alt="">
            <h3>Amir Siddiqui</h3>
            <h4>WordPress Shofiy <br> Web & Graphics Designer </h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Having around 3 years of experience in website and graphics design, mobile app designs, flyers, brochures & logos. My designs speaks for me.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
            <img src="public/img/testimonials/goapl.jpg" class="testimonial-img" alt="">
            <h3>Gopal Acharya </h3>
            <h4>Wordpress & Core PHP</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Working on PHP technology, API’s & WordPress from last 5 years. Created CRM’s and business solutions for MNC’s and other small businesses.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
            <img src="public/img/testimonials/soran.jpg" class="testimonial-img" alt="">
            <h3>Soarn Paswan</h3>
            <h4>Laravel developer</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                    I am developing websites for the last 4 years and developed websites for the Govt and private firms in India and abroad.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
            <img src="public/img/testimonials/anurag.jpg" class="testimonial-img" alt="">
            <h3>Anurag</h3>
            <h4> Executive Quality analyst</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
            2 Years experience in manual testing , knowledge of sql databse, java, selenium,j meter.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

        </div>
      </div>

    </section><!-- End Ttstimonials Section -->



  </main><!-- End #main -->

    <!-- ======= Pricing Section ======= -->

     <!-- ======= Facts Section ======= -->

     <section class="facts section-bg" data-aos="fade-up">
      <div class="container">
        <div class="row counters">
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">232</span>
            <p>Clients</p>
          </div>
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">521</span>
            <p>Projects</p>
          </div>
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">1,463</span>
            <p>Hours Of Support</p>
          </div>
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">15</span>
            <p>Hard Workers</p>
          </div>
        </div>
      </div>

    </section><!-- End Facts Section -->

  </main><!-- End #main -->







<?php  require_once(APPPATH . 'Views/layout/footer.php'); ?>

