
<style>
#anmi-hero{
  width: 100%;
  height: 90vh;
  overflow: hidden;
  position: relative;
  background-image: url("public/img/ban-img1.jpg");
  background-size: cover;
}
 </style>
<div class="col-md-12"> 
    <?php require_once(APPPATH . 'Views/layout/header.php');?>
    
  <main id="main">
   
 
   
    <!-- ======= Hero Section ======= -->
  <section id="anmi-hero" >
  <div class="bann_anim">
    <figure class="icon-cards mt-3">
      <div class="icon-cards__content">

        <div class="icon-cards__item align-items-center justify-content-center">
          <h2 >WELCOME TO TEAMAPP</h2>
          <p>TEAMAPP a multinational team headquartered in Greater Noida. We are a lean start-up that values making meaningful and easy-to-use products. We shift things fast to give our customers what matters most to them, and to keep us ahead of the game. Our motto is "First do it, then do it right, then do it better."</p>
          <a href="#" class="anim-btn">Read More</a>
        </div>

        <div class="icon-cards__item align-items-center content-center">
        <h2 >WHY CHOOSE US</h2>
          <p>Here are some of the reasons we think you should consider working with Technitab Web Designs & Digital Marketing, Inc. As your full service web development, Digital marketing and web hosting company We create experiences that are attractive, simple to use, and drive results for your company. </p>
          <a href="#" class="anim-btn">Read More</a>
       </div>

        <div class="icon-cards__item align-items-center justify-content-center">
        <h2 >We Create Experiences</h2>
          <p >We create experiences that are attractive, simple to use, and drive results for your company. We are not your typical web development agency. Sure, we’re strong on corporate branding and web design, but we’re really focused on making things work for your audience ... and your business.</p>
          <a href="#" class="anim-btn">Read More</a>
        </div>

    
      </div>
    </figure>
  </div>
  </section><!-- End Hero -->

     <?php require_once(APPPATH . 'Views/layout/slider.php');?>

      <main id="main">
         <!-- ======= Services Section End======= -->
    <!-- ======= Services Section ======= -->
    
    <section class="services">
      <div class="container">

        <div class="row">
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up">
            <div class="icon-box icon-box-pink">
              <div class="icon"><i class="bx bxl-dribbble"></i></div>
              <h4 class="title"><a href="">Attendance</a></h4>
              <p class="description">In this the user can mark and review their attendence. User can review their attendance according to their wish. As it have the feature of monthly view, yearly view and detailed view.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="icon-box icon-box-cyan">
              <div class="icon"><i class="bx bx-file"></i></div>
              <h4 class="title"><a href="">salary</a></h4>
              <p class="description"> In this user can check their salary as per month paid by the the company.And benifits and perks priovided to them by the company.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box icon-box-green">
              <div class="icon"><i class="bx bx-tachometer"></i></div>
              <h4 class="title"><a href="">Expenses</a></h4>
              <p class="description">In this user can claim their expenses done by them and check the claims status which is paid,unpaid or rejected by the company</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box icon-box-blue">
              <div class="icon"><i class="bx bx-world"></i></div>
              <h4 class="title"><a href="">SOA</a></h4>
              <p class="description">Statement of Account. in this user can check their payment which is provided to them by the company. In this user can check thier payment as per date with discription. </p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= Why Us Section ======= -->
    <!-- <section class="why-us section-bg" data-aos="fade-up" date-aos-delay="200">
      <div class="container">

        <div class="row">
          <div class="col-lg-6 video-box">
            <img src="assets/img/why-us.jpg" class="img-fluid" alt="">
            <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
          </div>

          <div class="col-lg-6 d-flex flex-column justify-content-center p-5">

            <div class="icon-box">
              <div class="icon"><i class="bx bx-fingerprint"></i></div>
              <h4 class="title"><a href="">Lorem Ipsum</a></h4>
              <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
            </div>

            <div class="icon-box">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">Nemo Enim</a></h4>
              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
            </div>

          </div>
        </div>

      </div>
    </section>End Why Us Section -->

    <!-- ======= Features Section ======= -->
    <!-- <section class="features">
      <div class="container">

        <div class="section-title">
          <h2>Features</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <div class="row" data-aos="fade-up">
          <div class="col-md-5">
            <img src="assets/img/features-1.svg" class="img-fluid" alt="">
          </div>
          <div class="col-md-7 pt-4">
            <h3>Voluptatem dignissimos provident quasi corporis voluptates sit assumenda.</h3>
            <p class="font-italic">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
              magna aliqua.
            </p>
            <ul>
              <li><i class="icofont-check"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
              <li><i class="icofont-check"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
            </ul>
          </div>
        </div>

        <div class="row" data-aos="fade-up">
          <div class="col-md-5 order-1 order-md-2">
            <img src="assets/img/features-2.svg" class="img-fluid" alt="">
          </div>
          <div class="col-md-7 pt-5 order-2 order-md-1">
            <h3>Corporis temporibus maiores provident</h3>
            <p class="font-italic">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
              magna aliqua.
            </p>
            <p>
              Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
              velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
              culpa qui officia deserunt mollit anim id est laborum
            </p>
          </div>
        </div>

        <div class="row" data-aos="fade-up">
          <div class="col-md-5">
            <img src="assets/img/features-3.svg" class="img-fluid" alt="">
          </div>
          <div class="col-md-7 pt-5">
            <h3>Sunt consequatur ad ut est nulla consectetur reiciendis animi voluptas</h3>
            <p>Cupiditate placeat cupiditate placeat est ipsam culpa. Delectus quia minima quod. Sunt saepe odit aut quia voluptatem hic voluptas dolor doloremque.</p>
            <ul>
      </ul>
    </div>
  </div>
</div>
</section> -->
<section class="service-details">
      <div class="container">
        <div class="row">
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-1.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">WHY CHOOSE US</a></h5>
                <p class="card-text">There is many reasons  to choose team app because there are many facilities provided in team app for manage a team in an origination like attendance ,daily report as timesheet , employee salary slip ,bank statement ,all project on which he working and also there showing offer letter which is offer to employee and company policy and which type of benefits offer company to employee  everything is transparent in team app . And team app is developed by high skills and well knowledge employee.</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-2.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">WHO WE ARE</a></h5>
                <p class="card-text">We have high skill developer who have excellent knowledge of PHP, larval, java, css etc programming languages and already we have done many project of our client which are web application ,web application and websites and also we have received  good feedback from our client side for our work and products which we have deliver to our clients."</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-3.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">Our Mission</a></h5>
                <p class="card-text">Our mission is to develop  a team in which we have high skills more experience employee and who can develop  high quality software, mobile app, website and logo etc. with maximum functionality and security with thin time in estimated cost .</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-4.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">Our Vission</a></h5>
                <p class="card-text">Our vision we more popular in whole country with high rated for our work and what facility we provide to our client and user via our software developing team and develop maximum to maximum software and website in very year.</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
        </div>    
      </div>

    </section>
    <!-- End Service Details Section -->
    <section class="facts section-bg" data-aos="fade-up">
      <div class="container">
        <div class="row counters">
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">232</span>
            <p>Clients</p>
          </div>
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">521</span>
            <p>Projects</p>
          </div>
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">1,463</span>
            <p>Hours Of Support</p>
          </div>
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">15</span>
            <p>Hard Workers</p>
          </div>
        </div>
      </div>

    </section>

<?php  require_once(APPPATH . 'Views/layout/footer.php'); ?>