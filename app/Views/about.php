<div class="col-md-12"> 
   <?php require_once(APPPATH . 'Views/layout/header.php');?>
   

  <main id="main">
    <!-- ======= About Us Section ======= -->
    <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <h2>About Us</h2>
          <ol>
            <li><a href="http://teamapp.in/">Home</a></li>
            <li>About Us</li>
          </ol>
        </div>
      </div>
    </section><!-- End About Us Section -->

    <!-- ====== About Section ======= -->

    <section class="about" data-aos="fade-up">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <img src="public/img/about.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0">
           <h3> <b>TEAMAPP </b> <span style="font-size:17px "> <u>perfect Solutions</u> </span> </h3>
            <p class="font-italic">
            <b>TEAMAPP</b> Team Management System Perfect Solutions  for IT companys.
            </p>
            <ul>
              <li><i class="icofont-check-circled"></i> Team App is a mobile and web app in which a company manage all information of employees, like his/her personal details, educational details, job details, skills details ,Assets details which is assign to employees to work on project  and offer letter which offer to employee during employment .  </li>
             
            </ul>
          </div>
        </div>



      </div>
    </section><!-- End About Section -->


<!-- ============= Feature of Team app========================== -->
    <section class="skills" data-aos="fade-up">
      <div class="container">
        <div class="section-title">
          <h2> Features of TeamApp</h2>
          <p style="text-align:left;"><b>TeamApp</b> daily attendance in which mention log hours , working hoursand also can mark /apply leave for that day on which we is not able to come office for work and there is access level like Am/PM ,Admin/HR ,project manager, Accounts,Team lead and Employee according to  level of post this application provide access to them like AM/PM have access of every module and sub modules, Admin/HR access of every team modules. <br><br>
           Manager can access team project ,activity and task and team lead can access his team members and accounts access level all account related modules and employee access level is all personal level modules. In the team app all we can create on which we have to work or Manager/ Team lead can assign project to employee and also activity related to projects according to budget hours, for daily working report he can add task related to project and activity and fill timesheet according to working hours. And also every employee can check and download accounts related information like salary slip,<br><br> 
           Account statement and payout details. For site Engineers there is module to create trip for site project and also can create booking request and payment request for booking . In team app there is project expense module available from where site engineers can claim for all charges like local convenes ,hotel/PG, food per diem ,repair and maintenance ,other expenses on the basis of invoices amount /per diem rate .there is also account related module available from where company manage all account  related work like invoice match ,Payment match etc.<br><br>
          HR there is HCM module available from where he/she can upload data for hiring and  also can fill all call information according to response from condidates and status of that particulars call and approve attendance according to punch In and out time and timesheet basis and prepare salary of employees and for admin there is CRM module available from where he/she call to customer/client for project work and fill all information regarding call to client for project and response from client according to status.</p>
        </div>
      </div>
    </section>
    <!-- ============= End Feature of Team app========================== -->

    <!-- ============= Services Section Start ========================== -->
    <section class="services">
          <div class="container">
            <div class="row">
              <div class="col-md-6  d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                  <div class="icon-box icon-box-cyan">
                    <div class="icon"><i class="bx bx-file"></i></div>
                    <h4 class="title"><a href="">What Super Admin Do</a></h4>
                    <p class="description">In team app Super admin work for Discuss with client for project and collect requirement from client and discuss with project manager and Team lead about project functionality and feature require ment in application and website client want and also discuss with client about budgut of project and also check status and update about project on which IT team working and after deliver project to client take feedback from client about the project which is deliver to client and update feedback on our website to make more valuable comoany in future .</p>

                  
                  </div>
              </div>

              <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
                <div class="icon-box icon-box-green">
                  <div class="icon"><i class="bx bx-tachometer"></i></div>
                  <h4 class="title"><a href="">What Admin Do </a></h4>
                    <p class="description">InTeam app Admin aoorove all project cereated by site engineers during site and also do boking as per request by site engineers for travel or stay in hotel for site project and also payment for booking hotel or flight ,train etc as per request by site engineers for his payment request and verify all tec entry like perdiem amount according to base location site location and stay mode and hotel bill according to invoice attached by engineers and local convenes according to travelling bill amount is corret or not and afert verify of tec entry approve then and pay according to tec entry amount fill by site engineers for expense . And assign assets to site engineer during project work as per requirement and  recharge petty cash and also maintain record of petty cash . and aslo give access according to access level or employee type .</p>
                  </div>
                </div>
              </div>
          </div> 
        </div>
    </section>
    <!-- ============= Services Section Start ========================== -->

    <!-- ======= Our Skills Section ======= -->
    <section class="skills" data-aos="fade-up">
      <div class="container">
        <div class="section-title">
          <h2>Our Skills</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <div class="skills-content">

        <div class="progress">
            <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">Core PHP<i class="val">100%</i></span>
            </div>
          </div>
          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">HTML <i class="val">90%</i></span>
            </div>
          </div>
          
          

          <div class="progress">
            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">CSS <i class="val">80%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">JavaScript <i class="val">70%</i></span>
            </div>
          </div>

         

          <div class="progress">
            <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">Photoshop <i class="val">60%</i></span>
            </div>
          </div>
        </div>
      </div>

    </section><!-- End Our Skills Section -->

   <!-- ======= Service Details Section ======= -->
   <section class="service-details">
      <div class="container">
        <div class="row">
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-1.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">WHY CHOOSE US</a></h5>
                <p class="card-text">There is many resons  to choose team app because there is very type of facility provide in team app for manage a team in an origination like attendance ,daily report as timesheet , employee salary slip ,bank statement ,all project on which he working and also there showing offer letter which is offer to employee and company policy and which type of benefits offer company to employee  everything is transparent in team app . And team app is developed by high skills and well knowledge employee.</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-2.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">WHO WE ARE</a></h5>
                <p class="card-text">We have high skill developer who have excellent knowledge of PHP, larval, java, css etc programming languages and already we have done many project of our client which are web application ,web application and websites and also we have received  good feedback from our client side for our work and products which we have deliver to our clients."</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-3.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">Our Mission</a></h5>
                <p class="card-text">Our mission is to develop  a team in which we have high skills more experience employee and who can develop  high quality software, mobile app, website and logo etc. with maximum functionality and security with thin time in estimated cost .</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-4.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">Our Vission</a></h5>
                <p class="card-text">Our vision we more popular in whole country with high rated for our work and what facility we provide to our client and user via our software developing team and develop maximum to maximum software and website in very year.</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
        </div>    
      </div>

    </section>
    <!-- End Service Details Section -->

    <!-- ======= Facts Section ======= -->

   
    <!-- ======= Tetstimonials Section ======= -->

    <section class="testimonials" data-aos="fade-up">
      <div class="container">
        <div class="section-title">
         <h2>Our Specialists</h2>
           <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
        </div>
        <div class="owl-carousel testimonials-carousel">
          <div class="testimonial-item">
            <img src="public/img/testimonials/amir1.jpg" class="testimonial-img" alt="">
            <h3>Amir Siddiqui</h3>
            <h4>WordPress Shofiy <br> Web & Graphics Designer </h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Having around 3 years of experience in website and graphics design, mobile app designs, flyers, brochures & logos. My designs speaks for me.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
            <img src="public/img/testimonials/goapl.jpg" class="testimonial-img" alt="">
            <h3>Gopal Arya </h3>
            <h4>Wordpress & Core PHP</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Working on PHP technology, API’s & WordPress from last 5 years. Created CRM’s and business solutions for MNC’s and other small businesses.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
            <img src="public/img/testimonials/soran.jpg" class="testimonial-img" alt="">
            <h3>Soren Paswan</h3>
            <h4>Laravel developer</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                    I am developing websites for the last 4 years and developed websites for the Govt and private firms in India and abroad.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
            <img src="public/img/testimonials/anurag.jpg" class="testimonial-img" alt="">
            <h3>Anurag</h3>
            <h4> Executive Quality analyst</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
            2 Years experience in manual testing , knowledge of sql databse, java, selenium,j meter.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

        </div>
      </div>

    </section>
    <!-- End Ttstimonials Section -->

    <section class="facts section-bg" data-aos="fade-up">
      <div class="container">
        <div class="row counters">
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">232</span>
            <p>Clients</p>
          </div>
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">521</span>
            <p>Projects</p>
          </div>
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">1,463</span>
            <p>Hours Of Support</p>
          </div>
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">15</span>
            <p>Hard Workers</p>
          </div>
        </div>
      </div>

    </section>
    <!-- End Facts Section -->


  </main><!-- End #main -->

<?php  require_once(APPPATH . 'Views/layout/footer.php'); ?>

