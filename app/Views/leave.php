<div class="col-md-12"> 



    <?php require_once(APPPATH . 'Views/layout/header.php');?>





<!-- End Header -->



  <main id="main">



    <!-- ======= Our Services Section ======= -->

    <section class="breadcrumbs">

      <div class="container">



        <div class="d-flex justify-content-between align-items-center">

          <h2>Our Services</h2>

          <ol>

          <li><a href="http://localhost/teamapp.in/">Home</a></li>
            <li>Our Services</li>

          </ol>

        </div>



      </div>

    </section><!-- End Our Services Section -->



    <!-- ======= Services Section ======= -->

    <section class="services">
      <div class="container">
        <div class="row">
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="icon-box icon-box-pink">
              <div class="icon"><i class="bx bxl-dribbble"></i></div>
              <h4 class="title"><a href="">Leaves</a></h4>
              <p class="description">With the help of Team App of the module of “Leaves” user can able to apply his/her Leave.  when user apply his/her Leave Then Leaves goes for Approval to the HR/ Admin/Project manager.</p>
            </div>
          </div>

          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="icon-box icon-box-cyan">
              <div class="icon"><i class="bx bx-file"></i></div>
              <h4 class="title"><a href="">My Leaves</a></h4>
              <p class="description">User can check his/her Past Leaves details by click on “MY Leaves” module. User can filter his/her leave data as per the date, as per the status or as per the Particular keywords.All The Approve Leaves from the HR/ Admin/Project manager is successfully showing on the page of “My Leaves” and user can easily see his/her approve Leaves. User can check Leaves Status and if user want to cancel his/her Apply Leaves then with the help of “Action Button” User can easily cancel his Leaves.</p>
            </div>
          </div>

        </div>

        <div class="row">
        <div class="col-md-4  d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box icon-box-green">
              <div class="icon"><i class="bx bx-tachometer"></i></div>
              <h4 class="title"><a href="">Leaves Apply</a></h4>
              <p class="description">User can directly apply leave by using “Leave Apply” module when user click on “Leaves Apply” Module then then Page of “Leaves Apply” opens then user required to fill Leave type like Paid Leave or any other from three listed in the dropdown, select date to or date from, location at time of Like-Delhi, Bangalore or it can be any place, select the reason of the leave like-sick, training or any other from the dropdown. And then save the details. When All required details successfully done then leaves Apply Successfully.</p>
            </div>
          </div>

          <div class="col-md-4  d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box icon-box-blue">
              <div class="icon"><i class="bx bx-world"></i></div>
              <h4 class="title"><a href="">Team Leaves</a></h4>
              <p class="description">This functionality is for HR/Admin/Project or Reporting Manager. Records of the leave Applied by Employees with the status of Approved & Rejected in the details will showing on the page of “Team Leaves” Page. Team Leaves Can Filter employee data as per the date, as per the status or as per the particular keywords.</p>
            </div>
          </div>

          <div class="col-md-4  d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box icon-box-blue">
              <div class="icon"><i class="bx bx-world"></i></div>
              <h4 class="title"><a href="">Assign Leaves</a></h4>
              <p class="description">This functionality is for HR/Admin/Project or Reporting Manager. If HR/Admin/Project or Reporting Manager want to assign Leaves to his/her employee, then HR/Admin/Project or Reporting Manager can access directly Assign Leave to his/her employee. If all our remaining leaves have been completed, then we can directly Assign Leave to his/her HR/Admin/Project or Reporting Manager.</p>
            </div>
          </div>
        
        </div>
      </div>

    </section><!-- End Services Section -->



    <!-- ======= Why Us Section ======= -->

    <!-- <section class="why-us section-bg" data-aos="fade-up" date-aos-delay="200">

      <div class="container">



        <div class="row">

          <div class="col-lg-6 video-box">

            <img src="public/img/why-us.jpg" class="img-fluid" alt="">

            <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>

          </div>



          <div class="col-lg-6 d-flex flex-column justify-content-center p-5">



            <div class="icon-box">

              <div class="icon"><i class="bx bx-fingerprint"></i></div>

              <h4 class="title"><a href="">Lorem Ipsum</a></h4>

              <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>

            </div>



            <div class="icon-box">

              <div class="icon"><i class="bx bx-gift"></i></div>

              <h4 class="title"><a href="">Nemo Enim</a></h4>

              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>

            </div>



          </div>

        </div>



      </div>

    </section>   End Why Us Section -->



    <!-- ======= Service Details Section ======= -->

 <!-- ======= Service Details Section ======= -->
 <section class="service-details">
      <div class="container">
        <div class="row">
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-1.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">WHY CHOOSE US</a></h5>
                <p class="card-text">There is many resons  to choose team app because there is very type of facility provide in team app for manage a team in an origination like attendance ,daily report as timesheet , employee salary slip ,bank statement ,all project on which he working and also there showing offer letter which is offer to employee and company policy and which type of benefits offer company to employee  everything is transparent in team app . And team app is developed by high skills and well knowledge employee.</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-2.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">WHO WE ARE</a></h5>
                <p class="card-text">We have high skill developer who have excellent knowledge of PHP, larval, java, css etc programming languages and already we have done many project of our client which are web application ,web application and websites and also we have received  good feedback from our client side for our work and products which we have deliver to our clients."</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-3.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">Our Mission</a></h5>
                <p class="card-text">Our mission is to develop  a team in which we have high skills more experience employee and who can develop  high quality software, mobile app, website and logo etc. with maximum functionality and security with thin time in estimated cost .</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-4.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">Our Vission</a></h5>
                <p class="card-text">Our vision we more popular in whole country with high rated for our work and what facility we provide to our client and user via our software developing team and develop maximum to maximum software and website in very year.</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
        </div>    
      </div>

    </section>
    <!-- End Service Details Section -->

    <!-- ======= Facts Section ======= -->

  </main><!-- End #main -->



    <!-- ======= Pricing Section ======= -->

    <section class="pricing section-bg" data-aos="fade-up">

      <div class="container">



        <div class="section-title">

          <h2>Pricing</h2>

          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>

        </div>



        <div class="row no-gutters">



          <div class="col-lg-4 box">

            <h3>Free</h3>

            <h4>$0<span>per month</span></h4>

            <ul>

              <li><i class="bx bx-check"></i> Quam adipiscing vitae proin</li>

              <li><i class="bx bx-check"></i> Nec feugiat nisl pretium</li>

              <li><i class="bx bx-check"></i> Nulla at volutpat diam uteera</li>

              <li class="na"><i class="bx bx-x"></i> <span>Pharetra massa massa ultricies</span></li>

              <li class="na"><i class="bx bx-x"></i> <span>Massa ultricies mi quis hendrerit</span></li>

            </ul>

            <a href="#" class="get-started-btn">Get Started</a>

          </div>



          <div class="col-lg-4 box featured">

            <h3>Business</h3>

            <h4>$29<span>per month</span></h4>

            <ul>

              <li><i class="bx bx-check"></i> Quam adipiscing vitae proin</li>

              <li><i class="bx bx-check"></i> Nec feugiat nisl pretium</li>

              <li><i class="bx bx-check"></i> Nulla at volutpat diam uteera</li>

              <li><i class="bx bx-check"></i> Pharetra massa massa ultricies</li>

              <li><i class="bx bx-check"></i> Massa ultricies mi quis hendrerit</li>

            </ul>

            <a href="#" class="get-started-btn">Get Started</a>

          </div>



          <div class="col-lg-4 box">

            <h3>Developer</h3>

            <h4>$49<span>per month</span></h4>

            <ul>

              <li><i class="bx bx-check"></i> Quam adipiscing vitae proin</li>

              <li><i class="bx bx-check"></i> Nec feugiat nisl pretium</li>

              <li><i class="bx bx-check"></i> Nulla at volutpat diam uteera</li>

              <li><i class="bx bx-check"></i> Pharetra massa massa ultricies</li>

              <li><i class="bx bx-check"></i> Massa ultricies mi quis hendrerit</li>

            </ul>

            <a href="#" class="get-started-btn">Get Started</a>

          </div>



        </div>



      </div>

    </section><!-- End Pricing Section -->

   <!-- ======= Facts Section ======= -->

   <section class="facts section-bg" data-aos="fade-up">
      <div class="container">
        <div class="row counters">
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">232</span>
            <p>Clients</p>
          </div>
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">521</span>
            <p>Projects</p>
          </div>
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">1,463</span>
            <p>Hours Of Support</p>
          </div>
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">15</span>
            <p>Hard Workers</p>
          </div>
        </div>
      </div>

    </section><!-- End Facts Section -->

  </main><!-- End #main -->







<?php  require_once(APPPATH . 'Views/layout/footer.php'); ?>

