<div class="col-md-12"> 



    <?php require_once(APPPATH . 'views/layout/header.php');?>





<!-- End Header -->



  <main id="main">



    <!-- ======= Our Services Section ======= -->

    <section class="breadcrumbs">

      <div class="container">



        <div class="d-flex justify-content-between align-items-center">

          <h2>Our Services</h2>

          <ol>

          <li><a href="http://teamapp.in/">Home</a></li>
            <li>Our Services</li>

          </ol>

        </div>



      </div>

    </section><!-- End Our Services Section -->



    <!-- ======= Services Section ======= -->

    <section class="services">

      <div class="container">



        <div class="row">

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up">

            <div class="icon-box icon-box-pink">

              <div class="icon"><i class="bx bxl-dribbble"></i></div>

              <h4 class="title"><a href="">Project & Activities</a></h4>

              <p class="description">With the help of Project & Activities module user can check his/her assigned Projects details by the click on “My Projects” and user can able to check all details about “Active Projects”, “Inactive Projects”, “Unapproved Projects” when user click on My Projects.
User can filter his/her Assigned Projects as per the date, as per the status or as per the particular keywords and user can able to “Add Projects” from the Add Project button on the page of “My Project”. When user successfully Add project then add project will be approved from the side of Admin/HR/Project or Reporting Manager.
User can check his/her Activities details by the click on My Activities and all the Activities that are assigned to his/her will be showing on the page of My Activities. User can filter his/her Activities List as per the status or as per the particular keyword. User can Add the new Activities fill the form to add a new activity when user select project field, activity name field and all required details. Then click on the save to create a new activity.
When user add the task on the daily basis “Add task” goes to project manager/Team lead for approval. when project manager/Team lead gives approval from own side then user will be able to see his/her approved task on the page of “My task” as well as “To Do List”. 
</p>

            </div>

          </div>



          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">

            <div class="icon-box icon-box-cyan">

              <div class="icon"><i class="bx bx-file"></i></div>

              <h4 class="title"><a href="">My Project</a></h4>

              <p class="description">User can check his/her assigned project details by the click on My Projects module. and user can able to check all details about “Active Projects”, “Inactive Projects”, “Unapproved Projects” when user click on My Projects.
User can filter his/her Assigned Projects as per the date, as per the status or as per the particular keywords. User can able to “Add Projects” from the Add Project button on the page of “My Project”. When user successfully Add project then add project will be approved from the side of Admin/HR/Project or Reporting Manager.
</p>

            </div>

          </div>



          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">

            <div class="icon-box icon-box-green">

              <div class="icon"><i class="bx bx-tachometer"></i></div>

              <h4 class="title"><a href="">Add Project</a></h4>

              <p class="description">When user click on Add Project module then user should be required to fill the details like select “Project Type” on the field of project type, choose “Client name” on the field of client name and finally enter customer name and click on search button. when user successfully click on search button then “Add Project” Form Page open and user should fill all required field and click on submit button. When user successfully click on submit button then project successfully Add.
</p>

            </div>

          </div>



          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">

            <div class="icon-box icon-box-blue">

              <div class="icon"><i class="bx bx-world"></i></div>

              <h4 class="title"><a href="">Team Projects</a></h4>

              <p class="description">When User click on Team Project module then all the details about “Team Project” are showing on the page of “Team Project” and user can able to check all details about “Active Projects”, “Inactive Projects”, “Unapproved Projects” “All Projects” displaying on the page of Team Projects when user click on Team Projects.
</p>

            </div>

          </div>




          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">

            <div class="icon-box icon-box-blue">

              <div class="icon"><i class="bx bx-world"></i></div>

              <h4 class="title"><a href="">Approve Project</a></h4>

              <p class="description">When user add the Project then “Add Project” goes to Admin/HR/Project or Reporting Manager for approval. when Admin/HR/Project or Reporting Manager gives approval from own side then user will be able to see his/her approved Project on the page of My Project and Authorized body can Edit, Delete and approve the pending projects.

</p>

            </div>

          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">

            <div class="icon-box icon-box-blue">

              <div class="icon"><i class="bx bx-world"></i></div>

              <h4 class="title"><a href="">Assign Project</a></h4>

              <p class="description">This functionality is for Admin/HR/Project or Reporting Manager. Functionality used to assign the project to the employee as per the project type. Fill the Form to assign the project Select “Project Type” on the field of Project type select “Project Name” on the field of project Name select “Assignees” and then click on submit button.
</p>

            </div>

          </div>

           <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">

            <div class="icon-box icon-box-blue">

              <div class="icon"><i class="bx bx-world"></i></div>

              <h4 class="title"><a href="">My Activities </a></h4>

              <p class="description">User can check his/her Activities details by the click on My Activities and all the Activities that are assigned to his/her will be showing on the page of My Activities. User can filter his/her Activities List as per the status or as per the particular keyword. User can Add the new Activities fill the form to add a new activity when user select project field, activity name field and all required details. Then click on the save to create a new activity.

</p>

            </div>

          </div>

           <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">

            <div class="icon-box icon-box-blue">

              <div class="icon"><i class="bx bx-world"></i></div>

              <h4 class="title"><a href="">Add Activities</a></h4>

              <p class="description">With the help of “Add Activities” module User can add new activities. Fill the form to add a new activity like select “project” on the field of project select “Activity Name” on the field of Activity name and enter both start date or end date on the both field of start date or end date and fill all other fields and finally click on update button. When user successfully add new Activities Then Add Activity appear on the page of “My Projects”.


</p>

            </div>

          </div>


 <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">

            <div class="icon-box icon-box-blue">

              <div class="icon"><i class="bx bx-world"></i></div>

              <h4 class="title"><a href="">Assign Activities</a></h4>

              <p class="description">This functionality is for Admin/HR/Project or Reporting Manager. Functionality used to assign the Activities to the employee. Fill the Form to assign the project Select “Project Type” on the field of Project type select “Project Name” on the field of project Name select “Assignees” and then click on submit button.

</p>

            </div>

          </div>
           <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">

            <div class="icon-box icon-box-blue">

              <div class="icon"><i class="bx bx-world"></i></div>

              <h4 class="title"><a href="">My Task</a></h4>

              <p class="description">When user successfully add task if Project manager/Team lead gives approval from own side then user will be able to see his/her approved task on the page of “My task” as well as “To Do List”. 

</p>

            </div>

          </div>
          

           <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">

            <div class="icon-box icon-box-blue">

              <div class="icon"><i class="bx bx-world"></i></div>

              <h4 class="title"><a href="">Add Task</a></h4>

              <p class="description">When user add the task on the daily basis “Add task” goes to project manager/Team lead for approval. when project manager/Team lead gives approval from own side then user will be able to see his/her approved task on the page of “My task” as well as “To Do List”.

</p>

            </div>

          </div>

          

        </div>



      </div>

    </section><!-- End Services Section -->



    <!-- ======= Why Us Section ======= -->

    <section class="why-us section-bg" data-aos="fade-up" date-aos-delay="200">

      <div class="container">



        <div class="row">

          <div class="col-lg-6 video-box">

            <img src="public/img/why-us.jpg" class="img-fluid" alt="">

            <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>

          </div>



          <div class="col-lg-6 d-flex flex-column justify-content-center p-5">



            <div class="icon-box">

              <div class="icon"><i class="bx bx-fingerprint"></i></div>

              <h4 class="title"><a href="">Lorem Ipsum</a></h4>

              <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>

            </div>



            <div class="icon-box">

              <div class="icon"><i class="bx bx-gift"></i></div>

              <h4 class="title"><a href="">Nemo Enim</a></h4>

              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>

            </div>



          </div>

        </div>



      </div>

    </section><!-- End Why Us Section -->



    <!-- ======= Service Details Section ======= -->

    <section class="service-details">
      <div class="container">
        <div class="row">
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-1.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">WHY CHOOSE US</a></h5>
                <p class="card-text">There is many resons  to choose team app because there is very type of facility provide in team app for manage a team in an origination like attendance ,daily report as timesheet , employee salary slip ,bank statement ,all project on which he working and also there showing offer letter which is offer to employee and company policy and which type of benefits offer company to employee  everything is transparent in team app . And team app is developed by high skills and well knowledge employee.</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-2.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">WHO WE ARE</a></h5>
                <p class="card-text">We have high skill developer who have excellent knowledge of PHP, larval, java, css etc programming languages and already we have done many project of our client which are web application ,web application and websites and also we have received  good feedback from our client side for our work and products which we have deliver to our clients."</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-3.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">Our Mission</a></h5>
                <p class="card-text">Our mission is to develop  a team in which we have high skills more experience employee and who can develop  high quality software, mobile app, website and logo etc. with maximum functionality and security with thin time in estimated cost .</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="public/img/service-details-4.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#">Our Vission</a></h5>
                <p class="card-text">Our vision we more popular in whole country with high rated for our work and what facility we provide to our client and user via our software developing team and develop maximum to maximum software and website in very year.</p>
                <div class="read-more"><a href="#"><i class="icofont-arrow-right"></i> Read More</a></div>
              </div>
            </div>
          </div>
        </div>    
      </div>

    </section><!-- End Service Details Section -->



    <!-- ======= Pricing Section ======= -->

    

  </main><!-- End #main -->







<?php  require_once(APPPATH . 'views/layout/footer.php'); ?>

