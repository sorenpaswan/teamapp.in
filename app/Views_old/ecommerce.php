<?php require_once(APPPATH . 'views/header/header.php'); ?>      
<?php require_once(APPPATH . 'views/header/headerpage.php'); ?>
<div class="ecommerce-tab">
    <div id="wpcont-2-outer">

        <div class="wpcont-2 clearfix">
            <h2> <span> Open Source eCommerce Application Development </span></h2>
            <p><span>With our expert group of devoted eCommerce Web Developers, we can distinguish the correct open-source model fitting to your selling requirements and afterward actualize it easily in the wake of altering everything-from topic improvement to premium layout mix and shopping basket customization to module combination according to your needs.</span></p>
            <div class="development-tab-section">
                <div id="horizontalTab">
                    <div class="ui-tab">
                        <ul class="resp-tabs-list">
                            <li><a href="javascript:;" class="tabLink tab_icon1" id="conts-8"><span><img src="<?php echo base_url('public/img/img/tab1.png'); ?>" alt=""><img src="<?php echo base_url('img/img/tab1-active.png'); ?>" alt="" class="tab-none"><p>Magento Community Edition Development</p></span></a></li>
                            <li><a href="javascript:;" class="tabLink " id="conts-9"><span><img src="<?php echo base_url('public/img/img/tab2.png'); ?>" alt=""><img src="<?php echo base_url('public/img/img/tab2-active.png'); ?>" alt="" class="tab-none"><p>Prestashop eCommerce Website Development & Customization</p></span></a></li>
                            <li><a href="javascript:;" class="tabLink " id="conts-10"><span><img src="<?php echo base_url('public/img/img/tab3.png'); ?>" alt=""><img src="<?php echo base_url('public/img/img/tab3-active.png'); ?>" alt="" class="tab-none"><p>OsCommerce Development & Customization</p></span></a></li>
                            <li><a href="javascript:;" class="tabLink " id="conts-11"><span><img src="<?php echo base_url('public/img/img/tab4.png'); ?>" alt=""><img src="<?php echo base_url('public/img/img/tab4-active.png'); ?>" alt="" class="tab-none"><p>Drupal Commerce Development - Ubercart</p></span></a></li>
                            <li><a href="javascript:;" class="tabLink " id="conts-12"><span><img src="<?php echo base_url('public/img/img/tab5.png'); ?>" alt=""><img src="<?php echo base_url('public/img/img/tab5-active.png'); ?>" alt="" class="tab-none"><p>Joomla VirtueMart Development</p></span></a></li>
                            <li><a href="javascript:;" class="tabLink " id="conts-13"><span><img src="<?php echo base_url('public/img/img/tab6.png'); ?>" alt=""><img src="<?php echo base_url('public/img/img/tab6-active.png'); ?>" alt="" class="tab-none"><p>Woocommerce Development</p></span></a></li>
                        </ul>
                    </div> 
                    <div class="resp-tabs-container clearfix">
                        <div class="services-tabcontent hide" id="conts-8-1">
                            <div class="magento-left">
                                <h3><span>Magento CE</span> Ecommerce Development & Customization </h3>
                                <p>We unite the flexibility of Magento open-source development (CE) with our significant specific inclination to give shippers full authority over their online store exercises. Our Magento Ecommerce Development aptitude incorporates-</p>
                                <div class="sign-box">
                                    <ul>
                                        <li>Custom Theme Designing & Plugin Integration</li>
                                        <li>Css/HTML to coding theme conversion</li>

                                        <li>Module Development</li>
                                        <li> Search engine optimization</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="MAGENTO-RIGHT">
                                <img src="<?php echo base_url('public/img/img/magento-main.png'); ?>" alt="">
                            </div>

                        </div>



                        <div class="services-tabcontent" id="conts-9-1">


                            <div class="magento-left">
                                <h3>OsCommerce Development & Customization</h3>
                                <h5></h5>
                                <p>Our professional osCommerce developers carry years of hands-on experience in putting together a feature-rich online store at competitive pricing. We specialize in -</p>
                                <div class="sign-box">
                                    <ul>
                                        <li>Custom Template Designing</li>
                                        <li>Module integration</li>
                                        <li>Site maintenance</li>
                                        <li>Product Catalogue Design & admin panel customization</li>
                                        <li>Shipping & Payment Gateway configuration</li>
                                        <li>Tracking order status and managing the whole inventory.</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="MAGENTO-RIGHT">
                                <img src="<?php echo base_url('public/img/img/OsCommerce.png'); ?>" alt="">
                            </div>
                        </div>









                        <div class="services-tabcontent hide" id="conts-10-1">
                            <div class="magento-left">
                                <h3>Drupal Commerce Development - Ubercart</h3>
                                <h5></h5>
                                <p>If you need an online shop that manages both products & site content with utmost ease, Drupal commerce is perfect for you. Over the years, we have delivered some of the most stable, flexible & fast web stores using Ubercart platform that offer enterprise-level scalability. </p>

                                <div class="sign-box">
                                    <ul>
                                        <li>Drupal commerce theme design & development </li>
                                        <li>Module Integration</li>
                                        <li>Advanced customer management</li>
                                        <li>Integration of payment process & 3rd party external systems</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="MAGENTO-RIGHT">
                                <img src="<?php echo base_url('public/img/img/drupal.png'); ?>" alt="">
                            </div>


                        </div>





                        <div class="services-tabcontent hide" id="conts-11-1">






                            <div class="magento-left">
                                <h3>Woocommerce Development</h3>
                                <h5></h5>
                                <p>We are committed to implement and customize the Woocommerce platform to build easy-to-use online stores for you. </p>

                                <div class="sign-box">
                                    <ul>
                                        <li>Custom Woocommerce theme design</li>
                                        <li>Convert PSDs to Woocommerce templates</li>
                                        <li>Extensions Development & Pre-built module integration</li>
                                        <li>Migration of online shop from any other ecommerce platform</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="MAGENTO-RIGHT">
                                <img src="<?php echo base_url('public/img/img/Woocommerce.png'); ?>" alt="">
                            </div>


                        </div>







                        <div class="services-tabcontent hide" id="conts-12-1">
                            <div class="magento-left">
                                <h3>Joomla VirtueMart Development</h3>
                                <h5></h5>
                                <p>VirtueMart is an extension of Joomla CMS which is best suited for ecommerce websites targeting low to medium level traffic. Our team specializes in </p>

                                <div class="sign-box">
                                    <ul>
                                        <li>Custom Joomla programming, front & back end implementation </li>
                                        <li>Site management & maintenance</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="MAGENTO-RIGHT">
                                <img src="<?php echo base_url('public/img/img/joomla.png'); ?>" alt="">
                            </div>
                        </div>


                        <div class="services-tabcontent hide" id="conts-13-1">


                            <div class="magento-left">
                                <h3>Prestashop eCommerce Website Development & Customization</h3>
                                <h5>With our esteemed status of one of the most reliable eCommerce website development agency in India, we strive to create user-friendly stores for different devices. So far, we have successfully created 100+ eCommerce websites on Prestashop.</h5>
                                <p>Our Prestashop team of eCommerce web developers ensures to deliver customized websites and web applications using LAMP (Linux, Apache, My SQL, PHP) stack for catering varied requirements of our patrons. In India, as compared with other eCommerce development companies, our team has successfully accomplished challenging PHP projects of ecommerce web development for users across the globe using latest tools and coding practices.</p>
                                <div class="sign-box">
                                    <ul>
                                        <li>One-click installation</li>
                                        <li>Integrating high-quality eCommerce templates</li>
                                        <li>Configuring shipping options</li>
                                        <li>Setting-up payment processing solutions</li>
                                        <li>Custom module development & integration</li>
                                        <li>Back-end (CMS) customization </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="MAGENTO-RIGHT">
                                <img src="<?php echo base_url('public/img/img/Prestashop.png'); ?>" alt="">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
<div id="wpcont3-outer">
    <!-- <div class="container clearfix">
      <h3>For Dedicated ecommerce Development Services</h3>
      <div class="request-seo"><a href="hire-php-programmer.html">Hire PHP Developer</a></div> -->
</div>
</div>
<div id="wpcont7-outer" class="php-color-bg">
    <div class="container clearfix">
        <div class="wpcont7left">
            <h2>Magento eCommerce Development </h2>
            <p>We are pleased to appreciate the status of the main Magento Service Company of India with a reasonable vision of conveying adaptable and adaptable eCommerce web architectures to abroad retailers and brands. Our Magento expert works in PHP, subject mix masters and cloud specialists having noteworthy learning of in tweaking on the web stores for speed, execution, load modifies, and security.</p>
            <h3>Our Magento Developers</h3>
            <ul class="col-left">
                <li>Boost up Performance</li>
                <li>Theme development for eCommerce websites and Customization</li>
                <li> Template Integration</li>
                <li>CMS Development</li>
                <li>ERP & 3rd party API Integration</li>
            </ul>

            <h3>Responsive Ecommerce Development</h3>
            <p>Do you have one of a kind thought and stressed that open source web-based business applications may not legitimize your prerequisite. TECHMODE India has an incredible group of custom web-based business engineers who can construct a completely custom web-based business site from Scratch as special as you thought. Get in touch with us for the top of the line, adaptable and adaptable custom online business website.</p>

        </div>
        <div class="wpcont7left">
            <h2> eCommerce - solving Shopping Carts</h2>
            <p>On the off chance that you intend to run a little online shop inside reasonable ease speculation by selecting a trustworthy eCommerce web development company, you can go with our SAAS - a cloud-based conveyance model where your online store is managed & hosted by your specialist provider and you have to pay them on a membership premise.</p>
            <h3>Shopify eCommerce Development</h3>
            <p>Being a presumed eCommerce website development agency of India, we have a team of extremely talented eCommerce software engineers in India who have involvement with doing custom Shopify advancement, execution, and customization work. We can even take your PSD plans and convert it into Shopify formats or skins utilizing our top to bottom mastery in LIQUID programming (a language explicit to Shopify stage). Our eCommerce specialists additionally offer store-redesigning & migration services for Shopify.</p>
            <h3>Bigcommerce</h3>
            <p>We design completely redid eCommerce site on Bigcommerce to meet your selective needs. Our Bigcommerce API development experts pursue streamlined procedures to make custom store designs, coordinate modules, drive traffic, and improve code structure and site execution. On the off chance that you previously run an online shop and need to move it to Bigcommerce through a presumed eCommerce improvement office India, procure our group of gifted eCommerce designers to prepare a gainful portable site.</p>
        </div>
    </div>
</div>
<div class="e-commerce-benefits">
    <div id="wpcont4-outer" class="iphone-bg">
        <div class="container clearfix">
            <h2><span> Some Business Benefits of by using our eCommerce Development Services</span></h2>
            <div class="cont4-left col_new1">
                <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/wordpressright.png'); ?>" alt="superior quality design">
                    <h3>Our eCommerce experts provide professional assistance</h3>
                    <p>Our eCommerce advisors investigate each specialized viewpoint to propose flawless eCommerce web improvement arrangement fitting to your needs, assistance dispatch your online store and guide how to advance it and increment deals - everything inside your characterized spending plan.</p>
                </div>
                <!--Wpsite-box-end-->
                <!--Wpsite-box-end-->
                <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/wordpressright.png'); ?>" alt="technical support">
                    <h3>We integrate premium eCommerce themes as per you</h3>
                    <p>The first impression is the last impression. In the event that you pick our eCommerce site development administrations, you can either get one of a kind and lovely eCommerce formats explicitly intended for your business or browse premium layouts according to your necessities.</p>
                </div>
                <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/wordpressright.png'); ?>" alt="version updates">
                    <h3>You can manage your online store with easy steps</h3>
                    <p>If you choose to take eCommerce website designing services from us, you can manage everything on your own from images & videos to product descriptions, pricing, inventory, content, marketing & more with a user-friendly CMS that we deliver with every eCommerce store.</p>
                </div>
                <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/wordpressright.png'); ?>" alt="version updates">
                    <h3>Integrating payment gateways to accept online payment</h3>
                    <p>We integrate payment gateways to your e-commerce applications of all major service providers like PayPal, EBS,  and CCAvenue so that your online store can accept credit cards, debit cards & netbanking payments in very ease.</p>
                </div>


                <!--Wpsite-box-end-->
            </div>
            <div class="cont4-right col_new1">
                <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/wordpressright.png'); ?>" alt="unique functionality">
                    <h3>Convinient check out process</h3>
                    <p>We make website very convenient so that end user can easily check out the product from the site</p>
                </div>
                <!--Wpsite-box-end-->
                <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/wordpressright.png'); ?>" alt="updates">
                    <h3>SEO optimized website</h3>
                    <p>With  Search engine optimization, our online stores are on the top of the search engines. That's a help to boost up the business. </p>
                </div>
                <!--Wpsite-box-end-->
                <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/wordpressright.png'); ?>" alt="mobile optimised UI">
                    <h3>Backend or technical support</h3>
                    <p>We realize that your online store needs to sell nonstop and so being a trusted eCommerce website architecture organization of India & worldwide; we give specialized help to every one of our customers even in the most peculiar hours.</p>
                </div>
                <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/wordpressright.png'); ?>" alt="mobile optimised UI">
                    <h3>Anytime & anywhere via your responsive application</h3>
                    <p>We develop the application with responsive UI so that you don&#39;t lose sales from your mobile traffic.</p>
                </div>
                <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/wordpressright.png'); ?>" alt="version updates">
                    <h3>Making easy points to communicate</h3>
                    <p>To boost engagement, we guarantee your customers stay by coordinating strong calls-to-action; immaculate web-based life sign and conversion-optimized retail website structures for an eCommerce site.</p>
                </div>
                <!-- <div class="Wpsite-box"> <img src="image/Not-just-an-eCommerce-website-you-get-a-personalized-online-presence.jpg" alt="mobile optimised UI">
                    <h3>Not just an eCommerce website, you get a personalized online presence</h3>
                    <p>When compared with other reputed eCommerce web development companies over the internet, our eCommerce web design services are focused to help you sell more. You get the most effective online selling tool fully loaded with advanced features, rich shopping cart design, awesome functionality & integrated social media signals.
             </p>
                  </div> -->

                <!--Wpsite-box-end-->
            </div>
        </div>
    </div></div>
<!--form start here-->
<div style="display:none;" class="pop_up_start_hear">
    <div id="hire-dedicated-resource" >
        <button onClick="$.fn.custombox('close');" class="close" type="button"></button>
        <div class="excited-talk">
            <h2>Excited! Let's Talk</h2>
            <h3><span>We discuss because We Care</span></h3>

            <!--form-main-start-here-->
            <div class="apply_form">

                <!--sucess-massage-start-here-->
                <div id="sucess-message">
                    <h1>Thank You!</h1>
                    <p>Hi,
                        We'll get back to you very soon.
                    </p>
                </div>
                <!--sucess-massage-start-here-->
                <!--form-start-here-->
                <div id="hd_form">
                    <form action="" method="post" id="contact-form" name="contact-form" >

                        <div class="apply_singal_row">
                            <label>Name*</label>
                            <input type="text" placeholder="Enter Your Name"  id="name" name="name" class="name-icon">
                        </div>
                        <!--div-start-here-->
                        <div class="apply_singal_row row-float">
                            <label>Email*</label>
                            <input type="text" placeholder="Enter Your Email" id="email" name="email" class="email-icon">
                        </div>
                        <!--div-end-here-->


                        <div class="apply_main">
                            <div class="apply_singal_row resp-row"> 
                                <div class="total-e">
                                    <label>Mobile*</label>
                                    <input type="text" placeholder="Enter Mobile No." id="time" name="phone" class="best-time">

                                </div>
                                <!--div-start-here-->
                                <div class="total-e row-float">
                                    <label>Skype</label>
                                    <input type="text" placeholder="Enter Skype ID" id="contact" name="skype" class="contas-icon">
                                </div>
                            </div>

                            <div class="apply_singal_row row-float resp-row">
                                <label>Country</label>
                                <div class="">
                                    <div class="bfh-selectbox">
                                        <input name="country" value="" type="hidden">
                                        <a class="bfh-selectbox-toggle" role="button" data-toggle="bfh-selectbox" href="#">
                                            <p class="bfh-selectbox-option input-medium wid_80" data-option="1">Select Your Country</p>
                                            <b class="arrow-down"></b> </a>
                                        <div class="bfh-selectbox-options">
                                            <div role="listbox">
                                                <ul class="option">
                                                    <li><a tabindex="-1" href="#" data-option="">Country...</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Afganistan">Afghanistan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Albania">Albania</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Algeria">Algeria</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="American Samoa">American Samoa</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Andorra">Andorra</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Angola">Angola</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Anguilla">Anguilla</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Antigua &amp; Barbuda">Antigua &amp; Barbuda</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Argentina">Argentina</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Armenia">Armenia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Aruba">Aruba</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Australia">Australia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Austria">Austria</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Azerbaijan">Azerbaijan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Bahamas">Bahamas</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Bahrain">Bahrain</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Bangladesh">Bangladesh</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Barbados">Barbados</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Belarus">Belarus</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Belgium">Belgium</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Belize">Belize</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Benin">Benin</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Bermuda">Bermuda</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Bhutan">Bhutan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Bolivia">Bolivia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Bonaire">Bonaire</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Botswana">Botswana</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Brazil">Brazil</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="British Indian Ocean Ter">British Indian Ocean Ter</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Brunei">Brunei</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Bulgaria">Bulgaria</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Burkina Faso">Burkina Faso</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Burundi">Burundi</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Cambodia">Cambodia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Cameroon">Cameroon</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Canada">Canada</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Canary Islands">Canary Islands</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Cape Verde">Cape Verde</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Cayman Islands">Cayman Islands</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Central African Republic">Central African Republic</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Chad">Chad</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Channel Islands">Channel Islands</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Chile">Chile</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="China">China</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Christmas Island">Christmas Island</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Cocos Island">Cocos Island</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Colombia">Colombia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Comoros">Comoros</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Congo">Congo</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Cook Islands">Cook Islands</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Costa Rica">Costa Rica</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Cote DIvoire">Cote D'Ivoire</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Croatia">Croatia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Cuba">Cuba</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Curaco">Curacao</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Cyprus">Cyprus</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Czech Republic">Czech Republic</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Denmark">Denmark</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Djibouti">Djibouti</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Dominica">Dominica</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Dominican Republic">Dominican Republic</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="East Timor">East Timor</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Ecuador">Ecuador</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Egypt">Egypt</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="El Salvador">El Salvador</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Equatorial Guinea">Equatorial Guinea</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Eritrea">Eritrea</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Estonia">Estonia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Ethiopia">Ethiopia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Falkland Islands">Falkland Islands</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Faroe Islands">Faroe Islands</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Fiji">Fiji</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Finland">Finland</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="France">France</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="French Guiana">French Guiana</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="French Polynesia">French Polynesia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="French Southern Ter">French Southern Ter</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Gabon">Gabon</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Gambia">Gambia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Georgia">Georgia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Germany">Germany</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Ghana">Ghana</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Gibraltar">Gibraltar</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Great Britain">Great Britain</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Greece">Greece</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Greenland">Greenland</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Grenada">Grenada</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Guadeloupe">Guadeloupe</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Guam">Guam</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Guatemala">Guatemala</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Guinea">Guinea</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Guyana">Guyana</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Haiti">Haiti</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Hawaii">Hawaii</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Honduras">Honduras</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Hong Kong">Hong Kong</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Hungary">Hungary</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Iceland">Iceland</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="India">India</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Indonesia">Indonesia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Iran">Iran</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Iraq">Iraq</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Ireland">Ireland</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Isle of Man">Isle of Man</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Israel">Israel</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Italy">Italy</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Jamaica">Jamaica</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Japan">Japan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Jordan">Jordan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Kazakhstan">Kazakhstan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Kenya">Kenya</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Kiribati">Kiribati</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Korea North">Korea North</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Korea Sout">Korea South</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Kuwait">Kuwait</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Kyrgyzstan">Kyrgyzstan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Laos">Laos</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Latvia">Latvia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Lebanon">Lebanon</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Lesotho">Lesotho</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Liberia">Liberia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Libya">Libya</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Liechtenstein">Liechtenstein</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Lithuania">Lithuania</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Luxembourg">Luxembourg</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Macau">Macau</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Macedonia">Macedonia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Madagascar">Madagascar</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Malaysia">Malaysia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Malawi">Malawi</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Maldives">Maldives</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Mali">Mali</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Malta">Malta</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Marshall Islands">Marshall Islands</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Martinique">Martinique</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Mauritania">Mauritania</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Mauritius">Mauritius</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Mayotte">Mayotte</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Mexico">Mexico</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Midway Islands">Midway Islands</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Moldova">Moldova</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Monaco">Monaco</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Mongolia">Mongolia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Montserrat">Montserrat</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Morocco">Morocco</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Mozambique">Mozambique</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Myanmar">Myanmar</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Nambia">Nambia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Nauru">Nauru</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Nepal">Nepal</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Netherland Antilles">Netherland Antilles</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Netherlands">Netherlands (Holland, Europe)</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Nevis">Nevis</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="New Caledonia">New Caledonia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="New Zealand">New Zealand</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Nicaragua">Nicaragua</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Niger">Niger</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Nigeria">Nigeria</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Niue">Niue</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Norfolk Island">Norfolk Island</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Norway">Norway</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Oman">Oman</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Pakistan">Pakistan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Palau Island">Palau Island</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Palestine">Palestine</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Panama">Panama</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Papua New Guinea">Papua New Guinea</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Paraguay">Paraguay</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Peru">Peru</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Phillipines">Philippines</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Pitcairn Island">Pitcairn Island</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Poland">Poland</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Portugal">Portugal</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Puerto Rico">Puerto Rico</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Qatar">Qatar</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Republic of Montenegro">Republic of Montenegro</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Republic of Serbia">Republic of Serbia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Reunion">Reunion</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Romania">Romania</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Russia">Russia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Rwanda">Rwanda</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="St Barthelemy">St Barthelemy</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="St Eustatius">St Eustatius</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="St Helena">St Helena</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="St Kitts-Nevis">St Kitts-Nevis</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="St Lucia">St Lucia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="St Maarten">St Maarten</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Saipan">Saipan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Samoa">Samoa</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Samoa American">Samoa American</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="San Marino">San Marino</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Sao Tome &amp; Principe">Sao Tome &amp; Principe</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Saudi Arabia">Saudi Arabia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Senegal">Senegal</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Serbia">Serbia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Seychelles">Seychelles</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Sierra Leone">Sierra Leone</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Singapore">Singapore</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Slovakia">Slovakia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Slovenia">Slovenia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Solomon Islands">Solomon Islands</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Somalia">Somalia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="South Africa">South Africa</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Spain">Spain</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Sri Lanka">Sri Lanka</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Sudan">Sudan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Suriname">Suriname</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Swaziland">Swaziland</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Sweden">Sweden</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Switzerland">Switzerland</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Syria">Syria</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Tahiti">Tahiti</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Taiwan">Taiwan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Tajikistan">Tajikistan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Tanzania">Tanzania</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Thailand">Thailand</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Togo">Togo</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Tokelau">Tokelau</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Tonga">Tonga</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Trinidad &amp; Tobago">Trinidad &amp; Tobago</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Tunisia">Tunisia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Turkey">Turkey</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Turkmenistan">Turkmenistan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Turks &amp; Caicos Is">Turks &amp; Caicos Is</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Tuvalu">Tuvalu</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Uganda">Uganda</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Ukraine">Ukraine</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="United Arab Erimates">United Arab Emirates</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="United Kingdom">United Kingdom</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="United States of America">United States of America</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Uraguay">Uruguay</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Uzbekistan">Uzbekistan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Vanuatu">Vanuatu</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Vatican City State">Vatican City State</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Venezuela">Venezuela</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Vietnam">Vietnam</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Virgin Islands (Brit)">Virgin Islands (Brit)</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Virgin Islands (USA)">Virgin Islands (USA)</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Wake Island">Wake Island</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Wallis &amp; Futana Is">Wallis &amp; Futana Is</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Yemen">Yemen</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Zaire">Zaire</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Zambia">Zambia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Zimbabwe">Zimbabwe</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!--div-end-here-->








                        <div class="apply_singal_row">
                            <label>Website</label>
                            <input type="text" placeholder="Website URL" id="company" name="website" class="company-icon">
                        </div>        
                        <!--div-start-here-->
                        <input type="hidden" name="page" value="e-commerce-website-development" >

                        <div class="apply_main">
                            <div class="apply_singal_row txtarea">
                                <label>Requirement*</label>

                                <textarea rows="10" cols="5" placeholder="Enter Your Requirement" id="requirement" name="requirement" class="mes-icon"></textarea>
                            </div>
                        </div>
                        <div class="apply_singal_row ">
                            <div id="slider_full" class="sl_g_fh"><img src="captcha6c37.html?rand=2124052013" id='captchaimg'><br>
                                <label for='message'>Enter the code above here :</label>
                                <br>
                                <input id="captcha_code" name="captcha_code" type="text">
                                <br>
                                <a href='javascript: refreshCaptcha();'>Refresh</a></div>
                        </div>
                        <!--div-end-here-->
                        <!--submit-button-start-here-->
                        <div class="apply_singal_row ne_c_r_r ">
                            <input type="submit" name="Submit" value="Let's Discuss">
                            <br>
                            <p>We will not share your details with anyone. </p>
                        </div>

                        <!--submit-button-end-here-->




                    </form>
                </div>
                <!--form-end-here-->

            </div>
            <!--form-main-end-here-->


        </div>
    </div>

</div>
<!--form end here-->

<?php require_once(APPPATH . 'views/footer/footer.php'); ?>