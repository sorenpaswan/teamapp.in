<?php require_once(APPPATH . 'views/header/header.php'); ?>

        <meta name="Description" content="Hire your own virtual team of full time PHP developers expert in PHP programming, Ajax, eCommerce solutions, custom website & apps development." />
        <meta name="robots" content="index, follow" />
 
  
  <?php require_once(APPPATH . 'views/header/headerpage.php'); ?>
    
 
        
<div class="hire-analist-outer">
<div class="hire-analist">
<h1>Strategy</h1>
<p></p>
<!--left-part-start-->
<div class="hire-analist-left">
<ul>
<li>
 <img src="<?php echo base_url('public/images/wordpressright.png');?>" alt="image">
  <p>Consultancy </p>
</li>
<li>
  <img src="<?php echo base_url('public/images/wordpressright.png');?>" alt="image">
  <p>Ideation </p>
</li>
<li>
  <img src="<?php echo base_url('public/images/wordpressright.png');?>" alt="image">
  <p>Product Roadmap</p>
</li>
</ul>
</div>
<div class="hire-analist-right">
  <ul>
    <li>
      <img src="<?php echo base_url('public/images/wordpressright.png');?>" alt="image">
      <p>Product Roadmap</p>
</li>
<li>
  <img src="<?php echo base_url('public/images/wordpressright.png');?>" alt="image">
  <p>Idea Validation</p>
</li>
<li>
  <img src="<?php echo base_url('public/images/wordpressright.png');?>" alt="image">
  <p>Stakeholders</p>
    </li>
  </ul>
</div>
<!--right-part-end-->
</div>
</div>
<div id="wpcont-1-outer">
  <div class="container clearfix">
    <div class="wpcont-1-left-seo">
      
      <h2> Project stakeholder or Customers</h2>
      <p>The project Customer or stakeholder is the person or group responsible for initiating the web site project. In most instances the Stakeholder is the client or customer for the web site development work, but in smaller in-house department projects the sponsoring manager and the web project manager may be the same person. The customers provides the overall strategic vision and purpose for the site development project, approves the contract or work plan. He is responsible for the budget and project schedule, and provides the resources to support the work of the website development team.

The stakeholder is the client the team works to please, but sponsors have critical work to perform as part of the overall site development team. Customer act as a liaison to the rest of the sponsoring organization, provide critical domain expertise, coordinate with the larger goals of the sponsoring organization, and deliver Website content and domain expertise to the project. As such, it is critical that customer and other stakeholders understand their responsibilities to the web team: late delivery of web site content is the most common cause of blown schedules in web development projects. Sponsors also are typically responsible for third-party or external content contracts, other media licensing negotiations, and coordination with other marketing, information technology, and communications efforts at the sponsoring organization or company.</p>
      <!--<div class="request-button"> <a href="hire-seo-consultant.html" class="request-btn ppc">Hire SEO Expert </a></div>-->
    </div>
     <div class="wpcont-1-right"> <br><br><img src="<?php echo base_url('public/about-img/blog-img-1.png');?>" alt="about-bg" class="thumbnail image"></div>
  </div>
</div>
<!--end-->
<div class="clear"></div>
<div style="display: none;" id="slidingDiv" >
<div class="slidingDiv">
  <div class="toogle-close">
  <a href="#" onClick="showSlidingDiv(); return false;"><img src="<?php echo base_url('public/img/close-btn01.png');?>" alt="Close button"></a></div>
  <div class="slide-popup-box">
  <h4>We'll call you soon</h4>
  <p>leave your details</p>
  <div class="lets-talk-out"> </div>
    <div class="slide-popup-box-form-main">
      <div class="slide-popup-box-form">
      
   

           <form method="POST" id="lets-talk-frm" action="<?php echo base_url('contactMe') ;?>">
                        @csrf
        <input type="text"  name="first_name" placeholder="First Name:" >
              <input type="text"  name="last_name" placeholder="Last Name:"  >

        <input type="text"  name="email" placeholder="Email:" >
        <input type="text"  name="address" placeholder="Address" >
        <input type="text"  name="mobile" placeholder="Mobile:" >
        <input type="hidden" name="slider_unlock" value="02" >
        <input type="text"  name="description" placeholder="Description" id="description" >
          <div id="slider_full_1"></div>
        <input type="submit" id="lets-talk" value="submit" name="submit">
        </form>
        </div>
        
      </div>
    </div>
  </div>
</div>

 <div id="letstalk-wrap" class="clearfix">
    <div class="container clearfix">
      <div class="lets-talk-row clearfix">
       <h4>Let's Talk</h4>
        <p>We'd love to answer any questions you may have. Contact us and discuss your business objectives & we will let you know how we can help along with a Free Quote.</p>
      <ul>
          <li><span></span>
            <figure><img alt="give us call" src="<?php echo base_url('public/images/roundphn.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
      
            <p><small>Give us a call</small>+91 92501 34134</p>
          </li>
          <li><span></span>
            <figure><img alt="write to us" src="<?php echo base_url('public/images/roundmsg.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
            <p><small>Write to us</small><a href="mailto:info@techmode.in">info@techmode.in</a></p>
          </li>
          <li><span></span>
            <figure><img alt="video chat with us" src="<?php echo base_url('public/images/skype.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
            <p><small>Video chat with us</small>Techmode</p>
          </li>
          <li>
      <div class="toogle-part">
      <a href="#" onClick="showSlidingDiv(); return false;">
      <span></span>
              <figure><img alt="leave your details" src="<?php echo base_url('public/images/mobile.png');?>"></figure>
              <div class="ani_bg-2"> </div>
              <div class="ani_bg"></div>
              <p><small>Have us call you</small>leave your detail</p>
        </a>
        </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
  
 <?php require_once(APPPATH . 'views/footer/footer.php'); ?>
