<?php require_once(APPPATH . 'views/header/header.php'); ?>

<meta name="Description" content="Hire your own virtual team of full time PHP developers expert in PHP programming, Ajax, eCommerce solutions, custom website & apps development." />
<meta name="robots" content="index, follow" />       

<?php require_once(APPPATH . 'views/header/headerpage.php'); ?>
<div class="your-apps">
    <div class="app-continer">
        <h1>Website Design Services</h1>
        <p>Give your website an amazing UI & conversion potential with our customer centric website design that works on all devices.</p>
        <a href="<?php echo base_url('hire-dedicated-resource'); ?>" class="request-btn">Request a Free Quote</a> </div>
</div>
<div class="web-responsive">
    <div class="res-part clearfix">
        <div class="res-portfolio">
            <h2>Beautiful</h2>
            <h5>Beautiful websites give better user experience</h5>
            <img src="<?php echo base_url('public/images/think-it.jpg'); ?>" alt="better user experience">
            <p>Unique web design with interactive UI that have better user experience, lesser bounce rates, stand out to give you an edge over competitors & have that wow factor can compel your visitors to convert.  Our creative website designing team spends time to understand your business objectives & target niche before starting the designing process.</p>
        </div>
        <div class="res-portfolio row-float">
            <h2>Responsive</h2>
            <h5>User friendly websites for multi device world</h5>
            <img src="<?php echo base_url('public/images/web-portfolio-img.jpg'); ?>" alt="user friendly website">
            <p>More than 70% of your website visitors are now using mobile. Our world class web design company include fully responsive websites with intelligent user interface that adapts to varied screen resolutions and deliver an amazing user experience regardless of the device and browsing environment.</p>
        </div>
        <div class="clear">
            <div class="res-portfolio">
                <h2>Effective </h2>
                <h5>Make your website your best sales person</h5>
                <img src="<?php echo base_url('public/images/for-sale.jpg'); ?>" alt="effective website">
                <p>Your website is the high spot of all your marketing efforts & we strive to make it effective so that it can speak directly to your potential customers & generate new leads. Our web design team visualizes the site with sales perspective to build a website that not just looks great but can convert visitors into customers.</p>
            </div>
            <div class="res-portfolio row-float">
                <h2>Customized</h2>
                <h5>Websites that connect well, perform well</h5>
                <img src="<?php echo base_url('public/images/buzzy.jpg'); ?>" alt="customize website">
                <p>When it comes to your marketing dollar, you need a solid digital strategy customized to your specific business requirements & a well-structured web design in line with your marketing plan. Our affordable web design service is carried out by an experienced team who spends 	time understanding your industry and target market before starting the web designing process.</p>
            </div>
        </div>
    </div>
    <!--blue-part-strat-->
    <div>
        <div id="wpcont3-outer">
            <div class="container clearfix">
                <h3>Get your website a digital makeover</h3>
                <div class="request-seo"><a href="<?php echo base_url('request-a-proposalf294.html'); ?>?p=designer">Get Started </a></div>
            </div>
        </div>
    </div>
    <!--blue-part-end-->
    <div class="hire-dect-dgn">
        <div class="container clearfix">
            <div class="hire-dedticed-box"> <img src="<?php echo base_url('public/images/hire-title.jpg'); ?>" alt="hire dedicated designer">
                <div class="hour-panel">
                    <div class="hour-panel-left">
                        <p>Hourly</p>
                        <h2><span><sup>$</sup></span>12</h2>
                    </div>
                    <div class="hour-panel-left monthly">
                        <p>monthly</p>
                        <h2><span><sup>$</sup></span>1500</h2>
                    </div>
                    <div class="month-get-start"><a href="<?php echo base_url('hire-dedicated-resource'); ?>" class="request-btn">Get started <img src="<?php echo base_url('images/get-arrow.png'); ?>" alt="hire a designer"> </a></div>
                </div>
            </div>
            <div class="hire-dedticed-content">
                <p>If you have multiple projects or one large on-going project you are planning to launch or want to have a design makeover, then opting for our web design company can save you time and overhead costs.<br>
                    <br>
                    Our creative design team have 120+ web design experts having years of hands on experience with user interface web designing for desktop & mobile users. Like our 100+ happy clients, <a href="<?php echo base_url('hire-website-designer'); ?>">hiring full time web designers</a> over an in-house team can benefit you for many reasons:</p>
                <div class="webdesign-list">
                    <ul>
                        <li> Zero Spend on Infrastructure</li>
                        <li> Long term cost saving</li>
                        <li> Hand-picked experienced team</li>
                    </ul>
                    <ul>
                        <li> Full control & transparency</li>
                        <li> Direct monitoring on IM, email & calls</li>
                        <li> Transparent billing system</li>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>
            <div class="hire-continer-part">
                <ul>
                    <li class="hire-quilty"> <figure><img src="<?php echo base_url('public/images/satifaction.jpg'); ?>" alt="Quality guaranteed"></figure> Quality<br>
                        Guaranteed </li>
                    <li> <figure><img src="<?php echo base_url('public/images/proficent-img.jpg'); ?>" alt="proficient designer"> </figure> Proficient <br>
                        Designer </li>
                    <li> <figure><img src="<?php echo base_url('public/images/assured-img.jpg'); ?>" alt="assured replacement"></figure> Assured <br>
                        Replacement </li>
                    <li class="hire-quiltys"> <figure><img src="<?php echo base_url('public/images/frquent-img.jpg'); ?>" alt="frequent update"></figure> Frequent <br>
                        Update </li>
                    <li class="hire-quiltys"> <figure><img src="<?php echo base_url('public/images/great-community.jpg'); ?>" alt="Great support"></figure> Great <br>
                        Support </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="meet-our-team">
        <h2>Our Latest Web Design Work</h2>
    </div>
    <div class="our-latest-work">
        <ul id="protfilio-list">
            <img class="loader" src="<?php echo base_url('images/loader.gif'); ?>" style="opacity:0.7;align:center;"/>
        </ul>
    </div>
    <!--form start here-->
    <div style="display:none;" class="pop_up_start_hear">
        <div id="hire-dedicated-resource" >
            <script>

                $("#contact-form").validate({
                    rules: {
                        name: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        //	skype: "required",
                        country: "required",
                        requirement: "required",
                        phone: {
                            required: true,
                            digits: true,
                            minlength: 7
                        },
                        captcha_code: {
                            required: true,
                            remote: {
                                url: "captchav.php?t=" + $("#captcha_code").val(),

                            }
                        }
                    },
                    messages: {
                        name: '',
                        email: '',
                        requirement: '',
                        phone: '',
                        captcha_code: '',

                    },
                });
            </script>
            <button onclick="$.fn.custombox('close');" class="close" type="button"></button>
            <div class="excited-talk">
                <h2>Excited! Let's Talk</h2>
                <h3><span>We discuss because We Care</span></h3>

                <!--form-main-start-here-->
                <div class="apply_form">

                    <!--sucess-massage-start-here-->
                    <div id="sucess-message">
                        <h1>Thank You!</h1>
                        <p>Hi,
                            We'll get back to you very soon.
                        </p>
                    </div>
                    <!--sucess-massage-start-here-->
                    <!--form-start-here-->
                    <div id="hd_form">
                        <form action="https://www.tisindia.com/portfolio/formhandel/contactus" method="post" id="contact-form" name="contact-form" >

                            <div class="apply_singal_row">
                                <label>Name*</label>
                                <input type="text" placeholder="Enter Your Name"  id="name" name="name" class="name-icon">
                            </div>
                            <!--div-start-here-->
                            <div class="apply_singal_row row-float">
                                <label>Email*</label>
                                <input type="text" placeholder="Enter Your Email" id="email" name="email" class="email-icon">
                            </div>
                            <!--div-end-here-->


                            <div class="apply_main">
                                <div class="apply_singal_row resp-row"> 
                                    <div class="total-e">
                                        <label>Mobile*</label>
                                        <input type="text" placeholder="Enter Mobile No." id="time" name="phone" class="best-time">

                                    </div>
                                    <!--div-start-here-->
                                    <div class="total-e row-float">
                                        <label>Skype</label>
                                        <input type="text" placeholder="Enter Skype ID" id="contact" name="skype" class="contas-icon">
                                    </div>
                                </div>

                                <div class="apply_singal_row row-float resp-row">
                                    <label>Country</label>
                                    <div class="">
                                        <div class="bfh-selectbox">
                                            <input name="country" value="" type="hidden">
                                            <a class="bfh-selectbox-toggle" role="button" data-toggle="bfh-selectbox" href="#">
                                                <p class="bfh-selectbox-option input-medium wid_80" data-option="1">Select Your Country</p>
                                                <b class="arrow-down"></b> </a>
                                            <div class="bfh-selectbox-options">
                                                <div role="listbox">
                                                    <ul class="option">
                                                        <li><a tabindex="-1" href="#" data-option="">Country...</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Afganistan">Afghanistan</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Albania">Albania</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Algeria">Algeria</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="American Samoa">American Samoa</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Andorra">Andorra</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Angola">Angola</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Anguilla">Anguilla</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Antigua &amp; Barbuda">Antigua &amp; Barbuda</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Argentina">Argentina</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Armenia">Armenia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Aruba">Aruba</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Australia">Australia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Austria">Austria</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Azerbaijan">Azerbaijan</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Bahamas">Bahamas</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Bahrain">Bahrain</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Bangladesh">Bangladesh</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Barbados">Barbados</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Belarus">Belarus</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Belgium">Belgium</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Belize">Belize</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Benin">Benin</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Bermuda">Bermuda</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Bhutan">Bhutan</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Bolivia">Bolivia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Bonaire">Bonaire</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Botswana">Botswana</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Brazil">Brazil</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="British Indian Ocean Ter">British Indian Ocean Ter</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Brunei">Brunei</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Bulgaria">Bulgaria</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Burkina Faso">Burkina Faso</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Burundi">Burundi</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Cambodia">Cambodia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Cameroon">Cameroon</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Canada">Canada</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Canary Islands">Canary Islands</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Cape Verde">Cape Verde</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Cayman Islands">Cayman Islands</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Central African Republic">Central African Republic</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Chad">Chad</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Channel Islands">Channel Islands</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Chile">Chile</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="China">China</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Christmas Island">Christmas Island</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Cocos Island">Cocos Island</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Colombia">Colombia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Comoros">Comoros</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Congo">Congo</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Cook Islands">Cook Islands</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Costa Rica">Costa Rica</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Cote DIvoire">Cote D'Ivoire</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Croatia">Croatia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Cuba">Cuba</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Curaco">Curacao</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Cyprus">Cyprus</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Czech Republic">Czech Republic</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Denmark">Denmark</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Djibouti">Djibouti</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Dominica">Dominica</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Dominican Republic">Dominican Republic</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="East Timor">East Timor</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Ecuador">Ecuador</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Egypt">Egypt</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="El Salvador">El Salvador</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Equatorial Guinea">Equatorial Guinea</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Eritrea">Eritrea</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Estonia">Estonia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Ethiopia">Ethiopia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Falkland Islands">Falkland Islands</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Faroe Islands">Faroe Islands</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Fiji">Fiji</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Finland">Finland</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="France">France</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="French Guiana">French Guiana</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="French Polynesia">French Polynesia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="French Southern Ter">French Southern Ter</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Gabon">Gabon</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Gambia">Gambia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Georgia">Georgia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Germany">Germany</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Ghana">Ghana</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Gibraltar">Gibraltar</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Great Britain">Great Britain</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Greece">Greece</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Greenland">Greenland</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Grenada">Grenada</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Guadeloupe">Guadeloupe</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Guam">Guam</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Guatemala">Guatemala</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Guinea">Guinea</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Guyana">Guyana</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Haiti">Haiti</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Hawaii">Hawaii</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Honduras">Honduras</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Hong Kong">Hong Kong</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Hungary">Hungary</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Iceland">Iceland</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="India">India</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Indonesia">Indonesia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Iran">Iran</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Iraq">Iraq</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Ireland">Ireland</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Isle of Man">Isle of Man</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Israel">Israel</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Italy">Italy</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Jamaica">Jamaica</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Japan">Japan</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Jordan">Jordan</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Kazakhstan">Kazakhstan</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Kenya">Kenya</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Kiribati">Kiribati</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Korea North">Korea North</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Korea Sout">Korea South</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Kuwait">Kuwait</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Kyrgyzstan">Kyrgyzstan</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Laos">Laos</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Latvia">Latvia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Lebanon">Lebanon</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Lesotho">Lesotho</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Liberia">Liberia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Libya">Libya</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Liechtenstein">Liechtenstein</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Lithuania">Lithuania</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Luxembourg">Luxembourg</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Macau">Macau</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Macedonia">Macedonia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Madagascar">Madagascar</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Malaysia">Malaysia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Malawi">Malawi</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Maldives">Maldives</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Mali">Mali</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Malta">Malta</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Marshall Islands">Marshall Islands</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Martinique">Martinique</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Mauritania">Mauritania</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Mauritius">Mauritius</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Mayotte">Mayotte</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Mexico">Mexico</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Midway Islands">Midway Islands</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Moldova">Moldova</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Monaco">Monaco</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Mongolia">Mongolia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Montserrat">Montserrat</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Morocco">Morocco</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Mozambique">Mozambique</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Myanmar">Myanmar</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Nambia">Nambia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Nauru">Nauru</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Nepal">Nepal</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Netherland Antilles">Netherland Antilles</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Netherlands">Netherlands (Holland, Europe)</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Nevis">Nevis</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="New Caledonia">New Caledonia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="New Zealand">New Zealand</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Nicaragua">Nicaragua</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Niger">Niger</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Nigeria">Nigeria</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Niue">Niue</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Norfolk Island">Norfolk Island</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Norway">Norway</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Oman">Oman</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Pakistan">Pakistan</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Palau Island">Palau Island</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Palestine">Palestine</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Panama">Panama</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Papua New Guinea">Papua New Guinea</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Paraguay">Paraguay</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Peru">Peru</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Phillipines">Philippines</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Pitcairn Island">Pitcairn Island</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Poland">Poland</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Portugal">Portugal</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Puerto Rico">Puerto Rico</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Qatar">Qatar</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Republic of Montenegro">Republic of Montenegro</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Republic of Serbia">Republic of Serbia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Reunion">Reunion</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Romania">Romania</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Russia">Russia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Rwanda">Rwanda</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="St Barthelemy">St Barthelemy</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="St Eustatius">St Eustatius</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="St Helena">St Helena</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="St Kitts-Nevis">St Kitts-Nevis</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="St Lucia">St Lucia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="St Maarten">St Maarten</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Saipan">Saipan</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Samoa">Samoa</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Samoa American">Samoa American</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="San Marino">San Marino</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Sao Tome &amp; Principe">Sao Tome &amp; Principe</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Saudi Arabia">Saudi Arabia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Senegal">Senegal</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Serbia">Serbia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Seychelles">Seychelles</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Sierra Leone">Sierra Leone</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Singapore">Singapore</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Slovakia">Slovakia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Slovenia">Slovenia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Solomon Islands">Solomon Islands</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Somalia">Somalia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="South Africa">South Africa</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Spain">Spain</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Sri Lanka">Sri Lanka</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Sudan">Sudan</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Suriname">Suriname</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Swaziland">Swaziland</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Sweden">Sweden</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Switzerland">Switzerland</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Syria">Syria</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Tahiti">Tahiti</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Taiwan">Taiwan</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Tajikistan">Tajikistan</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Tanzania">Tanzania</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Thailand">Thailand</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Togo">Togo</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Tokelau">Tokelau</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Tonga">Tonga</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Trinidad &amp; Tobago">Trinidad &amp; Tobago</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Tunisia">Tunisia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Turkey">Turkey</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Turkmenistan">Turkmenistan</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Turks &amp; Caicos Is">Turks &amp; Caicos Is</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Tuvalu">Tuvalu</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Uganda">Uganda</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Ukraine">Ukraine</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="United Arab Erimates">United Arab Emirates</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="United Kingdom">United Kingdom</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="United States of America">United States of America</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Uraguay">Uruguay</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Uzbekistan">Uzbekistan</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Vanuatu">Vanuatu</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Vatican City State">Vatican City State</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Venezuela">Venezuela</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Vietnam">Vietnam</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Virgin Islands (Brit)">Virgin Islands (Brit)</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Virgin Islands (USA)">Virgin Islands (USA)</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Wake Island">Wake Island</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Wallis &amp; Futana Is">Wallis &amp; Futana Is</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Yemen">Yemen</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Zaire">Zaire</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Zambia">Zambia</a></li>
                                                        <li><a tabindex="-1" href="#" data-option="Zimbabwe">Zimbabwe</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <!--div-end-here-->





                            <div class="apply_singal_row">
                                <label>Website</label>
                                <input type="text" placeholder="Website URL" id="company" name="website" class="company-icon">
                            </div>			  
                            <!--div-start-here-->
                            <input type="hidden" name="page" value="web-design-services" >

                            <div class="apply_main">
                                <div class="apply_singal_row txtarea">
                                    <label>Requirement*</label>

                                    <textarea rows="10" cols="5" placeholder="Enter Your Requirement" id="requirement" name="requirement" class="mes-icon"></textarea>
                                </div>
                            </div>
                            <div class="apply_singal_row ">
                                <div id="slider_full" class="sl_g_fh"><img src="<?php echo base_url('captcha91e7.html'); ?>?rand=911629622" id='captchaimg'><br>
                                    <label for='message'>Enter the code above here :</label>
                                    <br>
                                    <input id="captcha_code" name="captcha_code" type="text">
                                    <br>
                                    <a href='javascript: refreshCaptcha();'>Refresh</a></div>
                            </div>
                            <!--div-end-here-->
                            <!--submit-button-start-here-->
                            <div class="apply_singal_row ne_c_r_r ">
                                <input type="submit" name="Submit" value="Let's Discuss">
                                <br>
                                <p>We will not share your details with anyone. </p>
                            </div>

                            <!--submit-button-end-here-->




                        </form>
                    </div>
                    <!--form-end-here-->

                </div>
                <!--form-main-end-here-->


            </div>
        </div>

    </div>
    <!--form end here-->
    <!-- webdesign section end here -->




    <div class="clear"></div>


  


    <div style="display: none;" id="slidingDiv" >
        <div class="slidingDiv">
            <div class="toogle-close">
                <a href="#" onClick="showSlidingDiv();
                        return false;"><img src="<?php echo base_url('public/images/close-btn01.png'); ?>" alt="Close button"></a></div>
            <div class="slide-popup-box">
                <h4>We'll call you soon</h4>
                <p>leave your details</p>
                <div class="lets-talk-out"> </div>
                <div class="slide-popup-box-form-main">
                    <div class="slide-popup-box-form">

                        <form id="lets-talk-frm" action="https://www.tisindia.com/portfolio/formhandel/contactusmini" method="post" >
                            <input type="text"  name="name" placeholder="Name:" >
                            <input type="text"  name="email" placeholder="Email:" >
                            <input type="text"  name="skype" placeholder="Skype" >
                            <input type="text"  name="mobile" placeholder="Mobile:" >
                            <input type="hidden" name="slider_unlock" value="02" >
                            <input type="text"  name="date" placeholder="Date:" id="ldate" >
                            <input type="text"  name="time" placeholder="Time:" id="ltime" >
                            <div id="slider_full_1"></div>
                            <input type="submit" id="lets-talk" value="submit" name="submit">
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div id="letstalk-wrap" class="clearfix">
        <div class="container clearfix">
            <div class="lets-talk-row clearfix">
                <h4>Let's Talk</h4>
                <p>We'd love to answer any questions you may have. Contact us and discuss your business objectives & we will let you know how we can help along with a Free Quote.</p>
                <ul>
                    <li><span></span>
                        <figure><img alt="give us call" src="<?php echo base_url('public/images/roundphn.png'); ?>"></figure>
                        <div class="ani_bg-2">
                        </div>
                        <div class="ani_bg"></div>

                        <p><small>Give us a call</small>+91 9811747579</p>
                    </li>
                    <li><span></span>
                        <figure><img alt="write to us" src="<?php echo base_url('public/images/roundmsg.png'); ?>"></figure>
                        <div class="ani_bg-2">
                        </div>
                        <div class="ani_bg"></div>
                        <p><small>Write to us</small><a href="mailto:info@tisindia.com">info@tisindia.com</a></p>
                    </li>
                    <li><span></span>
                        <figure><img alt="video chat with us" src="<?php echo base_url('public/images/skype.png'); ?>"></figure>
                        <div class="ani_bg-2">
                        </div>
                        <div class="ani_bg"></div>
                        <p><small>Video chat with us</small>info_529965</p>
                    </li>
                    <li>
                        <div class="toogle-part">
                            <a href="#" onClick="showSlidingDiv();
                                    return false;">
                                <span></span>
                                <figure><img alt="leave your details" src="<?php echo base_url('public/images/mobile.png'); ?>"></figure>
                                <div class="ani_bg-2"> </div>
                                <div class="ani_bg"></div>
                                <p><small>Have us call you</small>leave your detail</p>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <script>

        k("#lets-talk-frm").validate({
            rules: {
                name: "required",
                email: {
                    required: true,
                    email: true
                },

                //skype: "required",
                mobile: {
                    required: true,
                    digits: true,
                    minlength: 7
                },
                date: "required",
                time: "required",

                /*captcha: {
                 required: true,
                 remote: {
                 url: "portfolio/recaptcha/validate",
                 
                 }
                 } */

            },
            messages: {
                name: '',
                email: '', skype: '', mobile: '', date: '', time: '', phone: '',
            },
        });

    </script>  <!--excited lets talk-->
    <div id="wpcont9-outer" class="clearfix">

        <div class="container clearfix">
            <h2>From the blog</h2>
            <p>Keep on track of latest news and updates</p>
            <figure><img src="<?php echo base_url('public/images/divider.png'); ?>" alt="divider"></figure>

            <div class="wpcont9left">
                <h3>13 Primal Elements You Should Have on Your Homepage</h3>	
                <p>First impression always counts. In case of online businesses, it matters more than anything else, because there is an absence of face to face interaction. Hence the first impression in the form of a g ...<a href="<?php echo base_url('blog/13-primal-elements-you-should-have-on-your-homepage/index'); ?>">FIND OUT MORE</a></p>
            </div>
            <div class="wpcont9left">
                <h3>20 Key Elements of Modern Web Design to Follow In 2019 And Beyond</h3>	
                <p>
                    How Does Design Matter?
                    Web design is an evolving art. There is an evident difference in the way web pages used to appear once, and how they appear now. They are far more streamlined and user-frien ...<a href="<?php echo base_url('blog/key-elements-of-modern-web-design/index'); ?>">FIND OUT MORE</a></p>
            </div>
        </div>

    </div>

    <div id="social-share-wrap">
        <div class="addthis_toolbox addthis_default_style addthis_32x32_style">

            <div class="container clearfix">
                <ul>
                    <li> <figure><img src="<?php echo base_url('public/images/fb-icon.png'); ?>" alt="fb icon"></figure>
                        <h5><a target="_blank" href="https://www.facebook.com/tisdigitalconsulting">
                                3517+
                            </a> </h5>
                        <h6>Fans on Facebook</h6>
                    </li>
                    <li> <figure><img src="<?php echo base_url('public/images/twitter-icon.png'); ?>"  alt="twitter icon"></figure>
                        <h5><a target="https://twitter.com/TIS_Digital" href="https://twitter.com/TIS_Digital" class="" rel="nofollow">
                                2777+
                            </a> </h5>
                        <h6>Followers on Twitter</h6>
                    </li>
                    <li> <figure><img src="<?php echo base_url('public/images/linkedin-icon.png'); ?>" alt="linkedin icon"></figure>
                        <h5> <a target="_blank" href="https://www.linkedin.com/company/tis-digital-strategy-partner/">1351+</a></h5>
                        <h6>TIS on Linkedin</h6>
                    </li>
                    <!-- <li> <figure><img src="/images/gplus-icon.png" alt="gplus icon"></figure>
                      <h5><a target="_blank" href="https://plus.google.com/+Tisindiaservices/posts">Add tis</a></h5>
                      <h6>to your Circle on G+</h6>
                    </li> -->
                </ul>
            </div>
        </div>
    </div> 

<?php require_once(APPPATH . 'views/footer/footer.php'); ?>