<?php require_once(APPPATH . 'views/header/header.php'); ?>

<meta name="Description" content="Hire your own virtual team of full time PHP developers expert in PHP programming, Ajax, eCommerce solutions, custom website & apps development." />
<meta name="robots" content="index, follow" />
<?php require_once(APPPATH . 'views/header/headerpage.php'); ?>
<div id="wpcont-1-outer">

    <div class="container clearfix">
        <div class="wpcont-1-left-seo web-analist-left">
            <h1>Email Marketing</h1>
            <h5><b>"User-centric Email marketing to drive multi-channel engagement"</b></h5>
            <p>Email marketing is the step of promoting a product, selling a service, telling a story, or connecting with an audience through email. A marketing email is a one-to-many communication from an organization to a mailing list of customers and prospects. Email marketing can be a very cost effective way to reach, engage, and regain your customers. Of course, that requires that you send the right email to the right people at the right time. Working with email marketing services can help you send email campaigns that are effective and make sure successful email delivery.</p>

        </div>
        <div class="wpcont-1-right web-analist-right"><img src="<?php echo base_url('public/images/content-marketing.jpg'); ?>" alt="techmode-india"></div>
    </div>

</div>

<!--we-love-code-outer-part-start-->
<div id="wpcont3-outer">
    <div class="container clearfix">
        <div class="want-seo">
            <h3>Power your brand with Personalized Mail </h3>
            <div class="request-button"> <a href="<?php echo base_url('/request-Quote'); ?>" class="request-btn">Request a Quote</a> 
            </div>  
        </div>
    </div>
</div>
<!--we-love-code-outer-part-end-->

<!--hire-analist-we-outer-start-->
<div class="hire-analist-we-outer">

    <!--what-we-do-start-->
    <div class="what-we-do">
        <h3>How we capture a market personalized email marketing strategy?</h3>
        <div class="what-we-do-img"><img src="<?php echo base_url('public/images/web-analytic.jpg'); ?>" alt="image"></div>

        <!--what-we-do-left-start-->
        <div class="what-we-do-left">

            <!--box-1-start-->
            <div class="box-1">
                <h4>Define marketing goals</h4>
                <p>In the first stage, we set clear end-goals that we desire to obtain to move your business to the next level - whether for B2B, B2C or non-profit organisation. This not only, lets analyze your brand and then design the structure to promote. </p>
            </div>
            <!--box-1-end-->

            <!--box-3-start-->
            <div class="box-3">
                <h4>Create compelling & focused mail content</h4>
                <p>We can create highly relevant mail content customised as per your specifications -explore your business, your motive, your services, etc.; we do everything.</p>
            </div>
            <!--box-3-end-->


            <!--box-5-start-->
            <div class="box-5">
                <h4>Mail Marketing set up</h4>
                <p>Depending on our mail personalized content marketing strategy, we create touch points over the mail. We also help brands in setting up mail, optimize & manage them. </p>
            </div>
            <!--box-5-end-->


            <!--box-7-start-->
            <div class="box-7">
                <h4>Analyse to measure results</h4>
                <p>Our email experts uses the advance techniques and tools to measure the result fo the mail. So that you can measure the result of sended mails </p>
            </div>
            <!--box-7-end-->



        </div>
        <!--what-we-do-left-end-->

        <!--what-we-do-right-start-->
        <div class="what-we-do-right">

            <!--box-2-start-->
            <div class="box-2">
                <h4>Plan content strategy</h4>
                <p>We plan the content marketing strategy by creating a worthy mail content thorough research of your business and in-depth competitor analysis. As we know Email marketing is a proven content marketing strategy for increasing brand awareness, making website traffic, generating leads and promoting products and services.</p>
            </div>
            <!--box-2-end-->

            <!--box-4-start-->
            <div class="box-4">
                <h4>Optimal Send Times </h4>
                <p>Get the timing and frequency right. It’s a delicate balance between trying to stay on your audience’s radar and bombarding them with too much email marketing. Email experts can help you find patterns when it comes to opens and clicks so you can determine the optimal day, time, and frequency for your messaging.</p>
            </div>
            <!--box-4-end-->


            <!--box-6-start--> 
            <div class="box-6">
                <h4>Making SMTP Bulk Email</h4>
                <p>To make sure your marketing emails are actually reaching the audience in your contact list, you need an email marketing tool that’s reliable and worthy. The gigantic your email campaigns, the lower your delivery rate may fall. That’s why selecting the right SMTP bulk email service is so hard for email marketing.</p>
            </div>
            <!--box-6-end-->

            <!--box-8-start-->
            <div class="box-8">
                <h4>Email Content Management</h4>
                <p>Thankfully, we also have a team of intelligent email professional who are expert at creating personalized mail activities efficiently like - frequently updating mai;, managing everything etc.</p>
            </div>
            <!--box-8-end-->


        </div>
        <!--what-we-do-right-end-->

    </div>
    <!--what-we-do-end-->

</div>
<!--hire-analist-we-outer-end-->


<!--start-->  
<div class="hire-analist-outer">

    <div class="hire-analist">

        <h1>Why you should seek our Email Marketing Services?</h1>

        <!--left-part-start-->
        <div class="hire-analist-left">

            <ul>

                <li><figure><img src="<?php echo base_url('public/images/right.png'); ?>" alt="image"></figure> <p>Our team comprises email content strategists who've got the right skills to produce content of various types to promote business verticals.</p></li>

                <li><figure><img src="<?php echo base_url('public/images/right.png'); ?>" alt="image"></figure> <p>We've employed a staff of highly professional & experienced Email experts who know how to effect conversion of the business as per the targetted market.</p>  </li>



            </ul>

        </div>
        <!--left-part-end-->

        <!--right-part-start-->
        <div class="hire-analist-right">
            <ul>
                <li><figure><img src="<?php echo base_url('public/images/right.png'); ?>" alt="image"></figure> <p>We can provide you with different marketing techniques by sending bulk mail as per your needs.</p></li>

                <li><figure><img src="<?php echo base_url('public/images/right.png'); ?>" alt="image"></figure> <p>Our email marketing specialists can also track performance of your business to ensure everything to improve your business marketing potential & image.</p></li>

                <li><figure><img src="<?php echo base_url('public/images/right.png'); ?>" alt="image"></figure> <p>We've a proven past record of growing social media presence, building strong online-reputation, blog readership and lead generation email marketing campaigns created by our highly skilled team,all this within a decent budget.</p> </li>

            </ul>
        </div>
        <!--right-part-end-->

    </div>

</div>
<!--end-->  






<div class="clear"></div>



 


<div style="display: none;" id="slidingDiv" >
    <div class="slidingDiv">
        <div class="toogle-close">
            <a href="#" onClick="showSlidingDiv();
                            return false;"><img src="<?php echo base_url('public/images/close-btn01.png'); ?>" alt="Close button"></a></div>
        <div class="slide-popup-box">
            <h4>We'll call you soon</h4>
            <p>leave your details</p>
            <div class="lets-talk-out"> </div>
            <div class="slide-popup-box-form-main">
                <div class="slide-popup-box-form">



                    <form method="POST" id="lets-talk-frm" action="<?php echo base_url('contactMe'); ?>">

                        <input type="text"  name="first_name" placeholder="First Name:" >
                        <input type="text"  name="last_name" placeholder="Last Name:"  >

                        <input type="text"  name="email" placeholder="Email:" >
                        <input type="text"  name="address" placeholder="Address" >
                        <input type="text"  name="mobile" placeholder="Mobile:" >
                        <input type="hidden" name="slider_unlock" value="02" >
                        <input type="text"  name="description" placeholder="Description" id="description" >
                        <div id="slider_full_1"></div>
                        <input type="submit" id="lets-talk" value="submit" name="submit">
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="letstalk-wrap" class="clearfix">
    <div class="container clearfix">
        <div class="lets-talk-row clearfix">
            <h4>Let's Talk</h4>
            <p>We'd love to answer any questions you may have. Contact us and discuss your business objectives & we will let you know how we can help along with a Free Quote.</p>
            <ul>
                <li><span></span>
                    <figure><img alt="give us call" src="<?php echo base_url('public/images/roundphn.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>

                    <p><small>Give us a call</small>+91 8800535588</p>
                </li>
                <li><span></span>
                    <figure><img alt="write to us" src="<?php echo base_url('public/images/roundmsg.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>
                    <p><small>Write to us</small><a href="mailto:info@techmode.in">info@techmode.in</a></p>
                </li>
                <li><span></span>
                    <figure><img alt="video chat with us" src="<?php echo base_url('public/images/skype.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>
                    <p><small>Video chat with us</small>info_529965</p>
                </li>
                <li>
                    <div class="toogle-part">
                        <a href="#" onClick="showSlidingDiv();
                                        return false;">
                            <span></span>
                            <figure><img alt="leave your details" src="<?php echo base_url('public/images/mobile.png'); ?>"></figure>
                            <div class="ani_bg-2"> </div>
                            <div class="ani_bg"></div>
                            <p><small>Have us call you</small>leave your detail</p>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('public/js/toggle.js'); ?>"></script>
<script>
                            k("#lets-talk-frm").validate({
                                rules: {
                                    name: "required",
                                    email: {
                                        required: true,
                                        email: true
                                    },

                                    //skype: "required",
                                    mobile: {
                                        required: true,
                                        digits: true,
                                        minlength: 7
                                    },
                                    date: "required",
                                    time: "required",

                                },
                                messages: {
                                    name: '',
                                    email: '', skype: '', mobile: '', date: '', time: '', phone: '',
                                },
                            });

</script>  

<?php require_once(APPPATH . 'views/footer/footer.php'); ?>