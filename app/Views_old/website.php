<?php require_once(APPPATH . 'views/header/header.php'); ?>

<meta name="Description" content="Hire your own virtual team of full time PHP developers expert in PHP programming, Ajax, eCommerce solutions, custom website & apps development." />
<meta name="robots" content="index, follow" />       

<?php require_once(APPPATH . 'views/header/headerpage.php'); ?>
<div id="wpcont-1-outer">
    <div class="container  clearfix">
        <div class="hire-developer">
            <h1>Hire Dedicated Web Designers </h1>
            <p>Full time expert website designers modelling your specialized vision and concepts into high quality web solutions at highly competitive pricing. </p>
            <ul>
                <li><img src="<?php echo base_url('public/images/satisfaction-icon.jpg'); ?>" alt="customer satisfaction icon"> <span>100% project satisfaction</span> </li>
                <li><img src="<?php echo base_url('public/images/experots-icon.jpg'); ?>" alt="Long termcost saving"> <span>Long term<br>
                        cost saving</span> </li>
                <li><img src="<?php echo base_url('public/images/assured-icon.jpg'); ?>" alt="expert developers"> <span>20+ Creative<br>
                        Designers</span> </li>
                <li><img src="<?php echo base_url('public/images/frequent-icon.jpg'); ?>" alt="hassle free communication"> <span>Hassle Free Communication</span> </li>
                <li><img src="<?php echo base_url('public/images/hassle_free-icon.jpg'); ?>" alt="complete handcoded markup"> <span>100% Handcoded markup</span> </li>
                <li><img src="<?php echo base_url('public/images/great_s-icon.jpg'); ?>" alt="Total flexibility"> <span>Total<br>
                        flexibility</span> </li>
            </ul>
        </div>
    </div>
</div>
<div id="wpcont5-outer" class="clearfix">
    <div class="container per-hour clearfix">
        <div class="per-hour-box">
            <div class="blue-box">
                <h3>Per Hour<span>$12</span></h3>
                <img src="<?php echo base_url('public/images/or-devider.jpg'); ?>" alt="Or">
                <h3>Per Month<span>$1500</span></h3>
            </div>
            <!--   <div class="get-srt-bt"><a href="hire-dedicated-resource.html">Get Started Now</a></div> -->
            <div class="get-srt-bt"><a  class="ppc" href="#hire-dedicated-resource">Get Started Now</a></div>
        </div>
        <div class="hire-wd">
            <h3>In-house access !! Offshore cost benefits !!
                Hire dedicated Designers from TIS India</h3>
            <ul>
                <li>We have been delivering dedicated resources to many companies all over the world since years.</li>
                <li> Your complete involvement and direct control - through dedicated high-speed communications and
                    live chat conferencing capabilities.</li>
                <li> All hardware and software and office infrastructure are already in place</li>
                <li> Committed pool of skilled resources specialized in <a href="<?php echo base_url('responsive-website-design'); ?>">responsive and SEO friendly designing</a>.</li>
                <li> Easy access, immediate query handling</li>
                <li> Devoted team of Web Designing experts to provide result oriented solutions</li>
                <li> Minimization of risk - no need for full-time staff or contractors.</li>
                <li> Competitive pricing, significant cost savings over in-house teams</li>
                <li> No hidden cost involved</li>
            </ul>
        </div>
        <div class="how-it-work"> <img src="<?php echo base_url('public/images/how-it-work.jpg'); ?>" alt="how it work">
            <div class="how-it-box">
                <h3>How it <span>works</span></h3>
                <ul>
                    <li>Hire our dedicated designers</li>
                    <li>Instruct your own virtual team through PMS </li>
                    <li>Implementation of your ideas in real time</li>
                    <li>Testing by QA team before delivery</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="let-our-expert">
    <div class="container let-expeorts  clearfix">
        <h2>Skills of our Web Designing Experts</h2>
        <div class="let-left col_new1">
            <p>If you own multiple websites / portals and looking for full time web designers or your own offshore team of expert designers to work dedicatedly on your projects with your specialized vision and concepts, then TIS India can help you by providing dedicated web resources.</p>
            <p>We excel a team of expert full time designers, graphic designers, XHTML / CSS developers proficient in different designing tools and have magnificent experience in handling web designing projects.</p>
            <p>Dedicated resources will work for you dedicatedly as per your needs and you can experience the control of in-house staff with the financial advantages of off-shore development. We can also provide you experts who can deliver web design, <a href="<?php echo base_url('graphic-design-services'); ?>">graphic design</a> and XHTML work together.</p>
            <p>You may hire full time web designer individually or a virtual team of dedicated designers which will be available to you through IM, Email and Phone. </p>
        </div>
        <div class="let-left project-suss col_new1">
            <ul>
                <li>Minimum 4+ Years of Industry Experience</li>
                <li>3-4 Year Degree Course</li>
                <li>Proficient in Photoshop, Illustrator, Dreamweaver, HTML, XHTML,HTML5, CSS.</li>
                <li>Expert in UI/UX Design</li>
                <li>Creating corporate identity designs, logo, business card design, flyers. </li>
                <li>Updated with latest design trends.</li>
            </ul>
        </div>
    </div>
</div>
<div style="display:none;" class="pop_up_start_hear">
    <div id="hire-dedicated-resource" >
        <script>

            $("#contact-form").validate({
            rules: {
            name: "required",
                    email: {
                    required: true,
                            email: true
                    },
                    //	skype: "required",
                    country: "required",
                    requirement: "required",
                    phone:{
                    required: true,
                            digits: true,
                            minlength: 7
                    },
                    captcha_code: {
                    required: true,
                            remote: {
                            url: "captchav.php?t=" + $("#captcha_code").val(),
                            }
                    }
            },
                    messages:{
                    name: '',
                            email: '',
                            requirement: '',
                            phone: '',
                            captcha_code: '',
                    },
            });</script>
        <button onclick="$.fn.custombox('close');" class="close" type="button"></button>
        <div class="excited-talk">
            <h2>Excited! Let's Talk</h2>
            <h3><span>We discuss because We Care</span></h3>

            <!--form-main-start-here-->
            <div class="apply_form">

                <!--sucess-massage-start-here-->
                <div id="sucess-message">
                    <h1>Thank You!</h1>
                    <p>Hi,
                        We'll get back to you very soon.
                    </p>
                </div>
                <!--sucess-massage-start-here-->
                <!--form-start-here-->
                <div id="hd_form">
                    <form action="https://www.tisindia.com/portfolio/formhandel/contactus" method="post" id="contact-form" name="contact-form" >

                        <div class="apply_singal_row">
                            <label>Name*</label>
                            <input type="text" placeholder="Enter Your Name"  id="name" name="name" class="name-icon">
                        </div>
                        <!--div-start-here-->
                        <div class="apply_singal_row row-float">
                            <label>Email*</label>
                            <input type="text" placeholder="Enter Your Email" id="email" name="email" class="email-icon">
                        </div>
                        <!--div-end-here-->


                        <div class="apply_main">
                            <div class="apply_singal_row resp-row"> 
                                <div class="total-e">
                                    <label>Mobile*</label>
                                    <input type="text" placeholder="Enter Mobile No." id="time" name="phone" class="best-time">

                                </div>
                                <!--div-start-here-->
                                <div class="total-e row-float">
                                    <label>Skype</label>
                                    <input type="text" placeholder="Enter Skype ID" id="contact" name="skype" class="contas-icon">
                                </div>
                            </div>

                            <div class="apply_singal_row row-float resp-row">
                                <label>Country</label>
                                <div class="">
                                    <div class="bfh-selectbox">
                                        <input name="country" value="" type="hidden">
                                        <a class="bfh-selectbox-toggle" role="button" data-toggle="bfh-selectbox" href="#">
                                            <p class="bfh-selectbox-option input-medium wid_80" data-option="1">Select Your Country</p>
                                            <b class="arrow-down"></b> </a>
                                        <div class="bfh-selectbox-options">
                                            <div role="listbox">
                                                <ul class="option">
                                                    <li><a tabindex="-1" href="#" data-option="">Country...</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Afganistan">Afghanistan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Albania">Albania</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Algeria">Algeria</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="American Samoa">American Samoa</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Andorra">Andorra</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Angola">Angola</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Anguilla">Anguilla</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Antigua &amp; Barbuda">Antigua &amp; Barbuda</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Argentina">Argentina</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Armenia">Armenia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Aruba">Aruba</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Australia">Australia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Austria">Austria</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Azerbaijan">Azerbaijan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Bahamas">Bahamas</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Bahrain">Bahrain</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Bangladesh">Bangladesh</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Barbados">Barbados</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Belarus">Belarus</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Belgium">Belgium</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Belize">Belize</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Benin">Benin</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Bermuda">Bermuda</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Bhutan">Bhutan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Bolivia">Bolivia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Bonaire">Bonaire</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Botswana">Botswana</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Brazil">Brazil</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="British Indian Ocean Ter">British Indian Ocean Ter</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Brunei">Brunei</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Bulgaria">Bulgaria</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Burkina Faso">Burkina Faso</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Burundi">Burundi</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Cambodia">Cambodia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Cameroon">Cameroon</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Canada">Canada</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Canary Islands">Canary Islands</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Cape Verde">Cape Verde</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Cayman Islands">Cayman Islands</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Central African Republic">Central African Republic</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Chad">Chad</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Channel Islands">Channel Islands</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Chile">Chile</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="China">China</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Christmas Island">Christmas Island</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Cocos Island">Cocos Island</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Colombia">Colombia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Comoros">Comoros</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Congo">Congo</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Cook Islands">Cook Islands</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Costa Rica">Costa Rica</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Cote DIvoire">Cote D'Ivoire</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Croatia">Croatia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Cuba">Cuba</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Curaco">Curacao</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Cyprus">Cyprus</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Czech Republic">Czech Republic</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Denmark">Denmark</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Djibouti">Djibouti</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Dominica">Dominica</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Dominican Republic">Dominican Republic</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="East Timor">East Timor</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Ecuador">Ecuador</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Egypt">Egypt</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="El Salvador">El Salvador</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Equatorial Guinea">Equatorial Guinea</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Eritrea">Eritrea</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Estonia">Estonia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Ethiopia">Ethiopia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Falkland Islands">Falkland Islands</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Faroe Islands">Faroe Islands</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Fiji">Fiji</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Finland">Finland</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="France">France</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="French Guiana">French Guiana</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="French Polynesia">French Polynesia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="French Southern Ter">French Southern Ter</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Gabon">Gabon</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Gambia">Gambia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Georgia">Georgia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Germany">Germany</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Ghana">Ghana</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Gibraltar">Gibraltar</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Great Britain">Great Britain</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Greece">Greece</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Greenland">Greenland</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Grenada">Grenada</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Guadeloupe">Guadeloupe</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Guam">Guam</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Guatemala">Guatemala</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Guinea">Guinea</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Guyana">Guyana</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Haiti">Haiti</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Hawaii">Hawaii</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Honduras">Honduras</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Hong Kong">Hong Kong</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Hungary">Hungary</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Iceland">Iceland</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="India">India</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Indonesia">Indonesia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Iran">Iran</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Iraq">Iraq</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Ireland">Ireland</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Isle of Man">Isle of Man</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Israel">Israel</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Italy">Italy</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Jamaica">Jamaica</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Japan">Japan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Jordan">Jordan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Kazakhstan">Kazakhstan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Kenya">Kenya</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Kiribati">Kiribati</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Korea North">Korea North</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Korea Sout">Korea South</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Kuwait">Kuwait</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Kyrgyzstan">Kyrgyzstan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Laos">Laos</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Latvia">Latvia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Lebanon">Lebanon</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Lesotho">Lesotho</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Liberia">Liberia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Libya">Libya</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Liechtenstein">Liechtenstein</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Lithuania">Lithuania</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Luxembourg">Luxembourg</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Macau">Macau</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Macedonia">Macedonia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Madagascar">Madagascar</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Malaysia">Malaysia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Malawi">Malawi</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Maldives">Maldives</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Mali">Mali</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Malta">Malta</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Marshall Islands">Marshall Islands</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Martinique">Martinique</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Mauritania">Mauritania</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Mauritius">Mauritius</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Mayotte">Mayotte</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Mexico">Mexico</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Midway Islands">Midway Islands</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Moldova">Moldova</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Monaco">Monaco</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Mongolia">Mongolia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Montserrat">Montserrat</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Morocco">Morocco</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Mozambique">Mozambique</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Myanmar">Myanmar</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Nambia">Nambia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Nauru">Nauru</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Nepal">Nepal</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Netherland Antilles">Netherland Antilles</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Netherlands">Netherlands (Holland, Europe)</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Nevis">Nevis</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="New Caledonia">New Caledonia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="New Zealand">New Zealand</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Nicaragua">Nicaragua</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Niger">Niger</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Nigeria">Nigeria</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Niue">Niue</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Norfolk Island">Norfolk Island</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Norway">Norway</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Oman">Oman</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Pakistan">Pakistan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Palau Island">Palau Island</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Palestine">Palestine</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Panama">Panama</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Papua New Guinea">Papua New Guinea</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Paraguay">Paraguay</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Peru">Peru</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Phillipines">Philippines</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Pitcairn Island">Pitcairn Island</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Poland">Poland</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Portugal">Portugal</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Puerto Rico">Puerto Rico</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Qatar">Qatar</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Republic of Montenegro">Republic of Montenegro</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Republic of Serbia">Republic of Serbia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Reunion">Reunion</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Romania">Romania</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Russia">Russia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Rwanda">Rwanda</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="St Barthelemy">St Barthelemy</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="St Eustatius">St Eustatius</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="St Helena">St Helena</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="St Kitts-Nevis">St Kitts-Nevis</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="St Lucia">St Lucia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="St Maarten">St Maarten</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Saipan">Saipan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Samoa">Samoa</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Samoa American">Samoa American</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="San Marino">San Marino</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Sao Tome &amp; Principe">Sao Tome &amp; Principe</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Saudi Arabia">Saudi Arabia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Senegal">Senegal</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Serbia">Serbia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Seychelles">Seychelles</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Sierra Leone">Sierra Leone</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Singapore">Singapore</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Slovakia">Slovakia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Slovenia">Slovenia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Solomon Islands">Solomon Islands</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Somalia">Somalia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="South Africa">South Africa</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Spain">Spain</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Sri Lanka">Sri Lanka</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Sudan">Sudan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Suriname">Suriname</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Swaziland">Swaziland</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Sweden">Sweden</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Switzerland">Switzerland</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Syria">Syria</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Tahiti">Tahiti</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Taiwan">Taiwan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Tajikistan">Tajikistan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Tanzania">Tanzania</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Thailand">Thailand</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Togo">Togo</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Tokelau">Tokelau</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Tonga">Tonga</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Trinidad &amp; Tobago">Trinidad &amp; Tobago</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Tunisia">Tunisia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Turkey">Turkey</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Turkmenistan">Turkmenistan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Turks &amp; Caicos Is">Turks &amp; Caicos Is</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Tuvalu">Tuvalu</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Uganda">Uganda</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Ukraine">Ukraine</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="United Arab Erimates">United Arab Emirates</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="United Kingdom">United Kingdom</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="United States of America">United States of America</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Uraguay">Uruguay</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Uzbekistan">Uzbekistan</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Vanuatu">Vanuatu</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Vatican City State">Vatican City State</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Venezuela">Venezuela</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Vietnam">Vietnam</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Virgin Islands (Brit)">Virgin Islands (Brit)</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Virgin Islands (USA)">Virgin Islands (USA)</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Wake Island">Wake Island</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Wallis &amp; Futana Is">Wallis &amp; Futana Is</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Yemen">Yemen</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Zaire">Zaire</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Zambia">Zambia</a></li>
                                                    <li><a tabindex="-1" href="#" data-option="Zimbabwe">Zimbabwe</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!--div-end-here-->

                        <div class="apply_singal_row ">
                            <label>Experience</label>
                            <div class="">
                                <div class="bfh-selectbox">
                                    <input name="experience" value="" type="hidden">
                                    <a class="bfh-selectbox-toggle" role="button" data-toggle="bfh-selectbox" href="#">
                                        <p class="bfh-selectbox-option input-medium wid_80" data-option="1">Select Required Experience</p>
                                        <b class="arrow-down"></b> </a>
                                    <div class="bfh-selectbox-options">
                                        <div role="listbox">
                                            <ul class="option">
                                                <li><a tabindex="-1" href="#" data-option="0">Select</a></li>
                                                <li><a tabindex="-1" href="#" data-option="1 Year">1 Year</a></li>
                                                <li><a tabindex="-1" href="#" data-option="2 Year">2 Year</a></li>
                                                <li><a tabindex="-1" href="#" data-option="3 Year">3 Year</a></li>
                                                <li><a tabindex="-1" href="#" data-option="4 Year">4 Year</a></li>
                                                <li><a tabindex="-1" href="#" data-option="5 Year">5 Year</a></li>
                                                <li><a tabindex="-1" href="#" data-option="6 Year">6 Year</a></li>
                                                <li><a tabindex="-1" href="#" data-option="7 Year">7 Year</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>			  

                        </div>



                        <div class="apply_singal_row row-float ">
                            <label>Website</label>
                            <input type="text" placeholder="Website URL" id="company" name="website" class="company-icon">
                        </div>			  
                        <!--div-start-here-->
                        <input type="hidden" name="page" value="hire-website-designer" >

                        <div class="apply_main">
                            <div class="apply_singal_row txtarea">
                                <label>Requirement*</label>

                                <textarea rows="10" cols="5" placeholder="Enter Your Requirement" id="requirement" name="requirement" class="mes-icon"></textarea>
                            </div>
                        </div>
                        <div class="apply_singal_row ">
                            <div id="slider_full"><img src="captcha33b4.html?rand=101173590" id='captchaimg'><br>
                                <label for='message'>Enter the code above here :</label>
                                <br>
                                <input id="captcha_code" name="captcha_code" type="text">
                                <br>
                                <a href='javascript: refreshCaptcha();'>Refresh</a></div>
                        </div>
                        <!--div-end-here-->
                        <!--submit-button-start-here-->
                        <div class="apply_singal_row ne_c_r_r ">
                            <input type="submit" name="Submit" value="Let's Discuss">
                            <br>
                            <p>We will not share your details with anyone. </p>
                        </div>

                        <!--submit-button-end-here-->




                    </form>
                </div>
                <!--form-end-here-->

            </div>
            <!--form-main-end-here-->


        </div>
    </div>

</div>

<div id="meet-our-team">
    <h2>our latest work</h2>
    <p>Delivered 5000+ successful projects in last 7 years.</p>
</div>
<div class="our-latest-work">
    <ul id="protfilio-list">
        <img class="loader" src="<?php echo base_url('public/images/loader.gif'); ?>" style="opacity:0.7;align:center;"/>
    </ul>
</div>





<div class="clear"></div>

<link rel="stylesheet" href="<?php echo base_url('public/css/slider-captchab3e8.css'); ?>?ver=0.1" media="screen">
<script src="<?php echo base_url('public/js/jquery-ui-1.10.4.custom.min.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('public/css/jquery.bxslider.css'); ?>" media="screen">
<link rel="stylesheet" href="<?php echo base_url('public/css/main.css'); ?>" media="screen"> 
<script src="<?php echo base_url('public/js/jquery.bxslider.js'); ?>"></script>
<script type="text/javascript">
</script>
<script src="<?php echo base_url('public/js/jquery.ui.touch-punch-improved.js'); ?>"></script>
<script src="<?php echo base_url('public/js/slider-captchab3e8.js'); ?>?ver=0.1"></script>
<script src="<?php echo base_url('public/js/jquery.validate.js'); ?>"></script> 
<script src="<?php echo base_url('public/js/jquery.datetimepicker.js'); ?>">/script>


    <script src="<?php echo base_url('public/js/jquery.form.js'); ?>"></script>            


<div style="display: none;" id="slidingDiv" >
<div class="slidingDiv">
	<div class="toogle-close">
	<a href="#" onClick="showSlidingDiv(); return false;"><img src="<?php echo base_url('public/images/close-btn01.png'); ?>" alt="Close button"></a></div>
<div class="slide-popup-box">
    <h4>We'll call you soon</h4>
    <p>leave your details</p>
    <div class="lets-talk-out"> </div>
    <div class="slide-popup-box-form-main">
        <div class="slide-popup-box-form">

            <form id="lets-talk-frm" action="https://www.tisindia.com/portfolio/formhandel/contactusmini" method="post" >
                <input type="text"  name="name" placeholder="Name:" >
                <input type="text"  name="email" placeholder="Email:" >
                <input type="text"  name="skype" placeholder="Skype" >
                <input type="text"  name="mobile" placeholder="Mobile:" >
                <input type="hidden" name="slider_unlock" value="02" >
                <input type="text"  name="date" placeholder="Date:" id="ldate" >
                <input type="text"  name="time" placeholder="Time:" id="ltime" >
                <div id="slider_full_1"></div>
                <input type="submit" id="lets-talk" value="submit" name="submit">
            </form>
        </div>

    </div>
</div>
</div>
</div>

<div id="letstalk-wrap" class="clearfix">
    <div class="container clearfix">
        <div class="lets-talk-row clearfix">
            <h4>Let's Talk</h4>
            <p>We'd love to answer any questions you may have. Contact us and discuss your business objectives & we will let you know how we can help along with a Free Quote.</p>
            <ul>
                <li><span></span>
                    <figure><img alt="give us call" src="<?php echo base_url('public/images/roundphn.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>

                    <p><small>Give us a call</small>+91 9811747579</p>
                </li>
                <li><span></span>
                    <figure><img alt="write to us" src="<?php echo base_url('public/images/roundmsg.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>
                    <p><small>Write to us</small><a href="mailto:info@tisindia.com">info@tisindia.com</a></p>
                </li>
                <li><span></span>
                    <figure><img alt="video chat with us" src="<?php echo base_url('public/images/skype.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>
                    <p><small>Video chat with us</small>info_529965</p>
                </li>
                <li>
                    <div class="toogle-part">
                        <a href="#" onClick="showSlidi                            ngDiv(); return false;">
                            <span></span>
                                <figure><img alt="leave your details" src="<?php echo base_url('public/images/mobile.png'); ?>"></figure>
                                <div class="ani_bg-2"> </div>
                                <div class="ani_bg"></div>
                                <p><small>Have us call you</small>leave your detail</p>
                            </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<script>
                
                k("#lets-talk-frm").validate({
                            rules: {
                            name: "required",
                                    email: {
                                    required: true,
                                            email: true
                                    },
                                    //skype: "required",
                                    mobile:{
                                    required: true,
                                            digits: true,
                    minlength: 7
                                    },
                                    date: "required",
                                    time: "required",
                            },
                            messages:{
                            name: '',
                                    email: '', skype: '', mobile: '', date: '', time: '', phone: '',
                            },
                    });
                    
                            </script>
<!--excited lets talk-->
    <div id="wpcont9-outer" class="clearfix">

<div class="container clearfix">
      <h2>From the blog</h2        >
        <p>Keep on track of latest news and updates</p>
        <figure><img src="<?php echo base_url('public/images/divider.png'); ?>" alt="divider"></figure>

        <div class="wpcont9left">
            <h3>Content-First Design-3 Steps To Effectively Implement It</h3>	
            <p>

                Do you think that having a responsive and elegant website design is all that you need to enhance your visitor’s experience? Well, yes, but partially! Without the right content to flaunt your des ...<a href="<?php echo base_url('blog/content-first-design/index'); ?>">FIND OUT MORE</a></p>
        </div>
        <div class="wpcont9left">
            <h3>12 Essential Web Design Trends of 2017 To Watch Out For</h3>	
            <p>

                2016 was the year that web design disciples the world over proved themselves to be the singular champions of free thought in design. Slack’s outstanding UX propelled the startup to unicorn statu ...<a href="<?php echo base_url('blog/web-design-trends/index'); ?>">FIND OUT MORE</a></p>
        </div>
    </div>

</div>
<!--From the blog section-->

<div id="social-share-wrap">
    <div class="addthis_toolbox addthis_default_style addthis_32x32_style">

        <div class="container clearfix">
            <ul>
                <li> <figure><img src="<?php echo base_url('public/images/fb-icon.png'); ?>" alt="fb icon"></figure>
                    <h5><a target="_blank" href="https://www.facebook.com/tisdigitalconsulting">
                            3517+
                        </a> </h5>
                    <h6>Fans on Facebook</h6>
                </li>
                <li> <figure><img src="<?php echo base_url('public/images/twitter-icon.png'); ?>"  alt="twitter icon"></figure>
                    <h5><a target="https://twitter.com/TIS_Digital" href="https://twitter.com/TIS_Digital" class="" rel="nofollow">
                            2777+
                        </a> </h5>
                    <h6>Followers on Twitter</h6>
                </li>
                <li> <figure><img src="<?php echo base_url('public/images/linkedin-icon.png'); ?>" alt="linkedin icon"></figure>
                    <h5> <a target="_blank" href="https://www.linkedin.com/company/tis-digital-strategy-partner/">1351+</a></h5>
                    <h6>TIS on Linkedin</h6>
                </li>

            </ul>
        </div>
    </div>
</div><!--social share section-->
<footer>
    <div class="container clearfix">
        <ul class="foot-menu">
            <li><a href="<?php echo base_url('/'); ?>">Home</a></li>
            <li><a href="<?php echo base_url('portfolio'); ?>">portfolio</a></li>
            <li><a href="<?php echo base_url('blog/index'); ?>">Blog</a></li>
            <li><a href="<?php echo base_url('contact'); ?>">Contact Us</a></li>
            <li><a href="<?php echo base_url('testimonial'); ?>">Testimonials</a></li>
            <li><a href="<?php echo base_url('career'); ?>">Career</a></li>
            <li><a href="<?php echo base_url('sitemap'); ?>">Sitemap</a></li>

        </ul>

        <div class="dmca">
            <script src="<?php echo base_url('public/images.dmca.com/Badges/DMCABadgeHelper.min.js'); ?>"> </script>
            <a rel="nofollow" class="dmca-badge" target="_blank" href="http://www.dmca.com/Protection/Status.aspx?ID=5acc4010-2fe4-4321-a4fb-25cf12ec1bec" title="DMCA.com Protection Status"> <img src="<?php echo base_url('images.dmca.com/Badges/_dmca_premi_badge_55215.png'); ?>?ID=5acc4010-2fe4-4321-a4fb-25cf12ec1bec" alt="DMCA.com Protection Status"></a>
            <p>&copy; 2019 TIS India.  All rights reserved.</p></div>
    </div>
</footer>
<!--footer section-->
</div>

<!--main wrapper-->
                            <script> 
                            
                            $(window).load(function(){

                           $.ajax({
                            url: "portfolio/ajax/serprot?cat=12",
                                    beforeSend: function(xhr) {
                                    //xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
                                    }
                            })
                            .done(function(data) {
                            $('#protfilio-list').prepend(data);
                            $('.loader').hide();
                                $('.ppc').on('click', function (e) {
                            $.f n.custombox(this, {
                            effect: 'fadein',
                                    complete: function(){
                                    $(".zoomer_basic").zoomer({
                                    marginMax: 10,
                                            marginMin: 10,
                                            ///		increment: .1,
                                    });
                                    $(".zoomer_basic").zoomer("resize");
                                    },
                                    open: function(){
                                    $(".zoomer_basic").zoomer({
                                    marginMax: 10,
                                            marginMin: 10,
                                            //increment: .1,
                                    });
                                    }
                            ,
                                    close : function(){
                                    $(".zoomer_basic").zoomer("destroy");
                                    },
                            });
                            e.preventDefault();
                            });
                            });
                                });
                                
                                $(document).ready(function(){
                            $(".zoomer_basic").zoomer();
                                    });
                                    
    </script>

    <?php require_once(APPPATH . 'views/footer/footer.php'); ?>