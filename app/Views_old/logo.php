
<?php require_once(APPPATH . 'views/header/header.php'); ?>

<meta name="Description" content="Hire your own virtual team of full time PHP developers expert in PHP programming, Ajax, eCommerce solutions, custom website & apps development." />
<meta name="robots" content="index, follow" />
<?php require_once(APPPATH . 'views/header/headerpage.php'); ?>
<div id="wpcont-1-outer">
  <div class="container clearfix">
    <div class="wpcont-1-left-seo">
      <h1> Logo design </h1>
      <h5>One Logo Makes...Thousands of Ideas. </h5>
      <p> The logo, or brand is not just a visual but it is the embodiment of business - of its area of work, it's ethos, it's core values and so on. Creating a logo is commonly believed to be one of the most crucial areas in graphic design and thus taken as the most difficult area to perfect.</p>
      <p>A good logo is unique, and not subject to confusion with other logos among end users, is featurable and can be used in many different contexts while retaining its integrity. A worthy logo should remain effective reproduced small or large, can work in "full-color", but also in two color presentation (black and white), spot color or half tone.</p>
    </div>
    <div class="request-button"> <a href="<?php echo base_url('/request-Quote');?>" class="request-btn">Request a Quote</a> 
     </div>  
    <div class="wpcont-1-right g-search"><img src="<?php echo base_url('public/images/logo-design-banner.png');?>" alt="logo design company"></div>
  </div>
</div>
<div id="wpcont5-outer">
  <div class="container clearfix">
    <div class="wpcont7left">
      <h2>Requirement of logo</h2>
      <ul class="col-left">
        <li>Logo release strong brand identity and familiarity for a business. It makes people to think of your service or product as soon as they notice it.</li>
        <li>Logos are meant to represent companies and foster identities by consumers </li>
        <li>It communicate professionalism. It helps build trust.</li>
      </ul>
    </div>
    <div class="wpcont7left">
      <h2>Why choose TECHMODE India</h2>
      <ul class="col-left col-right">
        <li> We are having more than 5 years of experience and helped hundred of companies to establish their
          brands. At TECHMODE India we understand the valuable position a logo has in creating a brand identity and we are always intrested to take up this challenge as it brings out the best of creativity from out team. </li>
        <li>TECHMODE India Logo Design experts has the technical and creative competencies to develop, build and create a exclusive identity that is, a unique logo for your business. </li>
      </ul>
    </div>
  </div>
</div>




<div class="clear"></div>



<div style="display: none;" id="slidingDiv" >
<div class="slidingDiv">
  <div class="toogle-close">
  <a href="#" onClick="showSlidingDiv(); return false;"><img src="<?php echo base_url('public/images/close-btn01.png');?>" alt="Close button"></a></div>
  <div class="slide-popup-box">
  <h4>We'll call you soon</h4>
  <p>leave your details</p>
  <div class="lets-talk-out"> </div>
    <div class="slide-popup-box-form-main">
      <div class="slide-popup-box-form">
      
   

           <form method="POST" id="lets-talk-frm" action="<?php echo base_url('contactMe');?>">
                        
        <input type="text"  name="first_name" placeholder="First Name:" >
              <input type="text"  name="last_name" placeholder="Last Name:"  >

        <input type="text"  name="email" placeholder="Email:" >
        <input type="text"  name="address" placeholder="Address" >
        <input type="text"  name="mobile" placeholder="Mobile:" >
        <input type="hidden" name="slider_unlock" value="02" >
        <input type="text"  name="description" placeholder="Description" id="description" >
          <div id="slider_full_1"></div>
        <input type="submit" id="lets-talk" value="submit" name="submit">
        </form>
        </div>
        
      </div>
    </div>
  </div>
</div>

 <div id="letstalk-wrap" class="clearfix">
    <div class="container clearfix">
      <div class="lets-talk-row clearfix">
       <h4>Let's Talk</h4>
        <p>We'd love to answer any questions you may have. Contact us and discuss your business objectives & we will let you know how we can help along with a Free Quote.</p>
      <ul>
          <li><span></span>
            <figure><img alt="give us call" src="<?php echo base_url('public/images/roundphn.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
      
            <p><small>Give us a call</small>+91 8800535588</p>
          </li>
          <li><span></span>
            <figure><img alt="write to us" src="<?php echo base_url('public/images/roundmsg.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
            <p><small>Write to us</small><a href="mailto:info@techmode.in">info@techmode.in</a></p>
          </li>
          <li><span></span>
            <figure><img alt="video chat with us" src="<?php echo base_url('public/images/skype.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
            <p><small>Video chat with us</small>info_529965</p>
          </li>
          <li>
      <div class="toogle-part">
      <a href="#" onClick="showSlidingDiv(); return false;">
      <span></span>
              <figure><img alt="leave your details" src="<?php echo base_url('public/images/mobile.png');?>"></figure>
              <div class="ani_bg-2"> </div>
              <div class="ani_bg"></div>
              <p><small>Have us call you</small>leave your detail</p>
        </a>
        </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
 <script type="text/javascript" src="<?php echo base_url('public/js/toggle.js');?>"></script>
 <script>
  
  k("#lets-talk-frm").validate({
    rules: {
    name: "required",
    email: {
      required: true,
      email: true
    },
    
    //skype: "required",
    mobile:{
    required: true,
    digits: true,
     minlength: 7
    },
    date: "required",
    time: "required",
  
    /*captcha: {
      required: true,
      remote: {
      url: "portfolio/recaptcha/validate",
      
      }
    } */  
    
    },
    messages:{
      name: '',
      email: '', skype: '', mobile: '', date: '', time: '', phone: '',
    },
    });
  
 </script><!--excited lets talk-->
    <div id="wpcont9-outer" class="clearfix">

<div class="container clearfix">
      <h2>From the blog</h2>
      <p>Keep on track of latest news and updates</p>
      <figure><img src="<?php echo base_url('public/images/divider.png');?>" alt="divider"></figure>

      <div class="wpcont9left">
      <h3>Tips to Avoid Mistakes While Designing Company Logo</h3>  
      <p>
"A logo does not only give your brand a visual identity but it also performs the functions of elaborating your brand’s personality, making it visually memorable, marketing your products globally a ...<a href="blog/mistakes-designing-company-logo/index.html">FIND OUT MORE</a></p>
    </div>
      <div class="wpcont9left">
      <h3>Logo Design Principles – How to Create a Successful Logo For Your Brand?</h3> 
      <p>
Designing a good logo is one of the most vital things you can do for your business, especially when you are founding a new company. Your logo should be capable of conveying the intended message to y ...<a href="blog/logo-design-principles-create-successful-logo-brand/index.html">FIND OUT MORE</a></p>
    </div>
    </div>
    
     </div>



<?php require_once(APPPATH . 'views/footer/footer.php'); ?>



