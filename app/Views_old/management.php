<?php require_once(APPPATH . 'views/header/header.php'); ?>
<meta name="Description" content="Hire your own virtual team of full time PHP developers expert in PHP programming, Ajax, eCommerce solutions, custom website & apps development." />
<meta name="robots" content="index, follow" />

<?php require_once(APPPATH . 'views/header/headerpage.php'); ?>
<div id="wpcont-1-outer">
    <div class="container clearfix">
        <div class="wpcont-1-left-seo">
            <h1>Online Reputation Management </h1>
            <h5><b> Strategy to Expanded reach, Greater promotions, Increased traffic, Better sales and Boosting Brand image and popularity </b></h5>
            <p>Booming Internet has given birth to a lot of good things along with a few bad things. One such bad aspect of internet is open and criticism. Positive review can help you improve your business but if the review is more than feedback then you need to take some strong steps. Defamation, misrepresentation comments, negative reviews, professional wariness and reputation damaging campaign from your opposition, all these things can cause irreparable loss to your business. </p>
            <p>When we talk of online reputation for small/big businesses, we normally think that the only thing at stake is reputation of the company. The irony is that cynical remarks, aspersions and malign campaigns not just harm your business but they also get down the fame on your individual personality. </p>
            <p>Social media does give you a uncountable customers, but is it really that smooth to make all of them happy? Definitely not. There will be anyone who is complaining about this or that issue. In some cases the poor comments turn in to online abuse, leaving you and your business at the mercy of that one customer-turned-destruct. </p>
        </div>
        <div class="wpcont-1-right g-search mb-5"><img src="<?php echo base_url('public/images/reputations-management.png'); ?>" alt="reputation management"></div>
    </div>
</div>
<div id="wpcont3-outer">
    <div class="container clearfix">
        <div class="want-seo">
            <h3>Have experts manage your online reputation</h3>
            <div class="request-button"> <a href="<?php echo base_url('/request-Quote'); ?>" class="request-btn">Request a Quote</a> 
            </div>  
            <!--<div class="request-seo"> <a href="request-a-proposald5e4.html?p=Seo">Lets Discuss</a> </div>-->
        </div>
    </div>
</div>
<div id="wpcont5-outer" class="considr-coustmazation">
    <div class="container clearfix">
        <div class="cont4-left col_new1">
            <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/propaganda-strategy.png'); ?>" alt="Counter-Propaganda Strategy">
                <div class="consider">
                    <h3>Counter-Propaganda techniques:</h3>
                    <p>Your small business can bear severely if there is defamation online propaganda against it. Anyone with an axe to grind with you can upload a misrepresentation feedback or worse can run an entire goodwill damaging campaign against you.<br>
                        <br>
                        This is the time that you must take strong action and give a counter-propaganda technique. Digital SEO expert can help you to manage online reputation for small/big business with the help of SEO techniques and analytical tools. We help you to regulate your business and your reputation in your own hands. </p>
                </div>
            </div>
            <!--Wpsite-box-end-->
            <!--Wpsite-box-end-->
            <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/seo-management.png'); ?>" alt="seo reputation management">
                <div class="consider">
                    <h3>SEO and Online Reputation Management:</h3>
                    <p>With SEO, we ensure that all of the negative comments, bad reviews and anything maligning your reputation or business is moved down to the lowest search engine pages.<br>
                        <br>
                        The ranking will be down that it will go unnoticed by the valuable customers searching for you or your business. In special cases we will use exclusive techniques to completely eliminate negative comments from Google Listings.</p>
                </div>
            </div>
            <!--Wpsite-box-end-->
            <!--Wpsite-box-start-->
            <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/Testimonials.png'); ?>" alt="Testimonials on Website">
                <div class="consider">
                    <h3>Testimonials on Your Website:</h3>
                    <p>We do request all of your trusted customers to fill feedback. This feedback is published on the wall of their social media platforms ( Facebook, Twitter, Google+ or whichever they choose) as well as on your website as a testimonial. </p>
                </div>
            </div>
            <!--Wpsite-box-end-->
        </div>
        <div class="cont4-right col_new1">
            <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/business.png'); ?>" alt="business positive aspects">
                <div class="consider">
                    <h3>Explore Positive Aspects of the Individual and the Business</h3>
                    <p>The reviews, post and comments that appear on Google pages while searching for your business, as well as past comments, and, Facebook, Twitter uploads, do affect your business in a good or bad way. The negative remarks about your individual personality also put ruinous effects on your business. With the help of SEO techniques, SMM (Social Media Marketing) and Content Marketing tricks we ensure that only positive aspects and reviews are highlighted on search engines while the negative reviews and unwanted past uploads from your side go unnoticed. </p>
                </div>
            </div>
            <!--Wpsite-box-end-->
            <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/we-collect-reviews.png'); ?>" alt="collect reviews">
                <div class="consider">
                    <h3>We Restrain Reviews from All Major Websites:</h3>
                    <p>From Yellow Pages to LinkedIn, Google, Bing, Yahoo, Google+, Twitter to random Google reports and negative site listings, we point out everything that is online and that can impact your business. Instead of the bad comments we mark latest and positive reviews that give small business a new and fresh reward.</p>
                </div>
            </div>
            <!--Wpsite-box-end-->
            <!--Wpsite-box-start-->
            <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/reputation-monitoring.png'); ?>" alt="reputation monitoring">
                <div class="consider">
                    <h3>Online Reputation Monitoring:</h3>
                    <p>Digital SEO gives you complete monitoring of your business. We analyze reviews from all of the websites and share them with you. We provide you daily notifications on your business reputation management as well as weekly detailed reports so that nothing goes missing from your site. We do complete care of your business online reputation so that you can focused on much important things, like obtaining revenue target of this year.</p>
                </div>
            </div>
            <!--Wpsite-box-end-->
        </div>
    </div>
</div>
<div class="clear"></div>


<div style="display: none;" id="slidingDiv" >
    <div class="slidingDiv">
        <div class="toogle-close">
            <a href="#" onClick="showSlidingDiv();
                    return false;"><img src="<?php echo base_url('public/images/close-btn01.png'); ?>" alt="Close button"></a></div>
        <div class="slide-popup-box">
            <h4>We'll call you soon</h4>
            <p>leave your details</p>
            <div class="lets-talk-out"> </div>
            <div class="slide-popup-box-form-main">
                <div class="slide-popup-box-form">



                    <form method="POST" id="lets-talk-frm" action="<?php echo base_url('contactMe'); ?>">

                        <input type="text"  name="first_name" placeholder="First Name:" >
                        <input type="text"  name="last_name" placeholder="Last Name:"  >

                        <input type="text"  name="email" placeholder="Email:" >
                        <input type="text"  name="address" placeholder="Address" >
                        <input type="text"  name="mobile" placeholder="Mobile:" >
                        <input type="hidden" name="slider_unlock" value="02" >
                        <input type="text"  name="description" placeholder="Description" id="description" >
                        <div id="slider_full_1"></div>
                        <input type="submit" id="lets-talk" value="submit" name="submit">
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="letstalk-wrap" class="clearfix">
    <div class="container clearfix">
        <div class="lets-talk-row clearfix">
            <h4>Let's Talk</h4>
            <p>We'd love to answer any questions you may have. Contact us and discuss your business objectives & we will let you know how we can help along with a Free Quote.</p>
            <ul>
                <li><span></span>
                    <figure><img alt="give us call" src="<?php echo base_url('public/images/roundphn.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>

                    <p><small>Give us a call</small>+91 9811747579</p>
                </li>
                <li><span></span>
                    <figure><img alt="write to us" src="<?php echo base_url('public/images/roundmsg.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>
                    <p><small>Write to us</small><a href="mailto:info@techmode.in">info@techmode.in</a></p>
                </li>
                <li><span></span>
                    <figure><img alt="video chat with us" src="<?php echo base_url('public/images/skype.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>
                    <p><small>Video chat with us</small>info_529965</p>
                </li>
                <li>
                    <div class="toogle-part">
                        <a href="#" onClick="showSlidingDiv();
                                return false;">
                            <span></span>
                            <figure><img alt="leave your details" src="<?php echo base_url('public/images/mobile.png'); ?>"></figure>
                            <div class="ani_bg-2"> </div>
                            <div class="ani_bg"></div>
                            <p><small>Have us call you</small>leave your detail</p>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('public/js/toggle.js'); ?>"></script>
<script>

                            k("#lets-talk-frm").validate({
                                rules: {
                                    name: "required",
                                    email: {
                                        required: true,
                                        email: true
                                    },

                                    //skype: "required",
                                    mobile: {
                                        required: true,
                                        digits: true,
                                        minlength: 7
                                    },
                                    date: "required",
                                    time: "required",

                                    /*captcha: {
                                     required: true,
                                     remote: {
                                     url: "portfolio/recaptcha/validate",
                                     
                                     }
                                     } */

                                },
                                messages: {
                                    name: '',
                                    email: '', skype: '', mobile: '', date: '', time: '', phone: '',
                                },
                            });

</script><!--excited lets talk-->
<!--From the blog section-->

<div id="social-share-wrap">
    <div class="addthis_toolbox addthis_default_style addthis_32x32_style">

        <div class="container clearfix">
            <ul>
                <li> <figure><img src="<?php echo base_url('public/images/fb-icon.png'); ?>" alt="fb icon"></figure>
                    <h5><a target="_blank" href="">
                            3517+
                        </a> </h5>
                    <h6>Fans on Facebook</h6>
                </li>
                <li> <figure><img src="<?php echo base_url('public/images/twitter-icon.png'); ?>"  alt="twitter icon"></figure>
                    <h5><a target="" href="" class="" rel="nofollow">
                            2777+
                        </a> </h5>
                    <h6>Followers on Twitter</h6>
                </li>
                <li> <figure><img src="<?php echo base_url('public/images/linkedin-icon.png'); ?>" alt="linkedin icon"></figure>
                    <h5> <a target="_blank" href="">1351+</a></h5>
                    <h6>Techmode on Linkedin</h6>
                </li>

            </ul>
        </div>
    </div>
</div>





<?php require_once(APPPATH . 'views/footer/footer.php'); ?>









