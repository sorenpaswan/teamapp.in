<?php require_once(APPPATH . 'views/header/header.php'); ?>
<meta name="Description" content="Hire your own virtual team of full time PHP developers expert in PHP programming, Ajax, eCommerce solutions, custom website & apps development." />
<meta name="robots" content="index, follow" />

<?php require_once(APPPATH . 'views/header/headerpage.php'); ?>
<div id="wpcont-1-outer">
  <div class="container clearfix">
    <div class="wpcont-1-left-seo">
      <h1>Social Media Marketing </h1>
      <h5><b>Exclusively for Expanded reach, Greater promotions,
        Increased traffic, Better sales and Boosting Brand image and popularity</b></h5>
      <p>TIS India offers you social media consultancy and ultimate solution to all of your social media marketing problems. Social Media marketing has its own rules and regulations that govern its functioning. Only a professional like TIS India can help you get your word out to a majority of people, in fact your word will get out to more than half of the world's population, who is using one or other platform of social media.</p>
    </div>
    <div class="wpcont-1-right g-search"><img src="<?php echo base_url('public/images/sociam-media-banner.png');?>" alt="social media marketing"></div>
  </div>
</div>
<div id="wpcont3-outer">
  <div class="container clearfix">
    <div class="want-seo">
      <h3>For a focused social media plan</h3>
      <div class="request-seo"> <a href="<?php echo base_url('/request-Quote');?>" class="green-bt">Request a quote</a> </div>
    </div>
  </div>
</div>
<!--Hire a Developer section-->
<div id="wpcont5-outer" class="considr-coustmazation">
  <div class="container clearfix">
    <h2>How Social Media can transform your online business</h2>
    <div class="cont4-left col_new1">
      <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/we-will-help.png');?>" alt="help you to speak louder">
        <div class="consider">
          <h3>We Will Help You Speak Louder:</h3>
          <p>We target all social media platforms. From Facebook (1 trillion users) to Twitter (16% US citizens use it actively), from Pinterest (the visual sensation) to Google+ (a strong Facebook competitor), linkedIn and YouTube, we leave no rock unturned. </p>
        </div>
      </div>
      <!--Wpsite-box-end-->
      <!--Wpsite-box-end-->
      <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/we-offer-you.png');?>" alt="Offer You Individual Attention">
        <div class="consider">
          <h3>We Offer You Individual Attention:</h3>
          <p>Every business is unique. In social media we cannot run all media campaigns with one standard strategy. We have no single formula strategy. Our professionals study your business thoroughly and then cultivate an individual social media plan for your business. </p>
        </div>
      </div>
      <!--Wpsite-box-end-->
      <!--Wpsite-box-start-->
      <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/we-help-you.png');?>" alt="International Recognition">
        <div class="consider">
          <h3>We Help You Get International Recognition:</h3>
          <p>Exposure and access to the target audience and to target market is essential for successful survival of any business. We help you get the fame and recognition that was once reserved for presidents, prime ministers, Rock stars and sports men. Be prepared to see your business on top. </p>
        </div>
      </div>
      <!--Wpsite-box-end-->
      <!--Wpsite-box-start-->
      <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/social-media.png');?>" alt="smo is mixture of seo and content marketing">
        <div class="consider">
          <h3>TIS India Believes Social Media Is a Mixture of SEO and Content Marketing:</h3>
          <p>Social media does not work alone. Instead social media marketing never neglects its brothers SEO and content marketing. For example, all of the social media platforms like Facebook, Twitter, LinkedIn, Google+ or YouTube need content in one or the other form. Moreover, it is essential that the keywords should be placed in such a way that your social media posts should come on at least second or third search engine page. <br>
            Most of the social media marketers neglect this important aspect. We on TIS India pay special attention to minutest details of your social media life. We make sure that your social media posts are in coherence with SEO and follow rules and regulations of content marketing. Do we charge extra money for this? Absolutely not! Our team is dedicated towards its work; we do not charge hidden charges and do your work in most professional manner. </p>
        </div>
      </div>
      <!--Wpsite-box-end-->
    </div>
    <div class="cont4-right col_new1">
      <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/target-audience.png');?>" alt="target audience">
        <div class="consider">
          <h3>We Will Help You Get Your Target Audience:</h3>
          <p>Every business has its target audience. TIS India knows that your business requires individual attention and it has different audience. We help you achieve your target audience locally as well as internationally.</p>
        </div>
      </div>
      <!--Wpsite-box-end-->
      <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/we-manage.png');?>" alt="Manage Your Reputation">
        <div class="consider">
          <h3>We Manage Your Reputation:</h3>
          <p>On social media rapport building plays a very important role. We believe it&rsquo;s our responsibility to take care of your company&rsquo;s reputation. We consistently follow all of the social media platforms actively. No comment is left unanswered; no misleading or ambiguous statements are issued. Everything is done by humans and is done for humans. </p>
        </div>
      </div>
      <!--Wpsite-box-end-->
      <!--Wpsite-box-start-->
      <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/dedication.jpg');?>" alt="time dedication">
        <div class="consider">
          <h3>Dedication, Serious Effort and Positive Results</h3>
          <p>We on TIS India know that social media is a platform that if triggered rightly can generate hundreds and thousands of leads. At the same time, it is one of the most time consuming internet marketing strategies. Due to this reason, most of the social media marketers do the business in haste. As a result you will get likes, shares and perhaps some viral posts but very few true leads and eventually the campaign will die down. The dedication required by social media is not easy to give. We give you that dedication, serious effort and our fullest potential. We are with you until you want us; we are with you until your business reaches that height that you once dream of. </p>
        </div>
      </div>
      <!--Wpsite-box-end-->
      <!--Wpsite-box-start-->
      <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/my-marketer.png');?>)" alt="problem solving">
        <div class="consider">
          <h3>My Marketer Has Badly Ruined My Social Media Marketing, What Now?</h3>
          <p>A badly operated social media marketing does more harm than good, especially in these days when there is cut throat competition between rivals. However, there is always space for creative ideas and original work. Our professional team will help fix your social media blunders. We will also take care of your rapport building with your customers and followers. We guarantee you positive results, after all hard work always pays back! </p>
        </div>
      </div>
      <!--Wpsite-box-end-->
    </div>
  </div>
</div>
<!--customization of your site section-->
<div id="wpcont4-outer" class="clearfix">
  <div class="container clearfix">
    <h2>Our Social Media Marketing Services include:</h2>
    <div class="wpcont5left good-reasons">
      <ul>
        <li> <figure><img src="<?php echo base_url('public/images/facebook.png');?>" alt="Facebook logo"></figure> Facebook page integration, Management and Optimization </li>
        <li> <figure><img src="<?php echo base_url('public/images/twitter.png');?>" alt="Twitter logo"></figure> Twitter account integration, Management and Optimization </li>
        <li> <figure><img src="<?php echo base_url('public/images/youtube.png');?>" alt="youtube logo"></figure> YouTube video production and optimization </li>
        <li> <figure><img src="<?php echo base_url('public/images/branding.png');?>" alt="branding"></figure>
          <div class="consider"> Branding and Reputation Management </div>
        </li>
        <li> <figure><img src="<?php echo base_url('public/images/blog.png');?>" alt="tis-india"></figure> Blog Creations, Development and Management </li>
      </ul>
    </div>
    <div class="wpcont5right good-img"> <img src="<?php echo base_url('public/images/social-media-marketing.png');?>" alt="smm"> </div>
  </div>
</div>




<div class="clear"></div>

<div style="display: none;" id="slidingDiv" >
<div class="slidingDiv">
  <div class="toogle-close">
  <a href="#" onClick="showSlidingDiv(); return false;"><img src="<?php echo base_url('public/images/close-btn01.png');?>" alt="Close button"></a></div>
  <div class="slide-popup-box">
  <h4>We'll call you soon</h4>
  <p>leave your details</p>
  <div class="lets-talk-out"> </div>
    <div class="slide-popup-box-form-main">
      <div class="slide-popup-box-form">
      
   

           <form method="POST" id="lets-talk-frm" action="<?php echo base_url('contactMe');?>">
                      
        <input type="text"  name="first_name" placeholder="First Name:" >
              <input type="text"  name="last_name" placeholder="Last Name:"  >

        <input type="text"  name="email" placeholder="Email:" >
        <input type="text"  name="address" placeholder="Address" >
        <input type="text"  name="mobile" placeholder="Mobile:" >
        <input type="hidden" name="slider_unlock" value="02" >
        <input type="text"  name="description" placeholder="Description" id="description" >
          <div id="slider_full_1"></div>
        <input type="submit" id="lets-talk" value="submit" name="submit">
        </form>
        </div>
        
      </div>
    </div>
  </div>
</div>

 <div id="letstalk-wrap" class="clearfix">
    <div class="container clearfix">
      <div class="lets-talk-row clearfix">
       <h4>Let's Talk</h4>
        <p>We'd love to answer any questions you may have. Contact us and discuss your business objectives & we will let you know how we can help along with a Free Quote.</p>
      <ul>
          <li><span></span>
            <figure><img alt="give us call" src="<?php echo base_url('public/images/roundphn.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
      
            <p><small>Give us a call</small>+91 8800535588</p>
          </li>
          <li><span></span>
            <figure><img alt="write to us" src="<?php echo base_url('public/images/roundmsg.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
            <p><small>Write to us</small><a href="mailto:info@techmode.in">info@techmode.in</a></p>
          </li>
          <li><span></span>
            <figure><img alt="video chat with us" src="<?php echo base_url('public/images/skype.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
            <p><small>Video chat with us</small>info_529965</p>
          </li>
          <li>
      <div class="toogle-part">
      <a href="#" onClick="showSlidingDiv(); return false;">
      <span></span>
              <figure><img alt="leave your details" src="<?php echo base_url('public/images/mobile.png');?>"></figure>
              <div class="ani_bg-2"> </div>
              <div class="ani_bg"></div>
              <p><small>Have us call you</small>leave your detail</p>
        </a>
        </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
 <script type="text/javascript" src="<?php echo base_url('public/js/toggle.js');?>"></script>
 <script>
  
  k("#lets-talk-frm").validate({
    rules: {
    name: "required",
    email: {
      required: true,
      email: true
    },
    
    //skype: "required",
    mobile:{
    required: true,
    digits: true,
     minlength: 7
    },
    date: "required",
    time: "required",
  
    /*captcha: {
      required: true,
      remote: {
      url: "portfolio/recaptcha/validate",
      
      }
    } */  
    
    },
    messages:{
      name: '',
      email: '', skype: '', mobile: '', date: '', time: '', phone: '',
    },
    });
  
 </script><!--excited lets talk-->
    <div id="wpcont9-outer" class="clearfix">

<div class="container clearfix">
      <h2>From the blog</h2>
      <p>Keep on track of latest news and updates</p>
      <figure><img src="<?php echo base_url('public/images/divider.png');?>" alt="divider"></figure>

      <div class="wpcont9left">
      <h3>9 Prominent Tips You Should Consider For Facebook Retargeting Ads</h3>  
      <p>

In today’s era, where practically every business is online, just having an online presence is not enough. Studies have shown that roughly 96 percent of the visitors on your site are not even rea ...<a href="blog/tips-for-facebook-retargeting-ads/index.html">FIND OUT MORE</a></p>
    </div>
      <div class="wpcont9left">
      <h3>Rebranding- A Complete Guide to Planning and Executing It Successfully</h3> 
      <p>

So are you all set to dive deeper into the process of rebranding? Undoubtedly, rebranding might be an exciting moment for a few; it is a mere unfortunate step for the others. But, whatever it is,  ...<a href="blog/rebranding-a-complete-guide-to-planning-and-executing-it-successfully/index.html">FIND OUT MORE</a></p>
    </div>
    </div>
    
     </div>
 <!--From the blog section-->

<div id="social-share-wrap">
  <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
    
    <div class="container clearfix">
      <ul>
        <li> <figure><img src="<?php echo base_url('public/images/fb-icon.png');?>" alt="fb icon"></figure>
          <h5><a target="_blank" href="">
            3517+
            </a> </h5>
      <h6>Fans on Facebook</h6>
        </li>
        <li> <figure><img src="<?php echo base_url('public/images/twitter-icon.png');?>"  alt="twitter icon"></figure>
          <h5><a target="" href="" class="" rel="nofollow">
            2777+
            </a> </h5>
      <h6>Followers on Twitter</h6>
        </li>
        <li> <figure><img src="<?php echo base_url('public/images/linkedin-icon.png');?>" alt="linkedin icon"></figure>
          <h5> <a target="_blank" href="">1351+</a></h5>
      <h6>Techmode on Linkedin</h6>
        </li>
        <!-- <li> <figure><img src="/images/gplus-icon.png" alt="gplus icon"></figure>
          <h5><a target="_blank" href="https://plus.google.com/+Tisindiaservices/posts">Add tis</a></h5>
          <h6>to your Circle on G+</h6>
        </li> -->
      </ul>
    </div>
  </div>
</div>
 <?php require_once(APPPATH . 'views/footer/footer.php'); ?>

