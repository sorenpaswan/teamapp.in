<?php require_once(APPPATH . 'views/header/header.php'); ?>
<meta name="Description" content="Hire your own virtual team of full time PHP developers expert in PHP programming, Ajax, eCommerce solutions, custom website & apps development." />
<meta name="robots" content="index, follow" />

<?php require_once(APPPATH . 'views/header/headerpage.php'); ?>
<div id="wpcont-1-outer">
  <div class="container clearfix">
    <div class="wpcont-1-left-seo">
      <h1>SEO India </h1>
      <h5> <b>SEO services are focused on traffic growth & better sales</b> </h5>
      <p>Efficacious SEO is what you need for first page rankings, to get more traffic & enhanced sales. We have a team of SEO experts who carve a great SEO techniques with a blend of social media signals & content marketing efforts. From on page & off page optimization to placing your owned media & content links in an valuable position, we develop your site completely SEO friendly for higher traffic, more shares & valuable earned media as well. Our team also measure analytics to keep well with the latest Google algorithm updates & guidelines to capture intelligent online marketing services. </p>
      <!--<div class="request-button"> <a href="hire-seo-consultant.html" class="request-btn ppc">Hire SEO Expert </a></div>-->
    </div>
    <div class="wpcont-1-right"><img src="<?php echo base_url('public/images/seo-banner-image.png');?>" alt="SEO Services"></div>
  </div>
</div>

<!--we-love-code-outer-part-start-->
<div class="we-love-code-outer"> 
  
  <!--we-love-code-part-start-->
  <div class="we-love-code"> 
    
    <!--we-love-code-left-start-->
    <div class="we-love-code-left"> <img src="<?php echo base_url('public/images/we-love-code.jpg');?>" alt="SEO India"> </div>
    <!--we-love-code-left-end--> 
    
    <!--we-love-code-right-start-->
    <div class="we-love-code-right">
      <h4>SEO Case Study: How can we rise up conversion rate by 1,508% for Tour My India</h4>
      <p>A rewarding digital marketing campaign that helped Tour My India, one of the fastest expanding Travel companies in India to get conversions improved by a huge figure of 1,508% in just few months. Our Digital Marketing team was accountable for strategizing, planning & implementing marketing campaigns that includes SEO primarily & PPC, Social media, Conversion optimization as well that performed equally well. Site started ranking on top results in Google driving <strong>1,958% more non-paid traffic & 1,386% more leads</strong> from organic traffic. <br>
    <a href="">Download and Analyze the complete digital marketing case study</a>   </p>
    </div>
    <!--we-love-code-right-end--> 
    
  </div>
  <!--we-love-code-part-end--> 
  
</div>
<!--we-love-code-outer-part-end--> 

<!--hire-analist-we-outer-start-->
<div class="hire-analist-we-outer"> 
  
  <!--what-we-do-start-->
  <div class="what-we-do">
    <h2>Our SEO Services Include:</h2>
    <div class="what-we-do-img"><img src="<?php echo base_url('public/images/web-analytic.jpg');?>" alt="image"></div>
    
    <!--what-we-do-left-start-->
    <div class="what-we-do-left"> 
      
      <!--box-1-start-->
      <div class="box-1">
        <h4>Site Study & Optimization</h4>
        <p>Through comprehensive SEO study & deploying on-page optimization solutions to the site, we remove the flaws & optimize the site for search engines and users. From right keyword research to SEO tags, stronger internal linking to content marketing optimization, there are around 150+ ranking signals we take care of. </p>
      </div>
      <!--box-1-end--> 
      
      <!--box-3-start-->
      <div class="box-3">
        <h4>Persona Modelling </h4>
        <p>We analyse, collect, research & review your business data along with the competitors' status to identify target audience, their interests and demography & build cutting-edge SEO techniques aimed on driving  traffic that have potential to engage & convert.</p>
      </div>
      <!--box-3-end--> 
      
      <!--box-5-start-->
      <div class="box-5">
        <h4>Content Marketing & Outreach</h4>
        <p>Analysing the content marketing channel as a part of our off-page optimization techniques, we develop content ideas around user persona & search engines, optimize & publish alluring content, outreach to a wider audience, set-up & handle blogs, promote on social media - all this to get websites move up on search results. </p>
      </div>
      <!--box-5-end--> 
      
      <!--box-7-start-->
      <div class="box-7">
        <h4>Link Audit</h4>
        <p>Link play an important role in better ranking, bad links can harm the site. From accurate link audit to link removal and then further submitting reconsideration request to Google, our SEO experts handle the whole process required to secure the site from penalty, if hit by Google Penguin.  </p>
      </div>
      <!--box-7-end--> 
      
    </div>
    <!--what-we-do-left-end--> 
    
    <!--what-we-do-right-start-->
    <div class="what-we-do-right"> 
      
      <!--box-2-start-->
      <div class="box-2">
        <h4>Keyword & Opportunity Research </h4>
        <p>Our SEO experts withdraw online opportunities by finding monitoring productive keywords, monitoring density of highly competitive search terms/ phrases to acknowledge for top-rankings and finding more opportunities through long tail related searches that can increase the organic traffic. </p>
      </div>
      <!--box-2-end--> 
      
      <!--box-4-start-->
      <div class="box-4">
        <h4>SEO Integrated Marketing</h4>
        <p>SEO is more powerful when collabrating with other marketing action including social media, content marketing, PR & Branding focused on the common goal of improving traffic & sales. As a full service digital marketing company, we plan integrating marketing path that can turnout faster results, more traffic & enhanced engagement. </p>
      </div>
      <!--box-4-end--> 
      
      <!--box-6-start-->
      <div class="box-6">
        <h4>In-Depth Analytics  </h4>
        <p>We have a team of Google Analytics certified SEO experts who know how Analytics can help in monitoring & optimizing the site's performances. From putting up goals, tracking events, making custom reports to setting Intelligence alerts, we manage all to provide actionable insights.</p>
      </div>
      <!--box-6-end--> 
      
      <!--box-8-start-->
      <div class="box-8">
        <h4>Monitoring & Reporting</h4>
        <p>Our experienced SEO services also enclose detailed  SEO reporting, monitoring & on-going consultation through which you can adjust your marketing techniques accordingly & formulate strategy to make benefits. From regular analytics monitoring to site audits, customised reports to search engine transformation charts, detailed backlink review to rank tracking; we keep you knowledgeable about every status  so as to keep you informed with your site's growth.</p>
      </div>
      <!--box-8-end--> 
      
    </div>
    <!--what-we-do-right-end--> 
    
  </div>
  <!--what-we-do-end--> 
  
</div>
<!--hire-analist-we-outer-end--> 

<!--start-->
<div class="hire-analist-outer">
<div class="hire-analist">
<h1>10 Good Reasons to outsource SEO projects to us !!</h1>
<p></p>

<!--left-part-start-->
<div class="hire-analist-left">
<ul>
<li>
  <figure><img src="<?php echo base_url('public/images/right.png');?>" alt="image"></figure>
  <p>To make your business unique is our SEO strategy </p>
</li>
<li>
  <figure><img src="<?php echo base_url('public/images/right.png');?>" alt="image"></figure>
  <p>You get to hire experts in SEO who follow latest techniques updates & ethical SEO trends to offer intelligent SEO services. </p>
</li>
<li>
  <figure><img src="<?php echo base_url('public/images/right.png');?>" alt="image"></figure>
  <p>Practicing risk-free SEO is our forte. Our SEO experts rank up the client website & so they strictly follow good SEO ethics while running the campaign. </p>
</li>
<li>
  <figure><img src="<?php echo base_url('public/images/right.png');?>" alt="image"></figure>
  <p>As a leading SEO company based in India, we have worked with clients worldwide including big organisation & helped them achieve a higher rate of growth than expected in sales through organic traffic.</p>
</li>
<li>
  <figure><img src="<?php echo base_url('public/images/right.png');?>" alt="image"></figure>
  <p>Our SEO agency encompass a team of SEO experts who are certified marketers and Google Digital & Ecommerce certified experts.</p>
</li>
</ul>
</div>
<!--left-part-end--> 

<!--right-part-start-->
<div class="hire-analist-right">
  <ul>
    <li>
      <figure><img src="<?php echo base_url('public/images/right.png');?>" alt="image"></figure>
      <p>Our SEO experts have a experience with some enterprise level tools like Visual website optimizer, Brightedge, Moz, Ahrefs, etc.</p>
    </li>
    <li>
      <figure><img src="<?php echo base_url('public/images/right.png');?>" alt="image"></figure>
      <p>Our team of expert SEO consultants have save many sites recover them from Google penalties (Penguin & Panda hits)</p>
    </li>
    <li>
      <figure><img src="<?php echo base_url('public/images/right.png');?>" alt="image"></figure>
      <p>From small to large businesses, we treat equally - the best SEO service. </p>
    </li>
    <li>
      <figure><img src="<?php echo base_url('public/images/right.png');?>" alt="image"></figure>
      <p>We assured guarantee top rankings, which nobody can. Altogether we also build your organic traffic which is the ultimate purpose to hire SEO.</p>
    </li>
  <li>
      <figure><img src="<?php echo base_url('public/images/right.png');?>" alt="image"></figure>
      <p>We're not a newcomer agency or just a link building company, our SEO agency have a team of experienced SEO experts who've got a decade of experience in driving traffic & sales. </p>
    </li>
  </ul>
</div>
<!--right-part-end-->

</div>
</div>


<!--end-->




<div class="clear"></div>

<div class="add-logo-support-outer">
  <div class="container">
    <div class="logo-section">
      <h3>Our Clients Are Published On</h3>
      <ul class="bxslider">
<li><img src="<?php echo base_url('public/images/logo/huffingtonpost.png');?>" /></li>
<li><img src="<?php echo base_url('public/images/logo/chicagotribune.png');?>" /></li>
<li><img src="<?php echo base_url('public/images/logo/sap.png');?>" /></li>
<li><img src="<?php echo base_url('public/images/logo/entrepreneur.png');?>" /></li>
<li><img src="<?php echo base_url('public/images/logo/theinstitute.png');?>" /></li>
<li><img src="<?php echo base_url('public/images/logo/wired.png');?>" /></li>
<li><img src="<?php echo base_url('public/images/logo/buzzfeed.png');?>" /></li>
<li><img src="<?php echo base_url('public/images/logo/tmcnet.png');?>" /></li>
<li><img src="<?php echo base_url('public/images/logo/socialmediatoday.png');?>" /></li>
<li><img src="<?php echo base_url('public/images/logo/tech.png');?>" /></li>
        
</ul>
</div>
    </div>
</div>

<div style="display: none;" id="slidingDiv" >
<div class="slidingDiv">
  <div class="toogle-close">
  <a href="#" onClick="showSlidingDiv(); return false;"><img src="<?php echo base_url('public/images/close-btn01.png');?>" alt="Close button"></a></div>
  <div class="slide-popup-box">
  <h4>We'll call you soon</h4>
  <p>leave your details</p>
  <div class="lets-talk-out"> </div>
    <div class="slide-popup-box-form-main">
      <div class="slide-popup-box-form">
      
   

           <form method="POST" id="lets-talk-frm" action="<?php echo base_url('contactMe');?>">
                      
        <input type="text"  name="first_name" placeholder="First Name:" >
              <input type="text"  name="last_name" placeholder="Last Name:"  >

        <input type="text"  name="email" placeholder="Email:" >
        <input type="text"  name="address" placeholder="Address" >
        <input type="text"  name="mobile" placeholder="Mobile:" >
        <input type="hidden" name="slider_unlock" value="02" >
        <input type="text"  name="description" placeholder="Description" id="description" >
          <div id="slider_full_1"></div>
        <input type="submit" id="lets-talk" value="submit" name="submit">
        </form>
        </div>
        
      </div>
    </div>
  </div>
</div>

 <div id="letstalk-wrap" class="clearfix">
    <div class="container clearfix">
      <div class="lets-talk-row clearfix">
       <h4>Let's Talk</h4>
        <p>We'd love to answer any questions you may have. Contact us and discuss your business objectives & we will let you know how we can help along with a Free Quote.</p>
      <ul>
          <li><span></span>
            <figure><img alt="give us call" src="<?php echo base_url('public/images/roundphn.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
      
            <p><small>Give us a call</small>+91-8800535588</p>
          </li>
          <li><span></span>
            <figure><img alt="write to us" src="<?php echo base_url('public/images/roundmsg.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
            <p><small>Write to us</small><a href="mailto:info@techmode.in">info@techmode.in</a></p>
          </li>
          <li><span></span>
            <figure><img alt="video chat with us" src="<?php echo base_url('public/images/skype.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
            <p><small>Video chat with us</small>Techmode</p>
          </li>
          <li>
      <div class="toogle-part">
      <a href="#" onClick="showSlidingDiv(); return false;">
      <span></span>
              <figure><img alt="leave your details" src="<?php echo base_url('public/images/mobile.png');?>"></figure>
              <div class="ani_bg-2"> </div>
              <div class="ani_bg"></div>
              <p><small>Have us call you</small>leave your detail</p>
        </a>
        </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
<?php require_once(APPPATH . 'views/footer/footer.php'); ?>