
<?php require_once(APPPATH . 'views/header/header.php'); ?>
<br><br><br><br>       

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /> 
<meta name="Description" content="techmode India is one of the leading digital agency in India. Hire us for creative website design, web application development & digital marketing. 100% satisfaction guaranteed." />
<meta name="robots" content="index, follow"/> 
<?php require_once(APPPATH . 'views/header/headerpage.php'); ?>

<div class="web-design-bg">
    <style type="text/css">
        .your-apps {
            width: 100%;
            background: url(public/images/web-design-bg_dm.jpg) center 0 no-repeat!important;
            height: 318px;
            margin-top: 72px;
        }
    </style>
</div>

<div class="your-apps" id="wpcont-2-outer">
    <div class="app-continer">
        <h1>Digital Marketing Agency India</h1>
        <p>Does your business need focused Digital Marketing campaigns ?</p>
        <div class="request-button"> <a href="<?php echo base_url('/request-Quote'); ?>" class="request-btn">Request a Quote</a> 
        </div>  
    </div>

    <!--top banner end here-->
    <!--Degital marketing start here-->
    <div class="degital-real clearfix">
        <div class="container clearfix">

            <h2>Digital is the new 'idea', let your business reach right people through right channels!!</h2>
            <p>We at Technical Solution, digital marketing company in India offer creative marketing strategies that help you reach out to the right people with the right customer’s message through a right channel. Even if you have a lowest budget for your marketing campaign, we can suggest which service your brand needs most to keep it alive performance. Our experts can allocate your marketing budget intelligently across various digital marketing services depending on your business needs, its revenue goals & target niche and measure ROI Return on Investment. We have latest digital marketing tools to provide you effective solutions within stipulated time frame..</p>
        </div>
    </div>
    <div class="digital-search-opt clearfix">
        <div class="container clearfix">
            <div class="search-opt-panel">
                <h2>SEO</h2>
                <h5><em>Get Organic reach, better traffic, more revenue your business growth!!</em></h5>
                <p>Potential buyers tend to look for products or services via google results gain their focus first. To convert all these searchers to customers, our digital marketing agency provide your business on higher search engine rankings  for <strong>better business  traffic.</strong>.Keeping digital marketing performance in mind, our SEO team offers digital marketing services  based on latest Google algorithmic & SEO trends fused with campaign based digital marketing solution. All these attributes makes our firm, one of the best and digital marketing company in India.</p>
                <div class="digital-find-out"><a href="#">Optimize for SEO </a></div>
            </div>

            <div class="search-opt-panel-right"> <img src="<?php echo base_url('public/images/digital-search-opt.png'); ?>" alt="organic reach" >



            </div>
        </div>
    </div>
    <section class="analytics clearfix">
        <div class="analytics-mid_banner">
            <aside class="marketing_agency">
                <h4>Analytics</h4>
                <h5><em>Analyse results, measure performance & goal strategies</em></h5>
                <p>We Technical Expert Analysing performance & end-results is central to our digital marketing solution but, our innovative digital marketing services don't just stop here.  We are are  Google Analytics certified expert who are adept in finding out why your online presence failed to gather ample number of qualified leads and  suggest how to you improvement factors. We have the  experience working for top industries, our digital marketing agencies tracking systems to find new opportunities for bringing more specific traffic, improving conversions & optimizing tactics to maintain existing customers.  we are known in this industry for our timely execution of projects and economical pricing policy thats why we are counted among leading digital marketing agency in India.</p><br><br>
                <div class="digital-find-out"><a href="#">Know more on our Analytics Model </a></div>
            </aside>
        </div>
</div>
</section>
<div class="digi-publishing clearfix">
    <div class="container clearfix">
        <div class="digi-publishing-panel"> <img src="<?php echo base_url('public/images/digi-publise.jpg'); ?>" alt="content marketing and publishing"> </div>
        <div class="digi-publishing-content-panel">
            <h2>Content Marketing & Publishing</h2>
            <h5><em>Compelling content drives cross-channel engagement</em></h5>
            <p> Our digital marketing company offers full-time  content marketiing through an <strong>integrated content creation & publishing plan</strong> that includes drafting a competent strategy & then fuelling your online presence through branded blogs, quality custom-written news, blogs, Articles & info graphics. Our high quality content is cached fast & ranked top by the search engines well. We always strive to provide you best-in-domain digital marketing services at the best friendly prices.</p>
            <div class="digital-find-out"><a href="#">How we fuel your brand with Content Marketing </a></div>
        </div>
    </div>
</div>
<div class="digital-search-opt clearfix">
    <div class="container clearfix">
        <div class="search-opt-panel">
            <h2>Paid Advertising - Search & Display</h2>
            <h5><em>Uncovering paid opportunities to grow your business fast!!</em></h5>
            <p>When it comes to paid advertising, you need focused advertising campaign built on creative engagement model that can guarantee maximum response in minimum marketing prices/ dollars. We have the google adwords certified experts of our digital marketing company invest time in creating successful paid advertising campaigns for both search & display ,Remarketing media using an intelligent, research & technology clubbed with <strong>real-time targeting and conversion optimization method.</strong> </p>
            <div class="digital-find-out"><a href="#">Generate leads with focused paid marketing campaigns</a></div>
        </div>
        <div class="search-opt-panel-right"> <img src="<?php echo base_url('public/images/digital-ad.png'); ?>" alt="Google image" usemap="#Map"> 
            <map name="Map">
                <area shape="rect" coords="38,109,238,178" href="https://www.google.com/partners/?hl=en-US#a_profile;idtf=2846214492" target="_blank">
                <area shape="rect" coords="329,72,465,219" href="http://advertise.bingads.microsoft.com/en-us/training-accreditation-find-a-pro-directory?CompanyID=18442" target="_blank">
                <area shape="rect" coords="420,184,436,185" href="http://advertise.bingads.microsoft.com/en-us/training-accreditation-find-a-pro-directory?CompanyID=18442" target="_blank">
            </map>
        </div>
    </div>
</div>
<section class="digital-social-second clearfix">
    <div class="analytics-mid_banner">
        <aside class="marketing_agency">
            <h4>Social Media Marketing</h4>
            <h2><em>Because online reputation management impacts business branding</em></h2>
            <p>Many People are spending more time on social media than anywhere else. More than 95% of your existing customers & target audience on Facebook and Twitter & Linked n that's our supper spot. As a prenowned digital marketing company in India, we have  a skilled team of social media marketers to provide you more reliable and effective digital marketing solution. Our digital marketing company targets the comfortable that's already engaged to build a strong branding of your business. Get feedbacks to increasing traffic, the team of our digital marketing company runs a cutting edge social media marketing campaign creating a community of loyal customers for you.</p>
            <div class="digital-find-out"><a href="#">Get started with social media marketing</a></div>
        </aside>
    </div>
</div>
</section>
<div class="conversion-rate clearfix">
    <div class="container clearfix">
        <h2>Conversion Rate Optimization </h2>
        <h3><em>Conversions matter!! A thousand Clicks don't!!</em></h3>
        <p>Gaining visibility is not enough for any business. If your website is not pulling in customers, your online presence is worthless. To succeed in all your marketing efforts, implementing a well-structured conversion rate optimization campaign right from the start. Our digital marketing company has expert team of <strong>specialists apply tried & tested approach</strong> to uplift your conversions & improve website performance. We do 360 degree digital marketing solution specifically designed for your business, we can multiply the impact of your web presence to raise your ROI. Our customers can avail these solutions at user friendly prices. </p>
        <a href="#">Power your ROI model through CRO campaigns </a> <img class="traffic" src="<?php echo base_url('public/images/digital-traffic-img.jpg'); ?>" alt="digital media marketing"> </div>
</div>
<div class="digital-search-opt clearfix">
    <div class="container clearfix">
        <div class="search-opt-panel">
            <h2>Email Marketing</h2>
            <h5><em>Campaigns that directly communicate & prompt to take action</em></h5>
            <p>We are a leading digital marketing company in India and email marketing is not about sending e-mailers to a large database waiting for someone to click & convert. To <strong>ensure maximum digital marketing performance, our email marketing campaigns</strong> are monitored by quality content & audience. Our digital marketing agency does this by designing a focused mailer, choosing the right kind of messaging for right audience, integrating perfect call to action buttons at appropriate places & finally measures the impact for better re-marketing and retargeting..</p>
            <div class="digital-find-out"><a href="#">How we bring Email Marketing Campaigns to life </a></div>
        </div>
        <div class="search-opt-panel-right"> <img src="<?php echo base_url('public/images/digital-email-marketing.jpg'); ?>" alt="email marketing"> </div>
    </div>
</div>
<div class="clear"></div>


<div style="display: none;" id="slidingDiv" >
    <div class="slidingDiv">
        <div class="toogle-close">
            <a href="#" onClick="showSlidingDiv();
                    return false;"><img src="<?php echo base_url('public/images/close-btn01.png'); ?>" alt="Close button"></a></div>
        <div class="slide-popup-box">
            <h4>We'll call you soon</h4>
            <p>leave your details</p>
            <div class="lets-talk-out"> </div>
            <div class="slide-popup-box-form-main">
                <div class="slide-popup-box-form">



                    <form method="POST" id="lets-talk-frm" action="<?php echo base_url('contactMe'); ?>">
                        @csrf
                        <input type="text"  name="first_name" placeholder="First Name:" >
                        <input type="text"  name="last_name" placeholder="Last Name:"  >

                        <input type="text"  name="email" placeholder="Email:" >
                        <input type="text"  name="address" placeholder="Address" >
                        <input type="text"  name="mobile" placeholder="Mobile:" >
                        <input type="hidden" name="slider_unlock" value="02" >
                        <input type="text"  name="description" placeholder="Description" id="description" >
                        <div id="slider_full_1"></div>
                        <input type="submit" id="lets-talk" value="submit" name="submit">
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="letstalk-wrap" class="clearfix">
    <div class="container clearfix">
        <div class="lets-talk-row clearfix">
            <h4>Let's Talk</h4>
            <p>We'd love to answer any questions you may have. Contact us and discuss your business objectives & we will let you know how we can help along with a Free Quote.</p>
            <ul>
                <li><span></span>
                    <figure><img alt="give us call" src="<?php echo base_url('public/images/roundphn.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>

                    <p><small>Give us a call</small>+91 8800535588</p>
                </li>
                <li><span></span>
                    <figure><img alt="write to us" src="<?php echo base_url('public/images/roundmsg.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>
                    <p><small>Write to us</small><a href="mailto:info@techmode.in">info@tecmode.in</a></p>
                </li>
                <li><span></span>
                    <figure><img alt="video chat with us" src="<?php echo base_url('public/images/skype.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>
                    <p><small>Video chat with us</small>info_529965</p>
                </li>
                <li>
                    <div class="toogle-part">
                        <a href="#" onClick="showSlidingDiv();
                                return false;">
                            <span></span>
                            <figure><img alt="leave your details" src="<?php echo base_url('public/images/mobile.png'); ?>"></figure>
                            <div class="ani_bg-2"> </div>
                            <div class="ani_bg"></div>
                            <p><small>Have us call you</small>leave your detail</p>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<script>

    k("#lets-talk-frm").validate({
        rules: {
            name: "required",
            email: {
                required: true,
                email: true
            },

            //skype: "required",
            mobile: {
                required: true,
                digits: true,
                minlength: 7
            },
            date: "required",
            time: "required",

        },
        messages: {
            name: '',
            email: '', skype: '', mobile: '', date: '', time: '', phone: '',
        },
    });

</script><!--toggle-section-part start here-->

<?php require_once(APPPATH . 'views/footer/footer.php'); ?>


