<?php require_once(APPPATH . 'views/header/header.php'); ?>

<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!--<title>Website Design & Web Development Company India - Digital Agency | TIS India</title>-->
<meta name="Description" content="TIS India is one of the leading digital agency in India. Hire us for creative website design, web application development & digital marketing. 100% satisfaction guaranteed." />
<meta name="robots" content="index, follow"/>
<meta property="og:title" content="Digital Marketing Agency - TIS India"/>
<meta property="og:description" content="Digital marketing agency follows innovative ideas of website designing, website development, digital marketing for better user experience, customer satisfaction and branding."/>
</head>
<?php require_once(APPPATH . 'views/header/header1.php'); ?>


<section class=baner-new>
    <div class=contaner>
        <div class="baner-prt-new wp1">
            <h1>We are working in <span>Digital Products</span></h1>
            <p>Contract us for wonderful user-driven web and mobile applications or for result-oriented digital marketing.</p>
            <a href="<?php echo base_url('request-a-proposal'); ?>" class="get-req-bnr req-btn">Request Pricing  </a>
        </div>
    </div>
</section>
<section class=what-we-do-new-outer>
    <div class=contaner>
        <div class="what-we-do-new-main">
            ::before
            <h2><span>We are Working In</span></h2>
            <article class="what-we-do-new-box box-hover block" data-move-y="-100px">
                <figure>
                    <img src="<?php echo base_url('public/images-home/what-we-do-new-img2.png'); ?>" alt="Website Development"><img class="sticky" src="<?php echo base_url('public/images-home/what-we-do-new-img2-hover.png'); ?>" alt="">
                    <figcaption>
                        <h3><a href="<?php echo base_url('web-development-services'); ?>">Web &amp; Mobile Development</a></h3>
                        <p class="just">TECHMODE helps you redefine your online presence by organizing web &amp; mobile applications that integrate your company’s vision and effectively mirror your core competencies and strengths.</p>
                    </figcaption>
                </figure>
            </article>
            <article class="what-we-do-new-box box-hover block" data-move-y="-100px">
                <figure>
                    <img src="<?php echo base_url('public/images-home/what-we-do-new-img4.png'); ?>" alt="Digital Marketing Agency"> <img class="sticky" src="<?php echo base_url('images-home/what-we-do-new-img4-hover.png'); ?>" alt="">
                    <figcaption>
                        <h3> <a href="<?php echo base_url('digital-marketing-agency'); ?>">Digital<br> Marketing</a></h3>
                        <p class="just">TECHMODE helps your business in marketing of products or services in a digital platform like- search engines, websites, social media, email, and mobile apps that stick out your identity in digital world.</p>
                    </figcaption>
                </figure>
            </article>
            <article class="what-we-do-new-box box-hover block" data-move-y="-100px">
                <figure>
                    <img src="<?php echo base_url('public/images-home/what-we-do-new-img1.png'); ?>" alt="Creative Web Design"> <img class="sticky" src="<?php echo base_url('public/images-home/what-we-do-new-img1-hover.png'); ?>" alt="">
                    <figcaption>
                        <h3><a href="<?php echo base_url('web-design-services'); ?>">Creative <br> Design</a></h3>
                        <p class="just">Tech Mode, one of the best Graphic Designing Company in Delhi NCR, provide a comprehensive range of design services for the Industry Verticals. </p>
                    </figcaption>
                </figure>
            </article>
            <article class="what-we-do-new-box box-hover block" data-move-y="-100px">
                <figure>
                    <img src="<?php echo base_url('public/images-home/techmode_iot_76x63.png'); ?>" alt="IOT"> <img class="sticky" src="<?php echo base_url('images-home/techmode_iot_white_76x63.png'); ?>" alt="">
                    <figcaption>
                        <h3><a href="<?php echo base_url('web-design-services'); ?>">The Internet Of Things</a></h3>
                        <p class="just">At TECHMODE we integrate the ideal industry components, ensuring a single point of accountability and deliver the best IoT (Internet of Things) service and solutions for a range of verticals.</p>
                    </figcaption>
                </figure>
            </article>

            <article class="what-we-do-new-box box-hover block" data-move-y="-100px">
                <figure>
                    <img src="<?php echo base_url('public/images-home/techmode_hosting_76x63.png'); ?>" alt="Hosting"> <img class="sticky" src="<?php echo base_url('images-home/techmode_hosting_white_76x63.png'); ?>" alt="">
                    <figcaption>
                        <h3><a href="<?php echo base_url('web-design-services'); ?>">Hosting <br>Services</a></h3>
                        <p class="just">Our web hosting service will keep your website up and running. When you are looking for a shared, dedicated, or virtual server hosting company, Tech Mode ensures you of a fast, secure, and reliable service.</p>
                    </figcaption>
                </figure>
            </article>

        </div>
    </div>
</section>
<div class="our-featured-product-new cf">
    <div class="contaner">
        <h3>Our Products</h3>
    </div>
    <ul class="bxslider">
        <li>
            <div class="contaner">
                <div class="feature-product-main-new cf">
                    <div class="feature-product-main-new-detls cf">

                        <div class="Featured-dtl">
                            <h4>TEAMAPP</h4>
                            <p>TeamApp is a subscriptions-based service.
                                It enables employees to apply travel bookings, log their own project time, mark attendance, mark the hours on timesheet, request vendor payments and request leaves and lots of other things.
                            </p>

                            <ul>
                                <li>
                                    <div class="sprite web-new"></div>
                                    <label><b>HR Automation</b></label>
                                </li>
                                <li>
                                    <div class="sprite html-new"></div>
                                    <label><b>ESS</b></label>
                                </li>

                                <li>
                                    <div class="sprite dov-new wordpress"></div>
                                    <label><b>Tracker</b></label>
                                </li>
                                <li>
                                    <div class="sprite ajax"></div>
                                    <label><b>Many More</b></label>
                                </li>
                            </ul>
                            <a class="view-case" href="http://teamapp.in/webapp/" target="_blank" rel="nofollow">launch website <img src="<?php echo base_url('public/images-home/lunch-web.png'); ?>" alt=""></a>
                        </div>
                    </div>
                    <div class="feature-product-main-new-img">
                        <img src="<?php echo base_url('public/images-home/newteamlogin.png'); ?>" alt="Sunnect Solar">
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
<section id=we-advance-section>
    <div class="contaner">
        <div class=we-advance-inner>
            <h2> <span>WE RECREATE YOUR ONLINE GOODWILL</span></h2>
            <div class=goal>
                <ul>
                    <li class=goal-li><figure><div class='sprite goal'></div><figcaption>Your<br> Goals</figcaption></figure></li>
                    <li class=strategy><figure><div class='sprite strategy'></div><figcaption>Applied<br> Strategy</figcaption></figure></li>
                    <li class=outstanding><figure><div class='sprite outstanding'></div><figcaption>Outstanding<br> Result</figcaption></figure></li>
                </ul>
            </div>
            <div class=we-advance-inner-box>
                <div class="we-advance-inner-box-left wp8">
                    <h4>Implementing ideas and delivering conclusion is what we stand for!</h4>
                </div>
                <div class="we-advance-inner-box-right wp9">
                    <p>
                        <b>TECHMODE</b> is an expertise web & mobile design and development company based in Greater Noida, Delhi NCR, and Bangalore, India. <b>TECHMODE</b> is a full-service provider for corporate or individual interested in designing their marketing, design & development needs of web & mobile based projects. <b>TECHMODE</b> has been eligible to provide the best of services to our clients across the world remaining to our years of experience in digital marketing, website design, web development and mobile development. </p>
                </div>
            </div>
        </div></div>
</section>
<section id=special-services-section>
    <div class=contaner>
        <div class=special-services-section-inner>
            <h2><span>Special Services to Transform Your Business</span></h2>
            <div class="special-services-box cf">
                <ul>
                    <li class=wp6><a href="<?php echo base_url('wordpress-development-services'); ?>"><figure><div class='sprite wordpress-icon'></div><figcaption><h3>Wordpress</h3><h5>Development</h5>
                                </figcaption></figure>
                        </a>
                        <div class="caption8 ctn_blue">
                            <a href="<?php echo base_url('wordpress-development-services'); ?>">
                                <h3>Wordpress</h3>
                                <h5>Development</h5>
                                <p>Utilizing the most prevalent CMS Wordpress, we make 360-degree arrangements as WP sites and blogs. From verified hosting and installation to making an easy to use admin panel, we accomplish results to surpass your desires.</p>
                            </a>
                        </div>
                    </li>
                    <li class="wp5 delay-05s"><a href="<?php echo base_url('magento-development-services'); ?>"> <figure><div class='sprite magento-icon'></div><figcaption><h3>Magento</h3><h5>Development</h5>
                                </figcaption></figure><div class=more><div class='sprite magento-more'></div></div></a>
                        <div class="caption8 ctn_blue_1">
                            <a href="<?php echo base_url('magento-development-services'); ?>"> 
                                <h3>Magento</h3>
                                <h5>Development</h5>
                                <p>Transform your guests straight into clients with our Magento solutions customized for your particular business needs. Sell 24 X 7 with a device friendly online store and have full control of your e-shop.</p>
                            </a>
                        </div>
                    </li>
                    <li class="wp6 delay-1s"><a href="<?php echo base_url('joomla-development-services'); ?>"> <figure><div class='sprite joomla'></div><figcaption><h3>Joomla</h3><h5>Development</h5>
                                </figcaption></figure><div class=more><div class='sprite magento-more'></div></div></a>
                        <div class="caption8 ctn_blue_2">
                            <a href="<?php echo base_url('joomla-development-services'); ?>">
                                <h3>Joomla</h3>
                                <h5>Development</h5>
                                <p>With huge Joomla website architecture and advancement ability, we make a total element rich online element that is 100% customized to target clients. From custom topics and expansions to modules and security, we keep everything unblemished.</p>
                            </a>
                        </div>
                    </li>
                    <li class="wp5 delay-5s"><a href="<?php echo base_url('drupal-development-services'); ?>"> <figure><div class='sprite drupal-icon'></div><figcaption><h3>Drupal</h3><h5>Development</h5>
                                </figcaption></figure><div class=more><div class='sprite dropal-more'></div></div></a>
                        <div class="caption8 ctn_blue_3">
                            <a href="<?php echo base_url('drupal-development-services'); ?>"> 
                                <h3>Drupal</h3>
                                <h5>Development</h5>
                                <p>Taking your business to the next level, we center around focusing on both web and mobile traffic with one responsive website architecture. Our sites highlight wonderful UI with stunningly quick burden times to wow your clients.</p>
                            </a>
                        </div></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section id=digital-marketing-section>
    <div class=contaner>
        <div class=digital-marketing-section-inner>
            <div class="tab-section wp1">
                <div id=tabs>
                    <ul>
                        <li>
                            <a href="#a"><img src="<?php echo base_url('public/images-home/who-we-are.png'); ?>" alt=who-we-are><img class=active src="<?php echo base_url('public/images-home/who-we-are-hover.png'); ?>" alt=who-we-are>Who<br> We Are</a>
                        </li>
                        <li>
                            <a href="#b"><img src="<?php echo base_url('public/images-home/diffrent.png'); ?>" alt=diffrent><img src="<?php echo base_url('public/images-home/diffrent-hover.png'); ?>" class=active alt=diffrent>How We Are Diffrent</a>
                        </li>
                        <li>
                        <!-- <a href="#c"><img src="img/img/serve.png" alt=serve><img src="img/img/serve-hover.png" class=active alt=serve>Who We Serve</a> -->
                        </li>
                    </ul>

                    <div id=a>
                        <h3>Web App, Mobile App Development, Design and Digital Marketing Company India</h3>
                        <p>TECHMODE is a built-up player in the IT advertise, We are an expert in creating sites, web-portal, static and dynamic sites, web-based applications, SEO administrations, Digital marketing, Mobile apps & web-based application that works for the online presence of your business in the world. We have the demonstrated understanding and immaculateness in designing website & web-based services that accomplishment by getting requests, which produces deals and online nearness and are answers for your business.</p>

                    </div>

                    <div id=b>
                        <h3>Deliverables are best</h3>
                        <ul>
                            <li>
                                <p>Basic service delivery isn't sufficient to separate any information technology firm in the present focused competitive sector. Understanding our customer's web needs, as well as their business needs, have pushed TECHMODE past clients' loyalty to reliability. We have set up a notoriety for reliably conveying strategic, in fact, testing ventures under tight courses of events, while likewise giving uncommon client support and backing to our customer base.This, thus, has prompted incredibly positive long haul working relationships with both clients and solution partners alike.</p>


                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class=right-div></div>
</section>
<section id=great-brands-section>
    <div class=contaner>
        <div class=great-brands-section-inner>
            <h2>Some <span>great brands</span> we work with</h2>
        </div>
    </div>
    <section id=testimonials>
        <div class="testimonial-inner wp1">
            <div class=test-box>
                <ul class=bxslider-2>
                    <li><figure><div class='sprite blockquotes'></div></figure>
                        <p> <a href="<?php echo base_url('testimonial-bill'); ?>">TECHMODE India did a <strong>great job</strong> for us. They were very responsive and worked brilliantly to complete our project. As a <strong>technology expert</strong>, I was very demanding of the quality of work we received.</a></p>
                        <span>Mike Brown</span>
                    </li>
                    <li>
                        <figure><div class='sprite blockquotes'></div></figure>
                        <p><a href="<?php echo base_url('testimonial-justyn'); ?>">I was a bit hesitant at first because I am in <strong>Canada and TECHMODE India</strong> was in India but Gopal quickly dispersed any doubt I had. Gopal and his team <strong>were quick</strong>, courteous and very efficient.</a></p>
                        <span>Chris Perry</span>
                    </li>
                    <li><figure><div class='sprite blockquotes'></div></figure>
                        <p><a href="<?php echo base_url('testimonial-drew'); ?>"> TECHMODE India, I would say is a <strong>brilliant team</strong> of creative designers and quirky developers.I was <strong>amazed by the end product</strong>.I highly recommend TECHMODE India</a></p>
                        <span>Johny Bieber</span>
                    </li>
                </ul>
            </div>
        </div>
    </section>
</section>
<section id='blog-section'>
    <div class=contaner>
        <div class=blog-section-inner>
            <h2><span> 7 MAIN REASONS WHY TECHMODE IS BEST</span></h2>

            <font size="5">• Great Design with Proper UX/UI Working.</font>
            <br>
            <font size="5">• Full on page Search Engine Optimization. </font>
            <br>
            <font size="5">• Social media integration so you can make your brand fans on the web. </font>
            <br>
            <font size="5">• Solid web search tools stage for your new brands' store. </font>
            <br>
            <font size="5">• Full responsive Mobile adaptation Website.</font>
            <br>
            <font size="5">• Very solid security with super solid SSL.</font>

        </div>

    </div>
</div>
</div>


</section>
<section id='info-section'>
    <div class='contaner'>
        <div class='info-section-inner'>
            <ul>
                <li><div class='sprite new-phone'></div> +91 92501 34134 </li>
                <li><div class='sprite new-mail'></div><a href="mailto:contact@techmode.in">contact@techmode.in</a></li>

            </ul>
            <div class=get><a class='get-req' href="http://techmode.in/request-Quote">Request Pricing </a></div>
        </div>
    </div>
</section>

<?php require_once(APPPATH . 'views/footer/footer.php'); ?>




