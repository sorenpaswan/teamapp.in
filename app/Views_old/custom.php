<?php require_once(APPPATH . 'views/header/header.php'); ?>   

<META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=UTF-8"><META HTTP-EQUIV="Refresh" CONTENT="0; URL=<?php echo base_url('ecommerce-website-development'); ?>"><TITLE>Page has moved</TITLE>
<meta name="robots" content="index, follow" />
<?php require_once(APPPATH . 'views/header/headerpage.php'); ?>
<div id="wpcont-1-outer">
    <div class="container clearfix">
        <div class="hire-developer e-com">
            <h1>Custom web designing services </h1>
            <p>Every website design should be exclusive and no one size fits all.</p>
            <img src="<?php echo base_url('public/images/custom-web-designing-banner.png'); ?>" alt="custom web designing"> </div>
    </div>
</div>
<!--About Wordpress section-->
<!--section-->
<div id="wpcont3-outer">
    <div class="container clearfix">
        <div class="want-seo asp-pro">
            <h3>Get your website customized according to your business </h3>
            <div class="request-button"> <a href="<?php echo base_url('/request-Quote'); ?>" class="request-btn">Request a Quote</a> 
            </div>   <!--<a href="hire-website-designerf294.html?p=designer">Hire Dedicated Designer </a></div>-->
        </div>
    </div>
</div>
</div>
<!--section-->
<div id="wpcont4-outer" class="clearfix">
    <div class="container clearfix">
        <div class="asp-left-box"> <img src="<?php echo base_url('public/images/user-centric-websites-customized.png'); ?>" alt="customized and usercentric websites"> </div>
        <div class="asp-right-box">
            <h4>User focused Websites Customized to Your Business</h4>
            <p>We are specialized in designing intutive user focused websites customized to your business goals & targeted niche. </p>
            <p>Our creative experts of <a href="#">web designing experts</a> aims to combine technical proficiency with creativity to create something extraordinary. We understand websites make businesses, so should be planned as per your business objectives and customer's psyche. So, we design websites that 'appealing' and 'sells' too.</p>
            <p>At TECHMODE India, we do extensive Requirement analysis before starting any web design project, so that you get as and what has been desired. We not just deliver a web design service but deliver 100% satisfied customized web design that will be an focused industry example and outdo your competitors. </p>
            <p>As an established <a href="#">Website Design India</a> Center, we have more than 5 years of deep experience in designing custom websites for each and every industry sector including Corporate, Retail, E-commerce, Health, Travel, IT, Entertainment, Squeeze pages, Education, etc.</p>
        </div>
    </div>
    <div class="container flex-bdr clearfix">
        <div class="asp-right-box">
            <h4>Custom website designs: what we offer at TECHMODE India</h4>
            <p>Websites besides having a unique look and feel should also be Usable, user friendly, business centric and search engine friendly too. We concentrate on designing interactive & marketing websites at minimum prices in the industry. Its more than just a design, at TECHMODE India creativity and usability go hand in hand.</p>
            <div class="sign-box">
                <h5>Our custom web designs include:</h5>
                <ul>
                    <li>Business focused visual </li>

                    <li>Best color schemes as per your business</li>
                    <li>Industry based theme</li>
                    <li>Ongoing support, updates & revisions </li>
                    <li>W3C validation</li>
                    <li>Fast loading for the best user experience</li>
                    <li>Search engine Optimization</li>
                    <li>Clean & user friendly navigation</li>
                    <li>Web 2.0 standard</li>

                    <li>Cost effective solutions &ndash; we deliver best quote</li>
                </ul>
            </div>
        </div>
        <div class="asp-left-box asp-float-panel"> <img src="<?php echo base_url('public/images/corporate-web-design-services-offer.jpg'); ?>" alt="corporate web design services"> </div>
        <div class="clear"></div>
        <p>We love what we do !! Get a chance custom web design or website redesign services to us and see the gap in the impact your website makes on your end users, partners & competitors.</p>
    </div>
    <div class="container flex-bdr clearfix">
        <div class="asp-left-box"> <img src="<?php echo base_url('public/images/outsource-web-design-services.png'); ?>" alt="tailored web design services"> </div>
        <div class="asp-right-box asp-float-panel">
            <h4>Outsource web design services: Turn visitors into end user</h4>
            <p>Not many website design company offer the level of custom website design service that we assure to offer. We have a team of experts web designers who work with you to initially study your business needs, give you practical advice and guidance, and then deliver the best solutions as per your needs. Perhaps during our analyzing process, we run through few questions to fully understand your company and target customers as well. We then bring together a website redesign proposal which usually includes-</p>
            <div class="sign-box col_new1 clm1">
                <ul>
                    <li> Wireframe layout of the new look.</li>
                    <li> Create the user journey route that you want your online visitors to take while surfing your website </li>
                    <li>A list of services that you never thought you would ever need, but is important to get a success. </li>
                </ul>
            </div>
        </div>
        <div class="clear"></div>
        <p>Our web designing is the branding method for your business and can be considered as the most important step in being successful online. Generally, custom web design solutions differ a lot in terms of complexity and functionality and so we offer a valuable solution which is very flexible yet competitive as per your individual requirements. </p>
    </div>
</div>
<!--section-->
<!--section-->
<div id="corapte-section"  class="clearfix flex-bdr">
    <div class="container clearfix">
        <div class="mid-contant-box">
            <h2>Why Choose TECHMODE India</h2>
            <div class="wpcont5left good-reasons">
                <div class="sign-box col_new1 clm1">
                    <ul>
                        <li> Manpower- Our group of website designers are the most intutive and talented people in the industry. Each of our website layouts is guaranteed 100% exclusive for each website we create. At TECHMODE India every custom website designing is based around structured layouts that are independently designed to ensure that no two website designs look similar. </li>
                        <li> Extensive Experience- With our deep experience in analyzing the customer needs and offering brilliant web designs, we as one of the leading professional web design companies even make sure that every single money each client invest in custom website design service is spent properly. </li>
                        <li> Satisfaction Guaranteed- In the past few years we have set new benchmarks and our web design portfolio is the best mirror to prove our excellence. We are a end user driven web design company and ensure every website is custom designed to fit your business. We never stop until our clients are 100% satisfied and are always available for any questions and updating required even after the site is completed. </li>
                    </ul>
                </div>
            </div>
            <div class="wpcont5right good-img"> <img src="<?php echo base_url('public/images/why-choose-tis-india.png'); ?>" alt="why choose TIS India"> </div>
            <div class="clear"></div>
            <p>We offer web design service that primarily aims to turn website visitors into end users. We are one of the few leading web designing companies that enjoys years of extensive experience in this field and know the true potential of having a website that features a one of its kind of look.</p>
            <br>
            <p>So whether you need help getting started with a new website or you need to design your existing website, TECHMODE India could be the best web design company you can ask. </p>
        </div>
    </div>
</div>
<!--section-->
<!--section-->




<div class="clear"></div>





<div style="display: none;" id="slidingDiv" >
    <div class="slidingDiv">
        <div class="toogle-close">
            <a href="#" onClick="showSlidingDiv();
                        return false;"><img src="<?php echo base_url('public/images/close-btn01.png'); ?>" alt="Close button"></a></div>
        <div class="slide-popup-box">
            <h4>We'll call you soon</h4>
            <p>leave your details</p>
            <div class="lets-talk-out"> </div>
            <div class="slide-popup-box-form-main">
                <div class="slide-popup-box-form">



                    <form method="POST" id="lets-talk-frm" action="<?php echo base_url('contactMe'); ?>">

                        <input type="text"  name="first_name" placeholder="First Name:" >
                        <input type="text"  name="last_name" placeholder="Last Name:"  >

                        <input type="text"  name="email" placeholder="Email:" >
                        <input type="text"  name="address" placeholder="Address" >
                        <input type="text"  name="mobile" placeholder="Mobile:" >
                        <input type="hidden" name="slider_unlock" value="02" >
                        <input type="text"  name="description" placeholder="Description" id="description" >
                        <div id="slider_full_1"></div>
                        <input type="submit" id="lets-talk" value="submit" name="submit">
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="letstalk-wrap" class="clearfix">
    <div class="container clearfix">
        <div class="lets-talk-row clearfix">
            <h4>Let's Talk</h4>
            <p>We'd love to answer any questions you may have. Contact us and discuss your business objectives & we will let you know how we can help along with a Free Quote.</p>
            <ul>
                <li><span></span>
                    <figure><img alt="give us call" src="<?php echo base_url('public/images/roundphn.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>

                    <p><small>Give us a call</small>+91 8800535588</p>
                </li>
                <li><span></span>
                    <figure><img alt="write to us" src="<?php echo base_url('public/images/roundmsg.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>
                    <p><small>Write to us</small><a href="mailto:info@techmode.in">info@techmode.in</a></p>
                </li>
                <li><span></span>
                    <figure><img alt="video chat with us" src="<?php echo base_url('public/images/skype.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>
                    <p><small>Video chat with us</small>info_529965</p>
                </li>
                <li>
                    <div class="toogle-part">
                        <a href="#" onClick="showSlidingDiv(); return false;">
                            <span></span>
                            <figure><img alt="leave your details" src="<?php echo base_url('public/images/mobile.png'); ?>"></figure>
                            <div class="ani_bg-2"> </div>
                            <div class="ani_bg"></div>
                            <p><small>Have us call you</small>leave your detail</p>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('public/js/toggle.js'); ?>"></script>
<script>
                            k("#lets-talk-frm").validate({
                                rules: {
                                    name: "required",
                                    email: {
                                        required: true,
                                        email: true
                                    },

                                    //skype: "required",
                                    mobile: {
                                        required: true,
                                        digits: true,
                                        minlength: 7
                                    },
                                    date: "required",
                                    time: "required",

                                },
                                messages: {
                                    name: '',
                                    email: '', skype: '', mobile: '', date: '', time: '', phone: '',
                                },
                            });

</script>
<div id="wpcont9-outer" class="clearfix">
    <div class="container clearfix">    
    </div>

</div>

</body>
</html>



<?php require_once(APPPATH . 'views/footer/footer.php'); ?>


