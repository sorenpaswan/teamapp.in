<?php require_once(APPPATH . 'views/header/header.php'); ?>

        <meta name="Description" content="Hire your own virtual team of full time PHP developers expert in PHP programming, Ajax, eCommerce solutions, custom website & apps development." />
        <meta name="robots" content="index, follow" />
    </head>
    <?php require_once(APPPATH . 'views/header/headerpage.php'); ?>

<div style="" id="slidingDiv">
    <div class="slidingDiv">
        <div class="container  clearfix">
            <div class="contact-part">

                <div class="call-expand">
                    <a href="#">Request For call</a>
                </div>
            </div>
        </div>
        <div class="slide-popup-box">
            <br><br><br>
            <h4>We'll call you soon</h4>
            <p>Request Quote</p>
            <div class="lets-talk-out"> </div>
            <div class="slide-popup-box-form-main">
                <div class="slide-popup-box-form">  
                    <form method="POST" id="lets-talk-frm" action="<?php echo base_url('requestQuoteData'); ?>">						 
                        <input type="text"  name="name" placeholder="Name:" required="">
                        <input type="text"  name="email" placeholder="Email:" required="">
                        <input type="text"  name="bussiness_name" placeholder="Name Of the Bussiness:"  required="">
                        <input type="text"  name="address" placeholder="Address" required="">
                        <input type="text"  name="mobile" placeholder="Mobile:" required="">
                        <input type="hidden" name="slider_unlock" value="02" required="">
                        <input type="text"  name="description" placeholder="Description" id="description" required="">
                        <input type="submit" id="lets-talk" value="submit" name="submit">
                    </form>
                </div>

            </div>
        </div>
    </div>
</div><br><br>
<div class="address-details">
    <div class="address-box hide-acco"><br><br>

        <div id="wpcont9-outer" class="clearfix">
            <div class="container clearfix">
                <img src="<?php echo base_url('public/images/india-flag.jpg'); ?>" alt="USA Flag"> 
                <h2>&nbsp;&nbsp;India Office</h2> <br> <br>
                <p><storng>+91 9667791813(HR)</storng></p> <br> <br>
                <p>+91 92501 34134(Sales)</p> <br> <br>
                <img src="<?php echo base_url('public/images/add-location-icon.png'); ?>" alt="Address1"> <br> <br>
                <p>Delhi/Greater Noida</p> <br> <br>
                <img src="<?php echo base_url('public/images/add-mail-icon.png'); ?>" alt="email id">
                <a href="mailto:info@techmode.in">info@techmode.in</a>

            </div>
        </div>
        </ul>
    </div>
    <div class="address-box show-acco">
    </div>
</div>
</div>
</div>

</div>
</div>
<div class="get-direction">
    <h2> Get Direction </h2>
</div>
<div>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d32608911.13066871!2d49.059202466980196!3d3.899797644067503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cc1617ba81873%3A0xde142c1992aac172!2sTechMode.in!5e0!3m2!1sen!2sin!4v1587382212729!5m2!1sen!2sin"   width="1347" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe> 
</div>
</div>

<script>
$("#contactus").validate({
    rules: {
        name: "required",
        email: {
            required: true,
            email: true
        },
        country: "required",
        //  skype: "required",
        requirement: "required",
        phone: {
            required: true,
            digits: true,
            minlength: 7,
            maxlength: 15
        },
    },
    messages: {
        name: '',
        email: '',
        country: '',
        requirement: '',
        phone: ''
    },
});


</script>


</script>

<script src="<?php echo base_url('public/js/costum-map.js') ;?>" defer></script>


<?php require_once(APPPATH . 'views/footer/footer.php'); ?>