<?php require_once(APPPATH . 'views/header/header.php'); ?>
<meta name="Description" content="Hire your own virtual team of full time PHP developers expert in PHP programming, Ajax, eCommerce solutions, custom website & apps development." />
<meta name="robots" content="index, follow" />
<?php require_once(APPPATH . 'views/header/headerpage.php'); ?>
<div id="wpcont-1-outer">
  <div class="container clearfix">
    <div class="hire-developer e-com">
      <h1>Responsive website design</h1>
      <p>Make your website perform its best on all gadgets </p>
      <img src="<?php echo base_url('public/images/responsive-website-design.png');?>" alt="responsive website design banner"> </div>
  </div>
</div>
<div id="wpcont3-outer">
  <div class="container clearfix">
    <div class="want-seo asp-pro">
      <h3>Large percentage of mobile users say that a seamless experience across all devices is very important. Be Ready for a Multi Device World. </h3>
      <div class="request-seo"> <a href="<?php echo base_url('/request-Quote');?>" class="green-bt">Request a quote</a>
       <!--<a href="hire-website-designerf294.html?p=designer">Hire Dedicated Designer </a>--></div>
    </div>
  </div>
</div>
<div id="wpcont4-outer" class="clearfix">
  <div class="container clearfix">
    <div class="asp-left-box"> <img src="<?php echo base_url('public/images/responsive-website01.png');?>" alt="responsive web design"> </div>
    <div class="asp-right-box">
      <p>Enlarge Your Reach with a responsive web design! The outbreak of electronic gadgetry releases and its use has greatly impressed the website design. As more people go online for longer duration, the use of desktop computer dimnishes. In fact, maximum are using the smart phones to browse the net than their desktop. To address this apparent shift of internet usage, you need to enhance your traditional website with a active website design. </p>
    </div>
  </div>
  <div class="container flex-bdr clearfix">
    <div class="asp-right-box">
      <p>A responsive website has the potential to adapt to the device being used. Most websites were designed & developed
        to be sight on a full screen desktop monitor or laptop. When accessed through smaller screens like that of smart phones, user interface are cut off, forcing the viewers to scroll. This can be annoing, especially to the end users with very little time to spare. The solution to this setback is by designing a website that can be conveniently viewed on small gadgets as it is in wide screen monitors. </p>
    </div>
    <div class="asp-left-box asp-float-panel"> <img src="<?php echo base_url('public/images/responsive01.png');?>" alt="capability to adapt device"> </div>
  </div>
</div>
<div id="wpcont13-outer"  class="clearfix flex-bdr">
  <div class="container clearfix">
    <h2>How Can Responsive Web Design boost Your Business</h2>
    <p>In traditional marketing, for significant bump, businesses would launch promotional campaigns that will reach the maximum number of people. The fundamental stays true to the internet marketing. Designing a website is basically focused at making an online presence. As more people are busy with the net using a variety of gadgets, it is only sensible that you adjust to the movement of internet usage. </p>
    <div class="wpcont5left good-reasons"><br>
      <div class="sign-box col_new1 clm1">
        <h5>Here's how a responsive website design can help your business:</h5>
        <ul>
          <li> <strong>Focused on one website</strong> - Initially, mobile websites were offered to cover Smart phone users. That means that you build a website specifically for small gadgets. This is a different version of the website. The creative Responsive website designers can hand over you with a viewer friendly, easy to navigate website designed </li>
          <li> <strong>Extensive Captured Users</strong> - Mobile users do not have much diligence for hard to use websites, they will just opt for another site that is easier to use. </li>
          <li> <strong>Expand Online Presence</strong> - A well-designed and well-marketed website will not drive with mobile users without a responsive design. </li>
          <li> <strong>No Palagrism</strong> - Building different websites to accommodate different devices could also mean palagrism content. To resolve this, you will need to come up with exclusive sets of content. </li>
          <li> <strong>Lower Costs</strong> - You only need to maintain and support one website that will work for all your end users, effectively cutting down on your operating costs. </li>
        </ul>
      </div>
    </div>
    <div class="wpcont5right good-img"> <img src="<?php echo base_url('public/images/how-can-responsive-web-design.png');?>" alt="how responsive website design can help"> </div>
    <div class="clear"></div>
    <p>Stop wasting your time and money building a website that can't be properly viewed by most of your customers that are on mobile. Get ahead in the competition with a responsive web design; <a href="contact.html">contact us</a> to know how we can help you with this. </p>
  </div>
</div>




<div class="clear"></div>

<div style="display: none;" id="slidingDiv" >
<div class="slidingDiv">
  <div class="toogle-close">
  <a href="#" onClick="showSlidingDiv(); return false;"><img src="<?php echo base_url('public/images/close-btn01.png');?>" alt="Close button"></a></div>
  <div class="slide-popup-box">
  <h4>We'll call you soon</h4>
  <p>leave your details</p>
  <div class="lets-talk-out"> </div>
    <div class="slide-popup-box-form-main">
      <div class="slide-popup-box-form">
      
   

           <form method="POST" id="lets-talk-frm" action="<?php echo base_url('contactMe');?>">
                       
        <input type="text"  name="first_name" placeholder="First Name:" >
              <input type="text"  name="last_name" placeholder="Last Name:"  >

        <input type="text"  name="email" placeholder="Email:" >
        <input type="text"  name="address" placeholder="Address" >
        <input type="text"  name="mobile" placeholder="Mobile:" >
        <input type="hidden" name="slider_unlock" value="02" >
        <input type="text"  name="description" placeholder="Description" id="description" >
          <div id="slider_full_1"></div>
        <input type="submit" id="lets-talk" value="submit" name="submit">
        </form>
        </div>
        
      </div>
    </div>
  </div>
</div>

 <div id="letstalk-wrap" class="clearfix">
    <div class="container clearfix">
      <div class="lets-talk-row clearfix">
       <h4>Let's Talk</h4>
        <p>We'd love to answer any questions you may have. Contact us and discuss your business objectives & we will let you know how we can help along with a Free Quote.</p>
      <ul>
          <li><span></span>
            <figure><img alt="give us call" src="<?php echo base_url('public/images/roundphn.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
      
            <p><small>Give us a call</small>+91 8800535588</p>
          </li>
          <li><span></span>
            <figure><img alt="write to us" src="<?php echo base_url('public/images/roundmsg.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
            <p><small>Write to us</small><a href="mailto:info@techmode.in">info@techmode.in</a></p>
          </li>
          <li><span></span>
            <figure><img alt="video chat with us" src="<?php echo base_url('public/images/skype.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
            <p><small>Video chat with us</small>info_529965</p>
          </li>
          <li>
      <div class="toogle-part">
      <a href="#" onClick="showSlidingDiv(); return false;">
      <span></span>
              <figure><img alt="leave your details" src="<?php echo base_url('public/images/mobile.png');?>"></figure>
              <div class="ani_bg-2"> </div>
              <div class="ani_bg"></div>
              <p><small>Have us call you</small>leave your detail</p>
        </a>
        </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
 <script type="text/javascript" src="<?php echo base_url('public/js/toggle.js');?>"></script>
 <script>
  
  k("#lets-talk-frm").validate({
    rules: {
    name: "required",
    email: {
      required: true,
      email: true
    },
    
    //skype: "required",
    mobile:{
    required: true,
    digits: true,
     minlength: 7
    },
    date: "required",
    time: "required",
  
    /*captcha: {
      required: true,
      remote: {
      url: "portfolio/recaptcha/validate",
      
      }
    } */  
    
    },
    messages:{
      name: '',
      email: '', skype: '', mobile: '', date: '', time: '', phone: '',
    },
    });
  
 </script><!--excited lets talk-->
    <div id="wpcont9-outer" class="clearfix">

<div class="container clearfix">
      <h2>From the blog</h2>
      <p>Keep on track of latest news and updates</p>
      <figure><img src="<?php echo base_url('public/images/divider.png');?>" alt="divider"></figure>

      <div class="wpcont9left">
      <h3>20 Key Elements of Modern Web Design to Follow In 2019 And Beyond</h3>  
      <p>
How Does Design Matter?
Web design is an evolving art. There is an evident difference in the way web pages used to appear once, and how they appear now. They are far more streamlined and user-frien ...<a href="blog/key-elements-of-modern-web-design/index.html">FIND OUT MORE</a></p>
    </div>
      <div class="wpcont9left">
      <h3>Future of UX Design: 4 Path-Breaking Trends Slowly Invading the UX World</h3> 
      <p>

User Experience design has perhaps been latently existent in the market long before the nomenclature ‘UX’ even happened. However, the more technology is advancing; ‘UX’ design is reforming ...<a href="blog/future-of-ux-design/index.html">FIND OUT MORE</a></p>
    </div>
    </div>
    
     </div>
 <!--From the blog section-->

<div id="social-share-wrap">
  <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
    
    <div class="container clearfix">
      <ul>
        <li> <figure><img src="<?php echo base_url('public/images/fb-icon.png');?>" alt="fb icon"></figure>
          <h5><a target="_blank" href="">
            3517+
            </a> </h5>
      <h6>Fans on Facebook</h6>
        </li>
        <li> <figure><img src="<?php echo base_url('public/images/twitter-icon.png');?>"  alt="twitter icon"></figure>
          <h5><a target="" href="" class="" rel="nofollow">
            2777+
            </a> </h5>
      <h6>Followers on Twitter</h6>
        </li>
        <li> <figure><img src="<?php echo base_url('public/images/linkedin-icon.png');?>" alt="linkedin icon"></figure>
          <h5> <a target="_blank" href="">1351+</a></h5>
      <h6>techmode on Linkedin</h6>
        </li>
        <!-- <li> <figure><img src="/images/gplus-icon.png" alt="gplus icon"></figure>
          <h5><a target="_blank" href="https://plus.google.com/+Tisindiaservices/posts">Add tis</a></h5>
          <h6>to your Circle on G+</h6>
        </li> -->
      </ul>
    </div>
  </div>
</div>
 
 <?php require_once(APPPATH . 'views/footer/footer.php'); ?>
