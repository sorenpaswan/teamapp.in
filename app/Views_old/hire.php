
<?php require_once(APPPATH . 'views/header/header.php'); ?>     

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- Google Tag Manager -->
<script>(function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({'gtm.start':
                    new Date().getTime(), event: 'gtm.js'});
        var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
                '../www.googletagmanager.com/gtm5445.html?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-TTWW35S');</script>
<!-- End Google Tag Manager --> 

<title>Hire PHP Developer India - Full Time PHP Programmer</title>
<meta name="Description" content="Hire your own virtual team of full time PHP developers expert in PHP programming, Ajax, eCommerce solutions, custom website & apps development." />
<meta name="robots" content="index, follow" />

<?php require_once(APPPATH . 'views/header/headerpage.php'); ?>

<section id="content">
    <div class="content">
        <!--Content-hire-Start-->
        <div class="content-hire">
            <h1>Hire PHP Developers</h1>
            <h3>Our rich experience in handling projects of varying complexity makes us the top priority of clients</h3>
            <ul>
                <li>Dedicated virtual team of highly skilled PHP programmers </li>
                <li>Vast experience of all major PHP development frameworks, modern tools & latest technologies.</li>
                <li>Custom PHP application development with enterprise features</li>
                <li>Ecommerce applications with shopping cart configuration & payment gateway integration</li>
                <li>CMS Customization services:  <a href="<?php echo base_url('wordpress-development-services'); ?>">Wordpress</a>, <a href="joomla-development-services.html">Joomla</a>, <a href="drupal-development-services.html">Drupal</a></li>
                <li>Custom development on Zend, CakePHP, Symfony, Laravel, CodeIgnitor, Yii & Phalcon</li>
                <li>Agile methodology to complete your projects faster & cost-effectively</li>
                <li>Clean W3C Compliant code 100% free from bugs</li>
                <li>SEO friendly structure for better visibility</li>
                <li>Perfect Module Integration to incorporate complex functionalities</li>
            </ul>
        </div>
        <!--Content-hire-finish-->
        <!--Content-Excited-Start-->
        <div class="content-excited">
            <div class="per-hours">
                <ul>
                    <li class="nobg">Per Hour <span>$15</span></li>
                    <li>Per Month <span>$1800</span></li>
                </ul>
            </div>
            <div class="excited-talk">
                <h2>Interested? Let's Talk</h2>
                <h3><span>We discuss because We Care</span></h3>
                <!--form-main-start-here-->
                <div class="form-main">
                    <!--sucess-massage-start-here-->
                    <div id="sucess-message">
                        <h2>Thank You!</h2>
                        <p>Hi,
                            We'll get back to you very soon. </p>
                    </div>
                    <!--sucess-massage-start-here-->
                    <!--form-start-here-->
                    <div id="hd_form">
                        <form name="contact-form" id="contact-form" method="post" action="https://www.tisindia.com/portfolio/formhandel/phpquote" >
                            <div class="row">
                                <input class="name-icon" type="text"  name="name" id="name" value="" placeholder="Name* :" onfocus="if (this.placeholder == 'Name* :') {
                                                    this.placeholder = '';
                                                } else {
                                                    fun(this)
                                                }" onblur="if (this.placeholder == '') {
                                                            this.placeholder = 'Name* :';
                                                        }" />
                                <input type="hidden" name="tick" value="7e7addba56bd406e3a56e2468ea65d7a">
                            </div>
                            <!--div-start-here-->
                            <div class="row2">
                                <input class="email-icon" type="text"  name="email" id="email" value="" placeholder="Email*" onfocus="if (this.placeholder == 'Email*') {
                                                    this.placeholder = '';
                                                } else {
                                                    fun(this)
                                                }" onblur="if (this.placeholder == '') {
                                                            this.placeholder = 'Email*';
                                                        }" />
                                <!--<input class="email-icon" type="text" name="email"  value="" placeholder="Email* :" onFocus="if (this.placeholder == 'Email* :') {this.placeholder = '';} else{fun(this)}" onBlur="if (this.placeholder == '') {this.placeholder = 'Email* :';}" />-->
                            </div>
                            <!--div-end-here-->
                            <!--div-start-here-->
                            <div class="row3">
                                <select name="aplication">
                                    <option value="">Open Source Application</option>
                                    <option value="">N/A</option>
                                    <option value="Magento">Magento</option>
                                    <option value="Wordpress">Wordpress</option>
                                    <option value="Joomla">Joomla</option>
                                    <option value="Drupal">Drupal</option>
                                    <option value="Zencart" >Zencart</option>
                                    <option value="Oscommerce">Oscommerce</option>
                                    <option value="Opencart">Opencart</option>
                                    <option value="Others">Others </option>
                                </select>
                            </div>
                            <!--div-end-here-->
                            <!--div-start-here-->
                            <div class="row">
                                <select name="framework">
                                    <option value="Framework">Framework</option>
                                    <option value="CakePHP">CakePHP </option>
                                    <option value="CodeIgniter">CodeIgniter </option>
                                    <option value="Zend">Zend</option>
                                    <option value="Yii">Yii</option>
                                    <option value="Phalcon">Phalcon</option>
                                    <option value="Laravel">Laravel</option>
                                    <option value="Symphony">Symphony</option>
                                    <option value="Others">Others </option>
                                </select>
                            </div>
                            <!--div-end-here-->
                            <!--div-start-here-->
                            <div class="row2">
                                <select name="experiance">
                                    <option value="">Experience ( year )</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                    <option>7</option>
                                    <option>8</option>
                                    <option>9</option>
                                    <option>10</option>
                                </select>
                            </div>
                            <!--div-end-here-->
                            <!--div-start-here-->
                            <div class="row4" >
                                <textarea class="mes-icon" name="requirement" placeholder="Please enter your requirement* :" onfocus="if (this.placeholder == 'Please enter your requirement* :') {
                                                    this.placeholder = '';
                                                }" onblur="if (this.placeholder == '') {
                                                            this.placeholder = 'Please enter your requirement* :';
                                                        }" cols="5" rows="10"></textarea>
                                <!--<textarea class="mes-icon" name="requirement">Please enter your requirement</textarea>-->
                            </div>

                            <div id="slider_full"> </div>
                            <div class="row4" style="height: auto;">
                                <input type="text" name="captcha"/>
                                <div id="scc_cap" style=" float:right;">
                                    <div id="scc_cap1" style=" float:left;">
                                        <img id="old_rec" src="<?php echo base_url('public/images/37259551'); ?>" width="130" height="42" style="border:0;" alt=" ">	
                                    </div>
                                    <a href="javascript:;" id="rec">
                                        <img src="<?php echo base_url('public/images/re.png'); ?>">
                                    </a>
                                </div>

                            </div>

                            <!--div-end-here-->
                            <input type="hidden" name="page" value="<?php echo base_url('hire-php-programmer'); ?>" >
                            <!--submit-button-start-here-->
                            <input type="Submit" name="submit" id="php-programmer" value="Let's Discuss">
                            <!--submit-button-end-here-->
                        </form>
                    </div>
                    <!--form-end-here-->
                </div>
                <!--form-main-end-here-->
            </div>
        </div>
        <!--Content-Excited-Finish-->
    </div>
</section>
<!--Content-finish-->
<div class="clear"></div>
<!--phpprogram-start-->
<section id="phpprogram" class="clearfix">
    <div class="phpprogram"> <img src="<?php echo base_url('public/images/men04.png'); ?>" alt="tis-india">
        <h2>Pool of 30+ skilled PHP programmers delivering result oriented solutions</h2>
        <p>We have expert PHP development team to deliver high performance web solutions covering a broad spectrum of business segments. Our core programmers can build highly scalable PHP applications to expand your business globally. We have expertise in requirement analysis, UI designing, CMS customization </p>
        <p> (Magento/Drupal/Joomla/Wordpress) & Rapid Application Development frameworks (Zend, CakePHP, Symfony, Laravel, Yii, Codeigniter, Phalcon). Using this, we develop fully functional & technologically robust bespoke solutions constituting all the features specified by the client. </p>
    </div>
</section>
<!--phpprogram-finish-->
<div class="clear"></div>
<!--technicalskills-start-->
<section id="technical-skills">
    <!--technical-start-->
    <div class="technical">
        <h2>Core Technical Expertise</h2>
        <!--technical-left-start-->
        <div class="technical-left">
            <h4><span>Open Source Development</span></h4>
            <!--ul-start-herer-->
            <ul>
                <!--li-start-herer-->
                <li class="no-padd-left"> <figure><img src="<?php echo base_url('public/images/drupal.jpg'); ?>" alt="Drupal Developer"> <figcaption>Drupal</figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/wordpress.jpg'); ?>" alt="WordPress Developer"> <figcaption>Wordpress</figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/mejento.jpg'); ?>" alt="Magento Developer"> <figcaption>Magento</figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/joomla.jpg'); ?>" alt="Joomla Developer"> <figcaption>Joomla</figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/oscommerce.jpg'); ?>" alt="Oscommerce Developer"> <figcaption>Oscommerce</figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/zen-cart.jpg'); ?>" alt="Zencart Developer"> <figcaption>Zencart</figcaption> </figure> </li>
                <!--li-end-herer-->
            </ul>
            <!--ul-end-herer-->
            <h4><span>Frameworks</span></h4>
            <!--ul-start-herer-->
            <ul>
                <!--li-start-herer-->
                <li class="no-padd-left"> <figure><img src="<?php echo base_url('public/images/zend.jpg'); ?>" alt="Zend Developer"> <figcaption>Zend </figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/cake-php.jpg'); ?>" alt="CakePHP Developer"> <figcaption>CakePHP </figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/codeigihter.jpg'); ?>" alt="Codeigniter Developer"> <figcaption>Codeigniter</figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/symfony.jpg'); ?>" alt="Symfony Developer"> <figcaption>Symfony</figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/yii.jpg'); ?>" alt="Yii Framework Developer"> <figcaption> Yii</figcaption> </figure> </li>
                <!--li-end-herer-->
            </ul>
            <!--ul-end-herer-->
            <h4><span>Browser End</span></h4>
            <!--ul-start-here-->
            <ul>
                <!--li-start-herer-->
                <li class="no-padd-left"> <figure><img src="<?php echo base_url('public/images/jquery.jpg'); ?>" alt=""> <figcaption>Jquery </figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/extjs.jpg');?>" alt=""> <figcaption>ExtJS </figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/sencha-touch.jpg'); ?>" alt=""> <figcaption>Sencha Touch </figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/html-5.jpg'); ?>" alt=""> <figcaption>HTML 5 </figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/css-3.jpg'); ?>" alt=""> <figcaption> CSS3 </figcaption> </figure> </li>
                <!--li-end-herer-->
            </ul>
            <!--ul-end-here-->
        </div>
        <!--technical-left-end-->
        <!--technical-right-start-->
        <div class="technical-right">
            <h4><span>Database </span></h4>
            <!--ul-start-herer-->
            <ul>
                <!--li-start-herer-->
                <li class="no-padd-left"> <figure><img src="<?php echo base_url('public/images/my-sql.jpg'); ?>" alt=""> <figcaption>MySQL</figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/oreacle.jpg'); ?>" alt=""> <figcaption>Oracle</figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/mongodb.jpg'); ?>" alt=""> <figcaption>mongodb</figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/my-sql.jpg'); ?>" alt=""> <figcaption>My SQL 5.x </figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/postgresql.jpg'); ?>" alt=""> <figcaption>PostGreSQL</figcaption> </figure> </li>
                <!--li-end-herer-->
            </ul>
            <!--ul-end-herer-->
            <h4><span>Concepts </span></h4>
            <!--ul-start-herer-->
            <ul>
                <!--li-start-herer-->
                <li class="no-padd-left"> <figure><img src="<?php echo base_url('public/images/oops.jpg'); ?>" alt=""> <figcaption>OOPS </figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/mvc.jpg'); ?>" alt=""> <figcaption>MVC</figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/svn.jpg'); ?>" alt=""> <figcaption>SVN</figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/cms.jpg'); ?>" alt=""> <figcaption>CMS</figcaption> </figure> </li>
                <!--li-end-herer-->
            </ul>
            <!--ul-end-herer-->
            <h4><span>Skills </span></h4>
            <!--ul-start-herer-->
            <ul>
                <!--li-start-herer-->
                <li class="no-padd-left"> <figure><img src="<?php echo base_url('public/images/php-4.jpg'); ?>" alt=""> <figcaption>PHP 4.x </figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li > <figure><img src="<?php echo base_url('public/images/php-5.jpg'); ?>" alt=""> <figcaption>PHP 5.x </figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/ajax.jpg'); ?>" alt=""> <figcaption>Ajax </figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/html.jpg'); ?>" alt=""> <figcaption>HTML </figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/xml.jpg'); ?>" alt=""> <figcaption>XML </figcaption> </figure> </li>
                <!--li-end-herer-->
                <!--li-start-herer-->
                <li> <figure><img src="<?php echo base_url('public/images/css.jpg'); ?>" alt=""> <figcaption> CSS </figcaption> </figure> </li>
                <!--li-end-herer-->
            </ul>
            <!--ul-end-herer-->
        </div>
        <!--technical-right-end-->
    </div>
    <!--technical-end-->
</section>
<!--technicalskills-end-->
<!---strenth-part-start-outer-->
<div id="strenth-part-outer">
    <!---strenth-part-start-->
    <div class="strenth-part">
        <!--box-row-start-->
        <div class="box-row nobdr">
            <!--box-left-start-->
            <div class="box-left">
                <div class="img-box"><img src="<?php echo base_url('public/images/img-1.jpg'); ?>" alt="Key Skills"></div>
                <div class="txt-box">
                    <h4 >Key Strengths</h4>
                    <ul>
                        <li>Well tested & certified developers</li>
                        <li>Quick Project Deployment </li>
                        <li>Diversified experience on multiple projects</li>
                        <li>Proven familiarity with quality procedures</li>
                        <li>High end Data Security & Confidentiality</li>
                        <li>Highly scalable source code</li>
                        <li>Dynamic Applications </li>
                        <li>Technologically robust solutions</li>
                        <li>Successful project execution</li>
                    </ul>
                </div>
            </div>
            <!--box-left-end-->
            <!--box-right-start-->
            <div class="box-right">
                <div class="img-box"><img src="<?php echo base_url('public/images/img-2.jpg'); ?>" alt="Technical Strengths"></div>
                <div class="txt-box">
                    <h4 >Technical Skills</h4>
                    <ul>
                        <li><strong>Open Source Development</strong> Drupal, Wordpress, Magento, Joomla, Oscommerce, Zencart</li>
                        <li><strong>Frameworks </strong> Zend, Cake PHP, Codeigniter, Symfony, Yii, Laravel</li>
                        <li><strong> Browser End </strong> : Jquery, ExtJS, Sencha Touch, HTML5, CSS3</li>
                        <li><strong>Database </strong>: MySQL, Oracle, SQL Server, My SQL 5.x, PostGreSQL</li>
                        <li><strong> Concepts </strong> : OOPS, MVC, SVN, CMS, Agile methodologies</li>
                        <li> <strong> Skills </strong> : PHP 4.x, PHP 5.x, Ajax, HTML, XML, CSS, jQuery etc</li>
                    </ul>
                </div>
            </div>
            <!--box-right-end-->
        </div>
        <!--box-row-end-->
        <!--box-row-1-start-->
        <div class="box-row">
            <!--box-left-start-->
            <div class="box-left">
                <div class="img-box"><img src="<?php echo base_url('public/images/img-3.jpg'); ?>" alt="hire virtual team"></div>
                <div class="txt-box">
                    <h4>10 reasons to hire our virtual team ??</h4>
                    <ul>
                        <li>Save big on Infrastructure & Hiring costs</li>
                        <li>Well tested & certified developers </li>
                        <li>Quality Deliverables within deadlines</li>
                        <li>Focus on business profits</li>
                        <li>Custom <a href="<?php echo base_url('php-web-development-services'); ?>">PHP application development</a> </li>
                        <li>100% Transparent Billing System</li>
                        <li>24 x 7 dedicated support </li>
                        <li>Guaranteed data confidentiality </li>
                        <li>Secure online presence</li>
                        <li>Testing by dedicated QA Team</li>
                    </ul>
                </div>
            </div>
            <!--box-left-end-->
            <!--box-right-start-->
            <div class="box-right"> <img src="<?php echo base_url('public/images/img-4.jpg'); ?>" alt="How it Works"> </div>
            <!--box-right-end-->
        </div>
        <!--box-row-end-->
    </div>
    <!---strenth-part-end-->
</div>
<!---strenth-part-end-outer-->
<!--out-sorces-outer-start-->
<div id="outsource-outer">
    <!--out-sources-part-start-->
    <div class="outsource-part">
        <h4>Why outsource to TIS India</h4>
        <!--ul-start-here-->
        <ul>
            <!--li-start-->
            <li> <figure><img src="<?php echo base_url('public/images/long-cost.jpg'); ?>" alt=""> <figcaption>Long Term <br>
                        Cost Saving</figcaption> </figure> </li>
            <!--li-end-->
            <!--li-start-->
            <li> <figure><img src="<?php echo base_url('public/images/zero-spend.jpg'); ?>" alt=""> <figcaption>Zero Spend on <br>
                        infrastructure</figcaption> </figure> </li>
            <!--li-end-->
            <!--li-start-->
            <li> <figure><img src="<?php echo base_url('public/images/transperent.jpg'); ?>" alt=""> <figcaption>Transparent Billing<br>
                        System</figcaption> </figure> </li>
            <!--li-end-->
            <!--li-start-->
            <li> <figure><img src="<?php echo base_url('public/images/focus.jpg'); ?>" alt=""> <figcaption>Focus on Core<br>
                        Business</figcaption> </figure> </li>
            <!--li-end-->
            <!--li-start-->
            <li> <figure><img src="<?php echo base_url('public/images/scalbilty.jpg'); ?>" alt=""> <figcaption>High-end<br>
                        Scalability</figcaption> </figure> </li>
            <!--li-end-->
            <!--li-start-->
            <li> <figure><img src="<?php echo base_url('public/images/hand-pick.jpg'); ?>" alt=""> <figcaption>Handpicked <br>
                        Experienced Team</figcaption> </figure> </li>
            <!--li-end-->
        </ul>
        <!--ul-end-here-->
    </div>
    <!--out-sources-part-end-->
</div>

<div id="meet-our-team">
    <h2>our latest work</h2>
    <p>Delivered 5000+ successful projects in last 7 years.</p>
</div>
<div id="wpcont5-outer" class="clearfix hire-php-main">
    <div class="com-portfolio clearfix">

        <!--portfolio nav-->
        <div class="portfolio-row hire-php-new">
            <div class="portfolio our_projects portfolioContainer hire-php-new">


            </div>
        </div>
        <!--porfolio row-->
        <div class="clear"></div>
        <div class="portfolio-text-row clearfix">
            <p>We have designed more than <span>5000 websites</span>, we can not publish all here.</p>
            <h6>Please fill the form to relevant Portfolio</h6>
            <a href="#" class="more-portfolio">Click here for more portfolio</a> </div>
    </div>
</div>


<div class="clear"></div>



<div style="display: none;" id="slidingDiv" >
    <div class="slidingDiv">
        <div class="toogle-close">
            <a href="#" onClick="showSlidingDiv();
                            return false;"><img src="<?php echo base_url('public/images/close-btn01.png'); ?>" alt="Close button"></a></div>
        <div class="slide-popup-box">
            <h4>We'll call you soon</h4>
            <p>leave your details</p>
            <div class="lets-talk-out"> </div>
            <div class="slide-popup-box-form-main">
                <div class="slide-popup-box-form">

                    <form id="lets-talk-frm" action="https://www.tisindia.com/portfolio/formhandel/contactusmini" method="post" >
                        <input type="text"  name="name" placeholder="Name:" >
                        <input type="text"  name="email" placeholder="Email:" >
                        <input type="text"  name="skype" placeholder="Skype" >
                        <input type="text"  name="mobile" placeholder="Mobile:" >
                        <input type="hidden" name="slider_unlock" value="02" >
                        <input type="text"  name="date" placeholder="Date:" id="ldate" >
                        <input type="text"  name="time" placeholder="Time:" id="ltime" >
                        <div id="slider_full_1"></div>
                        <input type="submit" id="lets-talk" value="submit" name="submit">
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="letstalk-wrap" class="clearfix">
    <div class="container clearfix">
        <div class="lets-talk-row clearfix">
            <h4>Let's Talk</h4>
            <p>We'd love to answer any questions you may have. Contact us and discuss your business objectives & we will let you know how we can help along with a Free Quote.</p>
            <ul>
                <li><span></span>
                    <figure><img alt="give us call" src="<?php echo base_url('public/images/roundphn.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>

                    <p><small>Give us a call</small>+91 9811747579</p>
                </li>
                <li><span></span>
                    <figure><img alt="write to us" src="<?php echo base_url('public/images/roundmsg.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>
                    <p><small>Write to us</small><a href="mailto:info@tisindia.com">info@tisindia.com</a></p>
                </li>
                <li><span></span>
                    <figure><img alt="video chat with us" src="<?php echo base_url('public/images/skype.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>
                    <p><small>Video chat with us</small>info_529965</p>
                </li>
                <li>
                    <div class="toogle-part">
                        <a href="#" onClick="showSlidingDiv();
                                        return false;">
                            <span></span>
                            <figure><img alt="leave your details" src="<?php echo base_url('public/images/mobile.png'); ?>"></figure>
                            <div class="ani_bg-2"> </div>
                            <div class="ani_bg"></div>
                            <p><small>Have us call you</small>leave your detail</p>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('public/js/toggle.js'); ?>"></script>
<script>

                            k("#lets-talk-frm").validate({
                                rules: {
                                    name: "required",
                                    email: {
                                        required: true,
                                        email: true
                                    },

                                    //skype: "required",
                                    mobile: {
                                        required: true,
                                        digits: true,
                                        minlength: 7
                                    },
                                    date: "required",
                                    time: "required",

                                },
                                messages: {
                                    name: '',
                                    email: '', skype: '', mobile: '', date: '', time: '', phone: '',
                                },
                            });

</script><!--excited lets talk-->
<div id="wpcont9-outer" class="clearfix">

    <div class="container clearfix">
        <h2>From the blog</h2>
        <p>Keep on track of latest news and updates</p>
        <figure><img src="<?php echo base_url('public/images/divider.png'); ?>" alt="divider"></figure>

        <div class="wpcont9left">
            <h3>6 E-Commerce Platforms That Can Make Your Business Fly</h3>	
            <p>

                “If your business is not on the internet, then your business will be out of business.” – Bill Gates.

                These simple words of wisdom are quite enough to set the tone for your E-Commerce vent ...<a href="<?php echo base_url('blog/ecommerce-platforms-that-can-make-business-fly/index'); ?>">FIND OUT MORE</a></p>
        </div>
        <div class="wpcont9left">
            <h3>5 PHP Techniques to Minimize Web Security Vulnerabilities</h3>	
            <p>
                Understanding the security of websites is mostly difficult for beginner developers. The simplest way to explain this school of thought is that a website is neither secure nor insecure. It is actuall ...<a href="<?php echo base_url('blog/web-security-vulnerabilities/index'); ?>">FIND OUT MORE</a></p>
        </div>
    </div>

</div>
<!--From the blog section-->

<div id="social-share-wrap">
    <div class="addthis_toolbox addthis_default_style addthis_32x32_style">

        <div class="container clearfix">
            <ul>
                <li> <figure><img src="<?php echo base_url('public/images/fb-icon.png'); ?>" alt="fb icon"></figure>
                    <h5><a target="_blank" href="https://www.facebook.com/tisdigitalconsulting">
                            3517+
                        </a> </h5>
                    <h6>Fans on Facebook</h6>
                </li>
                <li> <figure><img src="<?php echo base_url('public/images/twitter-icon.png'); ?>"  alt="twitter icon"></figure>
                    <h5><a target="https://twitter.com/TIS_Digital" href="https://twitter.com/TIS_Digital" class="" rel="nofollow">
                            2777+
                        </a> </h5>
                    <h6>Followers on Twitter</h6>
                </li>
                <li> <figure><img src="<?php echo base_url('public/images/linkedin-icon.png'); ?>" alt="linkedin icon"></figure>
                    <h5> <a target="_blank" href="https://www.linkedin.com/company/tis-digital-strategy-partner/">1351+</a></h5>
                    <h6>TIS on Linkedin</h6>
                </li>      
            </ul>
        </div>
    </div>
</div>
<footer>
    <div class="container clearfix">
        <ul class="foot-menu">
            <li><a href="index.html">Home</a></li>
            <li><a href="portfolio.html">portfolio</a></li>
            <li><a href="blog/index.html">Blog</a></li>
            <li><a href="contact.html">Contact Us</a></li>
            <li><a href="testimonial.html">Testimonials</a></li>
            <li><a href="career.html">Career</a></li>
            <li><a href="sitemap.html">Sitemap</a></li>
        </ul>     
        <div class="dmca">
            <script src="<?php echo base_url('public/images.dmca.com/Badges/DMCABadgeHelper.min.js');?>"></script>
            <a rel="nofollow" class="dmca-badge" target="_blank" href="http://www.dmca.com/Protection/Status.aspx?ID=5acc4010-2fe4-4321-a4fb-25cf12ec1bec" title="DMCA.com Protection Status"> <img src="../images.dmca.com/Badges/_dmca_premi_badge_55215.png?ID=5acc4010-2fe4-4321-a4fb-25cf12ec1bec" alt="DMCA.com Protection Status"></a>
            <p>&copy; 2019 TIS India.  All rights reserved.</p></div>
    </div>
</footer>  
</div>






<script>
                                $(document).ready(function ()
                                {
                                    $('#hd_form').on('click', '#rec', function () {
                                        $.post("portfolio/recaptcha/aj", {status: 'ok'}, function (data) {
                                            $('#old_rec').remove();
                                            $('#scc_cap1').html(data);
                                        });
                                    });
                                });
                                $(window).load(function () {
                                    $.post("portfolio/recaptcha/aj", {status: 'ok'}, function (data) {
                                        $('#old_rec').remove();
                                        $('#scc_cap1').html(data);
                                    });
                                    $.ajax({
                                        url: "portfolio/ajax/getProfolioCat/10",
                                        beforeSend: function (xhr) {
                                            //xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
                                        }
                                    })
                                            .done(function (data) {
                                                $('.loader').hide();
                                                $('.portfolioContainer').prepend(data);
                                                $('.ppc').on('click', function (e) {
                                                    $.fn.custombox(this, {
                                                        effect: 'fadein',
                                                        complete: function () {
                                                            $(".zoomer_basic").zoomer({
                                                                marginMax: 10,
                                                                marginMin: 10,
                                                                ///		increment: .1,
                                                            });
                                                            $(".zoomer_basic").zoomer("resize");
                                                        },
                                                        open: function () {
                                                            $(".zoomer_basic").zoomer({
                                                                marginMax: 10,
                                                                marginMin: 10,
                                                                //increment: .1,
                                                            });

                                                        }
                                                        ,
                                                        close: function () {
                                                            $(".zoomer_basic").zoomer("destroy");
                                                        },
                                                    });
                                                    e.preventDefault();
                                                });

                                            });

                                });

</script>
<script>

    $("#contact-form").validate({
        rules: {
            name: "required",
            email: {
                required: true,
                email: true
            },
            //	skype: "required",
            country: "required",
            website: "required",
            requirement: "required",
            phone: {
                required: true,
                digits: true,
                minlength: 7
            },
            captcha: {
                required: true,
                remote: {
                    url: "portfolio/recaptcha/validate",

                }
            }
        },
        messages: {
            name: '',
            email: '',
            requirement: '',
            phone: '',
            website: '',

        },
    });
</script>

<?php require_once(APPPATH . 'views/footer/footer.php'); ?>