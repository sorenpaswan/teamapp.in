<?php require_once(APPPATH . 'views/header/header.php'); ?>
<meta name="Description" content="Hire your own virtual team of full time PHP developers expert in PHP programming, Ajax, eCommerce solutions, custom website & apps development." />
<meta name="robots" content="index, follow" />

<?php require_once(APPPATH . 'views/header/headerpage.php'); ?>
<div id="wpcont-1-outer">
  <div class="container clearfix">
    <div class="wpcont-1-left-seo">
      <h1>PPC Management Services</h1>
      <h5><b>Searching beyond Click-through rates and Cost per Clicks, we analyze the ROI metrics that matters most for your business!!</b></h5>
      <p>The techniques in the PPC advertising space is highly tappable, but online opportunities can be put to great use only by digital professional. We don't build campaigns based on just the black box tools. Instead, we communicate to a sound researched, thoroughly consistent audience with firm focus on bringing more conversions for less spend. Our PPC professional including <B>Google adwords certified experts</B> uses leverage industry leading tools & best platforms to support you at every level - from account management to tracking results, sharing insights, bidding & managing campaigns at scale. </p>
      
    </div>
    <div class="wpcont-1-right">
 <img src="<?php echo base_url('public/images/pay-per-click-banner-image.jpg');?>"  usemap="#Map"  alt="php development services" >
<map name="Map">
  <area shape="rect" coords="15,158,220,225" href="https://www.google.com/partners/?hl=en-US#a_profile;idtf=2846214492" target="_blank">
  <area shape="rect" coords="277,134,384,251" href="" target="_blank">
</map>
<!-- <map name="planetmap">

  <area shape="rect" target="_blank" coords="422,108,274,271" alt="Sun" href="https://bingadsaccreditedpros.testcraft.com/downloads/reports/20160115_HUJI723O/PROD-ENGLISH-BAAP03_BaapCertificate201302_20160115_235747.pdf ">
</map> -->
    </div>
  </div>
</div>
<div id="wpcont-2-outer">
  <div class="container clearfix">
    <div class="wpcont-2 clearfix">
      <h2>Top-performing PPC campaigns require a architecture to achieve success!!</h2>
      
      <div class="development-tab-section">
        <div id="horizontalTab">
          <div class="resp-tabs-container">
            <div class="services-tabcontent" id="conts-8-1">
             <h3 class="head-tab">Paid Search Marketing</h3>
             
              <p>With TECHMODE paid marketing services - Google Adwords Ads, we can boost clarity, drive traffic & generate leads at an amazing performance rate. Abiding in control of your marketing budget, our Google adwords certified experts create effective ads for finer targeting & place them in a phenomenal position to improve CTR. Having experience & proficiency in handling paid advertising campaigns for our clients worldwide, we create, execute & track performing online advertising campaigns aimed at improving revenue with measurable results. </p>
            </div>
            <div class="services-tabcontent hide" id="conts-9-1">
              <h3 class="head-tab">Display Advertising</h3>
              
              <p>Getting a appropriate display advertisement with eye-popping banners, a perfect call to action & finer-targeting can quickly proceed as your most profitable advertising move. Having implemented 100+ successful campaigns for diverse business pillars, our professionals understand the display advertising marketplace in & out. And so they've sophisticated deep relationships with top-tier networks & multiple advertisers throughout businesses of every domain. Our online advertising expertise not only create display campaigns, they also test what works for your brand, shape up the strategy as per the results & constantly weigh up the impact for better re-targeting.</p>
            </div>
            <div class="services-tabcontent hide" id="conts-10-1">
              <h3 class="head-tab">Landing Page Optimization</h3>
              
              <p>We're not just digital marketing experts, we also have a team of intutive web designers & developers that can help you offer performance based landing optimization services that supports your conversions by finding distortion in your landing page & optimizing it for conversions. We create robust tests to repair channels while keeping track of your statistics as well. From A/B testing & new designs to robust tests, analytics & tracking, we've got you obscure.</p>
            </div>
            <div class="services-tabcontent hide" id="conts-11-1">
              <h3 class="head-tab">Social Media Advertising</h3>
              
              <p>With nearly half the world's population surfing online, social media is the final bonanza of marketers. As social media advertising experts, we don't just develop awareness & spread relevant content across social communities, we listen, talk, share & execute value by gaining insight on customer's desires, understanding how they perceive your brand & then grooming your brand's personality. Our social media advertising campaigns on Facebook Marketing, Instagram Marketing & Twitter Marketing etc are brilliantly designed to transform your brand from"talking" to "talk able", grow reliable fan base & thus improve conversions.</p>
            </div>
            <div class="services-tabcontent hide" id="conts-12-1">
              <h3 class="head-tab">Google Remarketing</h3>
              
              <p>90% traffic bounces back from your website without making a transaction. We have a resolution to capture your lost traffic - our sartorial Google Remarketing campaigns can rise up conversion rates by up to 50% by retargeting available visitors. That's because we do valuable retargeting a mix of careful strategy, detailed messaging, technical proficiency & eye-catching ad design. Moreover, we have intelligent tracking systems competent of recording customer's priorities & create highly customized display ads based on deep analysis.</p>
            </div>
            <div class="services-tabcontent hide" id="conts-13-1">
              <h3 class="head-tab">Mobile Ads</h3>
           
              <p>Mobile landscape needs native advertising to provide to the wide mobile audience & fulfil their exclusive enlisting needs. We can monetize your mobile apps with mobile loaded media. Whether you need to regulate app downloads or increase mobile user engagement for your mobile application, we can advertise your business across all mobile platforms through our highly targeted mobile advertising including performance mobile ads, mobile videos, flexible dashboards & real-time analytics using integrated tools.</p>
            </div>
          </div>
          <ul class="resp-tabs-list">
            <li><a href="javascript:;" class="tabLink tab_icon1" id="conts-8"><span>Paid Search Marketing</span></a></li>
            <li><a href="javascript:;" class="tabLink " id="conts-9"><span>Display Advertising</span></a></li>
            <li><a href="javascript:;" class="tabLink " id="conts-10"><span>Landing Page Optimization</span></a></li>
            <li><a href="javascript:;" class="tabLink " id="conts-11"><span>Social Media Advertising</span></a></li>
            <li><a href="javascript:;" class="tabLink " id="conts-12"><span>Google Remarketing</span></a></li>
            <li><a href="javascript:;" class="tabLink " id="conts-13"><span>Mobile Ads</span></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="wpcont3-outer">
  <div class="container clearfix">
    <div class="want-seo">
      <h3>Want to start with PPC Advertising?</h3>
      <div class="request-seo"> <a href="<?php echo base_url('/request-Quote');?> class="green-bt">Request a quote</a></div>
    </div>
  </div>
</div>
<div id="wpcont4-outer">
  <div class="container clearfix">
    <h2>DO the marketing with right people at right place with right platform - that's our PPC strategy</h2>
    <p>Whether you've a limited budget to spend for a specific service or a large amount to implement a multi-channel digital strategy, our certified PPC professional will analyze give right direction to your campaigns and make them succeed online. </p>
    <div class="wpcont5left">
      <div class="sign-box">
        <ul>
         <li>Research on Potential Keywords</li>
          <li>New Campaign set up</li>
          <li>Restructuring existing campaigns</li>
          <li>PPC Bid Management</li>
          <li>PPC Account configuration</li>
          <li>Managing & optimizing campaigns for maximum ROI</li>
          <li>Building remarketing campaigns & measuring impact</li>
          <li>Conversion Tracking through advanced tools</li>
          <li>Targeting people based on category (age, gender, and location)</li>
          <li>Targeting smart phone users with mobile-supportive ads</li>
          <li>Feasible Account Reviews</li>
          <li>Conversion Optimisation & Return On Investment metrics</li>
          <li>Ad Extensions and Innovations like Dynamic Keyword Execution, Site Links, Google Product Centre Integration & Remarketing</li>
          <!-- <li>Captivating Ad Copy and Split Testing</li> -->
        <!--   <li>Executive Reporting</li> -->
        </ul>
      </div>
    </div>
    <div class="wpcont5right">
      <div class="opensource-icons">
        <img src="<?php echo base_url('public/images/pay-per-click-process.png');?>" alt="ppc strategy">
      </div>
    </div>
  </div>
</div>
<div id="wpcont7-outer" class="php-color-bg">
  <div class="container clearfix">
    <div class="wpcont7left">
      <h2>What are the result of our custom PPC advertising campaigns? </h2>
      <ul class="col-left">
        <li><strong>Engaging Traffic</strong>- We create, tune and manage campaigns that consistently fetch quality traffic at considerably lower costs. Our strategy exactly matches to what our clients really need.</li>
        <li><strong>No Wasteful PPC Spending</strong> - We're experts in running campaigns that lower your spend & generate more value by identifying obstacle & reducing losses.</li>
        <li><strong>Higher Returns</strong> - Our campaigns is managed by clear business goals & achieve the highest Return on Investment by being productive, efficient & to the point. </li>
        <li><strong>Fast Project Payback</strong> - Our PPC advertising projects respond quickly, usually within the first month.</li>
      </ul>
    </div>
    <div class="wpcont7left">
      <h2>What are advertisers getting wrong?</h2>
      <ul class="col-left">
        <li>Incompetently converting landing pages </li>
        <li>Irrelevant Ads with wrong keywords</li>
        <li>Not including clear call to action </li>
        <li>Not testing for small variations in website </li>
        <li>Ignoring the differences in CTR each time you tweak your website </li>
        <li>Ignoring best guidelines for improving the ad relevancy & boosting the Ad Rank</li>
      </ul>
    </div>
  </div>
</div>





<div class="clear"></div>
<div style="display: none;" id="slidingDiv" >
<div class="slidingDiv">
  <div class="toogle-close">
  <a href="#" onClick="showSlidingDiv(); return false;"><img src="<?php echo base_url('public/images/close-btn01.png');?>" alt="Close button"></a></div>
  <div class="slide-popup-box">
  <h4>We'll call you soon</h4>
  <p>leave your details</p>
  <div class="lets-talk-out"> </div>
    <div class="slide-popup-box-form-main">
      <div class="slide-popup-box-form">
           <form method="POST" id="lets-talk-frm" action="<?php echo base_url('contactMe');?>">                       
        <input type="text"  name="first_name" placeholder="First Name:" >
              <input type="text"  name="last_name" placeholder="Last Name:"  >

        <input type="text"  name="email" placeholder="Email:" >
        <input type="text"  name="address" placeholder="Address" >
        <input type="text"  name="mobile" placeholder="Mobile:" >
        <input type="hidden" name="slider_unlock" value="02" >
        <input type="text"  name="description" placeholder="Description" id="description" >
          <div id="slider_full_1"></div>
        <input type="submit" id="lets-talk" value="submit" name="submit">
        </form>
        </div>
        
      </div>
    </div>
  </div>
</div>

 <div id="letstalk-wrap" class="clearfix">
    <div class="container clearfix">
      <div class="lets-talk-row clearfix">
       <h4>Let's Talk</h4>
        <p>We'd love to answer any questions you may have. Contact us and discuss your business objectives & we will let you know how we can help along with a Free Quote.</p>
      <ul>
          <li><span></span>
            <figure><img alt="give us call" src="<?php echo base_url('public/images/roundphn.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
      
            <p><small>Give us a call</small>+91 8800535588</p>
          </li>
          <li><span></span>
            <figure><img alt="write to us" src="<?php echo base_url('public/images/roundmsg.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
            <p><small>Write to us</small><a href="mailto:info@techmode.in">info@techmode.in</a></p>
          </li>
          <li><span></span>
            <figure><img alt="video chat with us" src="<?php echo base_url('public/images/skype.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
            <p><small>Video chat with us</small>info_529965</p>
          </li>
          <li>
      <div class="toogle-part">
      <a href="#" onClick="showSlidingDiv(); return false;">
      <span></span>
              <figure><img alt="leave your details" src="<?php echo base_url('public/images/mobile.png');?>"></figure>
              <div class="ani_bg-2"> </div>
              <div class="ani_bg"></div>
              <p><small>Have us call you</small>leave your detail</p>
        </a>
        </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
 
 <script src="<?php echo base_url('public/js/toggle.js');?>" defer></script>
 <script>
  
  k("#lets-talk-frm").validate({
    rules: {
    name: "required",
    email: {
      required: true,
      email: true
    },
    
    //skype: "required",
    mobile:{
    required: true,
    digits: true,
     minlength: 7
    },
    date: "required",
    time: "required",
  
    /*captcha: {
      required: true,
      remote: {
      url: "portfolio/recaptcha/validate",
      
      }
    } */  
    
    },
    messages:{
      name: '',
      email: '', skype: '', mobile: '', date: '', time: '', phone: '',
    },
    });
  
 </script><!--excited lets talk-->
    <div id="wpcont9-outer" class="clearfix">

<div class="container clearfix">
      <h2>From the blog</h2>
      <p>Keep on track of latest news and updates</p>
      <figure><img src="<?php echo base_url('public/images/divider.png');?>" alt="divider"></figure>

      <div class="wpcont9left">
      <h3>9 Prominent Tips You Should Consider For Facebook Retargeting Ads</h3>  
      <p>

In today’s era, where practically every business is online, just having an online presence is not enough. Studies have shown that roughly 96 percent of the visitors on your site are not even rea ...<a href="blog/tips-for-facebook-retargeting-ads/index.html">FIND OUT MORE</a></p>
    </div>
      <div class="wpcont9left">
      <h3>6 Awesome Local PPC Tips You Should Be Implementing Right Away</h3> 
      <p>

Are you trying hard to gain maximum output from your local PPC campaign, but all in vain? Is your PPC campaign not delivering the expected benefits?

Well, you’re not alone! We all have been t ...<a href="<?php echo base_url('blog/6-awesome-local-ppc-marketing-tips/index');?>">FIND OUT MORE</a></p>
    </div>
    </div>
    
     </div>
 <!--From the blog section-->

<div id="social-share-wrap">
  <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
    
    <div class="container clearfix">
      <ul>
        <li> <figure><img src="<?php echo base_url('public/images/fb-icon.png');?>" alt="fb icon"></figure>
          <h5><a target="_blank" href="">
            3517+
            </a> </h5>
      <h6>Fans on Facebook</h6>
        </li>
        <li> <figure><img src="<?php echo base_url('public/images/twitter-icon.png');?>"  alt="twitter icon"></figure>
          <h5><a target="" href="" class="" rel="nofollow">
            2777+
            </a> </h5>
      <h6>Followers on Twitter</h6>
        </li>
        <li> <figure><img src="<?php echo base_url('public/images/linkedin-icon.png');?>" alt="linkedin icon"></figure>
          <h5> <a target="_blank" href="">1351+</a></h5>
      <h6>Techmode on Linkedin</h6>
        </li>
        <!-- <li> <figure><img src="/images/gplus-icon.png" alt="gplus icon"></figure>
          <h5><a target="_blank" href="https://plus.google.com/+Tisindiaservices/posts">Add tis</a></h5>
          <h6>to your Circle on G+</h6>
        </li> -->
      </ul>
    </div>
  </div>
</div>

<?php require_once(APPPATH . 'views/footer/footer.php'); ?>