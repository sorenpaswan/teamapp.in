<?php require_once(APPPATH . 'views/header/header.php'); ?>
<meta name="Description" content="Hire your own virtual team of full time PHP developers expert in PHP programming, Ajax, eCommerce solutions, custom website & apps development." />
<meta name="robots" content="index, follow" />
<?php require_once(APPPATH . 'views/header/headerpage.php'); ?>

<div id="wpcont-1-outer">
  <div class="container clearfix">
    <div class="hire-developer e-com">
      <h1>Graphic Design Services </h1>
      <p><b>Graphic Design is all about bring your message through visuals given form and structure,<br>
        where by visual information is so as to broadcast your unique message and reflect your unique identity.</b></p>
      <img src="<?php echo base_url('public/images/graphic-design-services-banner.png');?>" alt="graphic design services"> </div>
  </div>
</div>
<div id="wpcont3-outer">
  <div class="container clearfix">
    <div class="want-seo asp-pro">
      <h3>Hire a Graphic Designer</h3>
      <div class="request-seo"> <a class="green-bt" href="<?php echo base_url('/request-Quote');?>">Request a quote</a></div>
    </div>
  </div>
</div>
<div id="wpcont4-outer" class="clearfix">
  <div class="container clearfix">
    <div class="asp-left-box"> <img src="<?php echo base_url('public/images/graphic-design-company-india.jpg');?>" alt="graphic design company India"> </div>
    <div class="asp-right-box">
      <p>TECHMODE India employs universally conventional and established visual principles and elements. </p>
      <div  class="sign-box">
        <ul>
          <li>Brand Foundation</li>
          <li>Corporate Presentation </li>
          <li>Visiting Card </li>
          <li>Envelopes</li>
          <li>Package Design</li>
          <li>Letterhead Design </li>
          <li>Logo and Icon Design </li>
          <li>Stationery Design </li>
          <li>CD Cover Design</li>
          <li>Naming & Tag line Writing</li>
          <li>Business Card Design</li>
          <li>Flyers, catalogs, print advertisements</li>
        </ul>
      </div>
    </div>
    <div class="clear"></div>
    <p>The fundamental are space, shape, form, mass, line, texture, pattern, time, light, and color compose the basic vocabulary of visual design. Design & develop fundamentales, such as emphasis, balance,  variety, scale, and unity establish the broader structural form of the composition.</p>
  </div>
</div>
<div class="clearfix flex-bdr">
  <div class="container clearfix">
    <div class="asp-right-box">
      <h4>How our Graphic Designers can help you enhance your visual appearance</h4>
      <div class="sign-box col_new1 clm1">
        <ul>
          <li> Our expert Graphic designers at TECHMODE have unbound ingenuity and professional expertise to bring a graphic design solution tailored to your message. For TECHMODE every graphic requirement comes as an respective challenge and we push ourselves to the limits to design & develop something which belongs to only you. </li>
          <li> Graphics developed for use on the web are precisely
         formatted for the internet and designed for visual impact. These
          graphics for the web/mobile may include photos, Flash animation services,
          illustration, and other forms of media. </li>
          <li> If needed, TEHMODE can also reconfigure your existing print logos for use on the web. We can also develop new graphics to be used in print from an 
current logos used on the web. We deploy technology bundled with creativity to new, create, innovative and conceptual designs that are bound to stand out. </li>
          <li>We make sue that graphics created for the web/mobile are fast loading, are easy to navigate and enhance your web/mobile solutions overall ranking in search engine and directories.</li>
          <li>TECHMODE design team expertise in designing all types of business stationery, Brochures, Letterheads/envelopes, posters, flyers, catalogs, print advertisements and other business media product for both online and offline operation.</li>
          <li>So if you are in need of an phenomenal visual tool to communicate your unique business message or identity, you can trust TECHMODE graphic designer experts to deliver it for you. </li>
        </ul>
      </div>
    </div>
    <div class="wpcont5right good-img"> <img src="<?php echo base_url('public/images/how-our-graphic-designers.jpg');?>" alt="Graphic Designers help to improve visual appearance"> </div>
  </div>
</div>




<div class="clear"></div>





<div style="display: none;" id="slidingDiv" >
<div class="slidingDiv">
  <div class="toogle-close">
  <a href="#" onClick="showSlidingDiv(); return false;"><img src="<?php echo base_url('public/images/close-btn01.png');?>" alt="Close button"></a></div>
  <div class="slide-popup-box">
  <h4>We'll call you soon</h4>
  <p>leave your details</p>
  <div class="lets-talk-out"> </div>
    <div class="slide-popup-box-form-main">
      <div class="slide-popup-box-form">
      
   

           <form method="POST" id="lets-talk-frm" action="<?php echo base_url('contactMe');?>">
                       
        <input type="text"  name="first_name" placeholder="First Name:" >
              <input type="text"  name="last_name" placeholder="Last Name:"  >

        <input type="text"  name="email" placeholder="Email:" >
        <input type="text"  name="address" placeholder="Address" >
        <input type="text"  name="mobile" placeholder="Mobile:" >
        <input type="hidden" name="slider_unlock" value="02" >
        <input type="text"  name="description" placeholder="Description" id="description" >
          <div id="slider_full_1"></div>
        <input type="submit" id="lets-talk" value="submit" name="submit">
        </form>
        </div>        
      </div>
    </div>
  </div>
</div>

 <div id="letstalk-wrap" class="clearfix">
    <div class="container clearfix">
      <div class="lets-talk-row clearfix">
       <h4>Let's Talk</h4>
        <p>We'd love to answer any questions you may have. Contact us and discuss your business objectives & we will let you know how we can help along with a Free Quote.</p>
      <ul>
          <li><span></span>
            <figure><img alt="give us call" src="<?php echo base_url('public/images/roundphn.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
      
            <p><small>Give us a call</small>+91 8800535588</p>
          </li>
          <li><span></span>
            <figure><img alt="write to us" src="<?php echo base_url('public/images/roundmsg.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
            <p><small>Write to us</small><a href="mailto:info@techmode.in">info@techmode.in</a></p>
          </li>
          <li><span></span>
            <figure><img alt="video chat with us" src="<?php echo base_url('public/images/skype.png');?>"></figure>
      <div class="ani_bg-2">
      </div>
            <div class="ani_bg"></div>
            <p><small>Video chat with us</small>info_529965</p>
          </li>
          <li>
      <div class="toogle-part">
      <a href="#" onClick="showSlidingDiv(); return false;">
      <span></span>
              <figure><img alt="leave your details" src="<?php echo base_url('public/images/mobile.png');?>"></figure>
              <div class="ani_bg-2"> </div>
              <div class="ani_bg"></div>
              <p><small>Have us call you</small>leave your detail</p>
        </a>
        </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
 <script type="text/javascript" src="<?php echo base_url('public/js/toggle.js');?>"></script>
 <script>
  
  k("#lets-talk-frm").validate({
    rules: {
    name: "required",
    email: {
      required: true,
      email: true
    },
    
    //skype: "required",
    mobile:{
    required: true,
    digits: true,
     minlength: 7
    },
    date: "required",
    time: "required",
  
    /*captcha: {
      required: true,
      remote: {
      url: "portfolio/recaptcha/validate",
      
      }
    } */  
    
    },
    messages:{
      name: '',
      email: '', skype: '', mobile: '', date: '', time: '', phone: '',
    },
    });
  
 </script>
 
     <?php require_once(APPPATH . 'views/footer/footer.php'); ?>