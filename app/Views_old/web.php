<?php require_once(APPPATH . 'views/header/header.php'); ?>

<html>
    <head>
        <meta name="Description" content="Hire your own virtual team of full time PHP developers expert in PHP programming, Ajax, eCommerce solutions, custom website & apps development." />
        <meta name="robots" content="index, follow" />
    </head>
    <body>

        <div class="your-apps">
            <div class="app-continer">
                <h1>Customer focused solutions built on effective programming concepts</h1>
                <p>The perfect blend of imagination & functionality</p>
                <a href="<?php echo base_url('request-a-proposal6cd3'); ?>?p=developer">Get started Now</a> </div>
        </div>
    </div>
    <div id="wpcont4-outer" class="clearfix">
        <div class="container clearfix">
            <div class="asp-left-box"> <img src="<?php echo base_url('images/front-end-development.jpg'); ?>" alt="front end development"> </div>
            <div class="asp-right-box">
                <h4 class="web">Front-End Development</h4>
                <h5>Convenience, Compatibility, Navigability & Usefulness delivered to leave a strong impression</h5>
                <p>Great web presence needs highly competent front-end implementation. Our team of <a href="<?php echo base_url('hire-php-programmer'); ?>">expert developers</a> can transpose your cutting-edge graphic designs to clean W3C validated mark-up that turns out to be the best possible development solution for our clients.</p>
                <h5>Front-end Development areas we are expert in:</h5>
                <div class="sign-box">
                    <ul>
                        <li>Highly functional interactive websites with user friendly interface & smooth navigation</li>
                        <li>Mobile optimized responsive web development</li>
                        <li>Complex HTML 5 Animations, Gaming & custom interactivity</li>
                        <li>Feature-rich client applications with cross-browser, cross-platform, cross-device functionality</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container flex-bdr clearfix">
            <div class="asp-right-box">
                <h4 class="web">Custom Web Development</h4>
                <h5>Completely customised web solutions for your organization's different web requirements</h5>
                <p>We specialize in executing custom web development projects with 100% accuracy & perfectly matching to your business requirements. Our team is adept at using cutting-edge technologies to customise everything  from the look & feel of front-end to expert back-end programming that results in a highly dynamic, fully functional & interactive web solution.</p>
                <h5>Custom Web Development areas we are expert in:</h5>
                <div class="sign-box">
                    <ul>
                        <li>Community driven sites </li>
                        <li>Complex e-commerce </li>
                        <li>Complex informational sites</li>
                        <li>Multi-functional web portals</li>
                        <li>Membership/Subscription sites </li>
                        <li>Complex web applications</li>
                        <li>Social Networking Applications</li>
                    </ul>
                </div>
            </div>
            <div class="asp-left-box asp-float-panel"> <img src="<?php echo base_url('images/custom-web-development.jpg'); ?>" alt="custom web development"> </div>
        </div>
        <div class="container flex-bdr clearfix">
            <div class="asp-left-box"> <img src="<?php echo base_url('images/managing-your-ecommerce-website.png'); ?>" alt="open source platform"> </div>
            <div class="asp-right-box asp-float-panel">
                <h4 class="web">Open Source Platform Development</h4>
                <h5>Deploying off-the-shelf development solutions to save time & money</h5>
                <p>We offer custom open source development services including content management solutions to ensure inherent flexibility & fast turnaround at an extremely affordable cost. </p>
                <h5>Open Source Platforms we are expert in:</h5>
                <div class="sign-box">
                    <ul>
                        <li><a href="<?php echo base_url('wordpress-development-services'); ?>">Wordpress</a> -- Highly customizable CMS & blogging engine</li>
                        <li><a href="<?php echo base_url('drupal-development-services'); ?>">Drupal</a> -- Feature-rich content management solution</li>
                        <li><a href="<?php echo base_url('joomla-development-services'); ?>">Joomla</a> -- Best content publishing framework</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container flex-bdr clearfix">
            <div class="asp-right-box">
                <h4 class="web">E-commerce development</h4>
                <h5>Perfect e-commerce solutions to give your customers the best shopping experience </h5>
                <p>We build websites that monetize your products in an effective way by giving the biggest market exposure to your business. From a beautiful user interface to easily manageable admin panel; integration of best payment gateway to ensuring highly secure transactions; we promise a 100% effective, fully-featured & flexible <a href="ecommerce-website-development.html">ecommerce solution</a> to expand your customer base.</p>
                <h5>E-commerce Development Areas we are expert in:</h5>
                <div class="sign-box">
                    <ul>
                        <li>Magento - Magento Go, Magento Community, Magento Enterprise</li>
                        <li>Prestashop</li>
                        <li>Shopify</li>
                        <li>3D Cart</li>
                        <li>Bigcommerce</li>
                        <li>Volusion</li>
                        <li>OSCommerce</li>
                        <li>Woocommerce</li>
                        <li>Ubercart</li>
                        <li>ZenCart</li>
                        <li>XCart</li>
                        <li>Opencart</li>
                        <li>Virtuemart</li>
                    </ul>
                </div>
            </div>
            <div class="asp-left-box asp-float-panel"> <img src="<?php echo base_url('images/commerce-web-design.jpg'); ?>" alt="ecommerce development"> </div>
        </div>
        <div class="container flex-bdr clearfix">
            <div class="asp-left-box"> <img src="<?php echo base_url('images/mobile-development.jpg'); ?>" alt="mobile devlopment"> </div>
            <div class="asp-right-box">
                <h4 class="web">Mobile Development</h4>
                <p>With more & more web traffic coming via mobile devices, we offer multiple solutions ranging from <a href="responsive-website-design.html">responsive web design</a> to mobile only templates & native mobile applications. </p>
                <h5>Mobile Development Areas we are expert in:</h5>
                <div class="sign-box">
                    <ul>
                        <li>Responsive Website Development</li>
                        <li>Mobile Specific Website Development</li>
                        <li>Windows Mobile App Development</li>
                        <li>iOS Mobile App Development</li>
                        <li>Blackberry Mobile App Development </li>
                        <li>Android Mobile App Development</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container flex-bdr clearfix">
            <div class="asp-right-box">
                <h4 class="web">Web Application Frameworks</h4>
                <p>We select an appropriate programming framework based on client requirements & develop everything keeping in mind a successful web presence comprising robust architecture, maximum versatility & best scope for future growth. Using object-oriented best practices, comprehensive code base & coding guidelines, our team offers highly customizable solutions to meet the requirements of our clients.</p>
                <h5>Web Application Frameworks we are expert in:</h5>
                <div class="sign-box">
                    <ul>
                        <li>Laravel PHP Framework</li>
                        <li>Phalcon PHP Framework</li>
                        <li>Symfony PHP Framework</li>
                        <li>CodeIgniter PHP Framework</li>
                        <li>Zend PHP Framework</li>
                        <li>.NET MVC Framework</li>
                        <li>MS Silverlight</li>
                        <li>MS Sharepoint</li>
                    </ul>
                </div>
            </div>
            <div class="asp-left-box asp-float-panel"> <img src="<?php echo base_url('images/web-application-frameworks.jpg'); ?>" alt="web application frameworks"> </div>
        </div>
    </div>
    <div id="wpcont7-outer" class="nopadding-top">
        <div class="container flex-bdr clearfix">
            <div class="wpcont7left">
                <h2>Why to opt for our Web Development Services</h2>
                <ul class="col-left">
                    <li>Technically strong team of 100+ domain experts carrying 10+ yrs of experience</li>
                    <li>Adherence to best coding guidelines & quality standards</li>
                    <li>Clean W3C validated code compatible with multiple platforms & browsers</li>
                    <li>Highly maintainable website structure</li>
                    <li>Responsive user-friendly interfaces</li>
                    <li>Performance, load & stress testing before delivery</li>
                    <li>Up-to-date techniques implemented using latest technologies </li>
                    <li>Completely SEO optimized designs to boost rankings</li>
                    <li>Considerably less maintenance work </li>
                    <li>High-end security against malicious practices</li>
                </ul>
            </div>
            <div class="wpcont7left">
                <h2>What would you get</h2>
                <ul class="col-left">
                    <li>Fully Functional & highly usable web applications matching to your business needs, requirements & expectations</li>
                    <li>Full control & complete transparency of the development process backed with seamless communication</li>
                    <li>Smart & Open application architecture allowing for high solution productivity & scalability</li>
                    <li>High application maintainability enabling to reduce the total cost of ownership</li>
                    <li>Mature QA procedures throughout the entire software development life cycle </li>
                    <li>Deployment, Stabilization & Ongoing support & maintenance of the delivered application</li>
                </ul>
            </div>
        </div>
    </div>




    <div class="clear"></div>

    <script>
        var k = jQuery.noConflict();

        k(document).ready(function () {
            var options = {
                target: '#lets-talk-out', // target element(s) to be updated with server response 
            };
            k('.bxslider').bxSlider({
                minSlides: 2,
                maxSlides: 5,
                slideWidth: 200,
                slideMargin: 10
            });
            k('#ltime').datetimepicker({
                datepicker: false,
                format: 'H:i'
            });
            k('#ldate').datetimepicker({
                timepicker: false,
                format: 'Y/m/d',
                minDate: '-1970/01/01'
            });
            // bind form using 'ajaxForm' 
            k('#lets-talk-frm').ajaxForm({success: function (responseText, statusText, xhr, $form) {
                    k('.slide-popup-box-form-main').prepend('<h4>' + responseText + '</h4>');
                    k('.slide-popup-box-form').hide();

                    //alert(responseText);
                    //showSlidingDiv();
                    //document.getElementById("lets-talk-out").html(responseText);
                    //	k('#lets-talk-out').html(responseText);
                },
            });
        });
        k("#lets-talk-frm").validate({
            rules: {
                name: "required",
                email: {
                    required: true,
                    email: true
                },

                //skype: "required",
                mobile: {
                    required: true,
                    digits: true,
                    minlength: 7
                },
                date: "required",
                time: "required",

                /*captcha: {
                 required: true,
                 remote: {
                 url: "portfolio/recaptcha/validate",
             
                 }
                 } */

            },
            messages: {
                name: '',
                email: '', skype: '', mobile: '', date: '', time: '', phone: '',
            },
        });
        k('#slider_full_1').sliderCaptcha({
            type: "filled",
            textFeedbackAnimation: 'swipe_overlap',
            hintText: "Swipe to submit",
            width: '300px',
            height: '55px',
            styles: {
                knobColor: "#72ba1c",
                knobColorAfterUnlock: "#000000",
                backgroundColor: "#444",
                textColor: "#fff",
                textColorAfterUnlock: "#fff"
            },
            face: {
                top: 0,
                right: 9,
                icon: 'right-thin',
                textColor: '#ddd',
                textColorAfterUnlock: '#72ba1c',
                topAfterUnlock: 0,
                rightAfterUnlock: 9,
                iconAfterUnlock: 'flag'
            },
            events: {
                submitAfterUnlock: 0,
                validateOnServer: 1,
                validateOnServerParamName: "slider_unlock"
            }
        });
        var $ = jQuery.noConflict();

    </script>


    <div style="display: none;" id="slidingDiv" >
        <div class="slidingDiv">
            <div class="toogle-close">
                <a href="#" onClick="showSlidingDiv();
                return false;"><img src="<?php echo base_url('images/close-btn01.png'); ?>" alt="Close button"></a></div>
            <div class="slide-popup-box">
                <h4>We'll call you soon</h4>
                <p>leave your details</p>
                <div class="lets-talk-out"> </div>
                <div class="slide-popup-box-form-main">
                    <div class="slide-popup-box-form">

                        <form id="lets-talk-frm" action="https://www.tisindia.com/portfolio/formhandel/contactusmini" method="post" >
                            <input type="text"  name="name" placeholder="Name:" >
                            <input type="text"  name="email" placeholder="Email:" >
                            <input type="text"  name="skype" placeholder="Skype" >
                            <input type="text"  name="mobile" placeholder="Mobile:" >
                            <input type="hidden" name="slider_unlock" value="02" >
                            <input type="text"  name="date" placeholder="Date:" id="ldate" >
                            <input type="text"  name="time" placeholder="Time:" id="ltime" >
                            <div id="slider_full_1"></div>
                            <input type="submit" id="lets-talk" value="submit" name="submit">
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div id="letstalk-wrap" class="clearfix">
        <div class="container clearfix">
            <div class="lets-talk-row clearfix">
                <h4>Let's Talk</h4>
                <p>We'd love to answer any questions you may have. Contact us and discuss your business objectives & we will let you know how we can help along with a Free Quote.</p>
                <ul>
                    <li><span></span>
                        <figure><img alt="give us call" src="<?php echo base_url('images/roundphn.png'); ?>"></figure>
                        <div class="ani_bg-2">
                        </div>
                        <div class="ani_bg"></div>

                        <p><small>Give us a call</small>+91 9811747579</p>
                    </li>
                    <li><span></span>
                        <figure><img alt="write to us" src="<?php echo base_url('images/roundmsg.png'); ?>"></figure>
                        <div class="ani_bg-2">
                        </div>
                        <div class="ani_bg"></div>
                        <p><small>Write to us</small><a href="mailto:info@tisindia.com">info@tisindia.com</a></p>
                    </li>
                    <li><span></span>
                        <figure><img alt="video chat with us" src="<?php echo base_url('images/skype.png'); ?>"></figure>
                        <div class="ani_bg-2">
                        </div>
                        <div class="ani_bg"></div>
                        <p><small>Video chat with us</small>info_529965</p>
                    </li>
                    <li>
                        <div class="toogle-part">
                            <a href="#" onClick="showSlidingDiv();
                                return false;">
                                <span></span>
                                <figure><img alt="leave your details" src="<?php echo base_url('images/mobile.png'); ?>"></figure>
                                <div class="ani_bg-2"> </div>
                                <div class="ani_bg"></div>
                                <p><small>Have us call you</small>leave your detail</p>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <script>

        k("#lets-talk-frm").validate({
            rules: {
                name: "required",
                email: {
                    required: true,
                    email: true
                },

                //skype: "required",
                mobile: {
                    required: true,
                    digits: true,
                    minlength: 7
                },
                date: "required",
                time: "required",

                /*captcha: {
                 required: true,
                 remote: {
                 url: "portfolio/recaptcha/validate",
              
                 }
                 } */

            },
            messages: {
                name: '',
                email: '', skype: '', mobile: '', date: '', time: '', phone: '',
            },
        });

    </script><!--excited lets talk-->
    <div id="wpcont9-outer" class="clearfix">

        <div class="container clearfix">
            <h2>From the blog</h2>
            <p>Keep on track of latest news and updates</p>
            <figure><img src="<?php echo base_url('images/divider.png'); ?>" alt="divider"></figure>

            <div class="wpcont9left">
                <h3>Content-First Design-3 Steps To Effectively Implement It</h3>	
                <p>

                    Do you think that having a responsive and elegant website design is all that you need to enhance your visitor’s experience? Well, yes, but partially! Without the right content to flaunt your des ...<a href="blog/content-first-design/index.html">FIND OUT MORE</a></p>
            </div>
            <div class="wpcont9left">
                <h3>12 Essential Web Design Trends of 2017 To Watch Out For</h3>	
                <p>

                    2016 was the year that web design disciples the world over proved themselves to be the singular champions of free thought in design. Slack’s outstanding UX propelled the startup to unicorn statu ...<a href="blog/web-design-trends/index.html">FIND OUT MORE</a></p>
            </div>
        </div>

    </div>
    <!--From the blog section-->

    <div id="social-share-wrap">
        <div class="addthis_toolbox addthis_default_style addthis_32x32_style">

            <div class="container clearfix">
                <ul>
                    <li> <figure><img src="<?php echo base_url('images/fb-icon.png'); ?>" alt="fb icon"></figure>
                        <h5><a target="_blank" href="https://www.facebook.com/tisdigitalconsulting">
                                3517+
                            </a> </h5>
                        <h6>Fans on Facebook</h6>
                    </li>
                    <li> <figure><img src="<?php echo base_url('images/twitter-icon.png'); ?>"  alt="twitter icon"></figure>
                        <h5><a target="https://twitter.com/TIS_Digital" href="https://twitter.com/TIS_Digital" class="" rel="nofollow">
                                2777+
                            </a> </h5>
                        <h6>Followers on Twitter</h6>
                    </li>
                    <li> <figure><img src="<?php echo base_url('images/linkedin-icon.png'); ?>" alt="linkedin icon"></figure>
                        <h5> <a target="_blank" href="https://www.linkedin.com/company/tis-digital-strategy-partner/">1351+</a></h5>
                        <h6>TIS on Linkedin</h6>
                    </li>
                    <!-- <li> <figure><img src="/images/gplus-icon.png" alt="gplus icon"></figure>
                      <h5><a target="_blank" href="https://plus.google.com/+Tisindiaservices/posts">Add tis</a></h5>
                      <h6>to your Circle on G+</h6>
                    </li> -->
                </ul>
            </div>
        </div>
    </div>

</body>
</html>
<?php require_once(APPPATH . 'views/footer/footer.php'); ?>
