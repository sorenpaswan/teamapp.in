
<?php require_once(APPPATH . 'views/header/header.php'); ?>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- Google Tag Manager -->
<script>(function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({'gtm.start':
                    new Date().getTime(), event: 'gtm.js'});
        var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
                '../www.googletagmanager.com/gtm5445.html?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-TTWW35S');</script>
<!-- End Google Tag Manager --> 

<link href="images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,400italic,700italic,800italic' rel='stylesheet' type='text/css'>
<meta name="description" content="TIS India is one of the leading Magento web development company in India, offering custom-built, secured, SEO optimized, mobile friendly Magento stores." />
<meta name="robots" content="index, follow" />
<?php require_once(APPPATH . 'views/header/headerpage.php'); ?>

<div id="wpcont-1-outer">
    <div class="container clearfix">
        <div class="wpcont-1-left">
            <h1>Magento Development Company</h1>
            <p>Our custom ecommerce solutions for Magento Go, Enterprise & Community edition give you 360<sup>o</sup> control over the looks, functionality & content of your online store. </p>
            <ul>
                <li>Take control of your ecommerce business through a custom-built, secure, SEO optimized mobile-friendly shop.</li>
                <li>Custom Magento stores with Magento template customization</li>
                <li>Safe Magento extensions development </li>
                <li>Usability focused SEO friendly architecture</li>
            </ul>
            <div class="request-button"> <a href="<?php echo base_url('request-a-proposal'); ?>" class="request-btn">Request a Quote</a> <a href="<?php echo base_url('#wpcont6-outer'); ?>">Our Portfolio</a> </div>
        </div>
        <div class="wpcont-1-right"><img src="<?php echo base_url('public/images/magento.png'); ?>" alt="magento development services"></div>
    </div>
</div>
<div id="wpcont-2-outer">
    <div class="container clearfix">
        <div class="wpcont-2 clearfix">
            <h2>Magento Ecommerce Development Services</h2>
            <p><span>"The Highest standards...the happiest Clients" - Those who have put their trust in us have generated million dollar sales from their Magento stores." <br>
                    <br>
                    Our expert Magento development team comprises highly experienced Magento certified developers who cater to the biggest brands in the US ecommerce market. We develop high performance user friendly online stores that earn visibility & yield profitability you dreamt of. From Magento installation & theme customization to module integration & plugin development; complete automation or overall optimization, we can handle any type of small or large projects with utmost ease to deliver smart ecommerce solutions.</span> </p>
            <div class="wordpress-tab-box">
                <div id="horizontalTab">
                    <ul class="resp-tabs-list">
                        <li><a href="javascript:;" class="tabLink tab_icon1" id="conts-1"><span>Complete Magento Customization</span></a></li>
                        <li><a href="javascript:;" class="tabLink " id="conts-2"><span>Theme Design & Template Integration Services</span></a></li>
                        <li><a href="javascript:;" class="tabLink " id="conts-3"><span>Magento SEO</span></a></li>
                        <li><a href="javascript:;" class="tabLink " id="conts-4"><span>Magento Extensions Development</span></a></li>
                        <li><a href="javascript:;" class="tabLink " id="conts-5"><span>Magento Store Maintenance & Support</span></a></li>
                        <li><a href="javascript:;" class="tabLink " id="conts-6"><span>PSD to Magento Theme Conversion</span></a></li>
                        <li><a href="javascript:;" class="tabLink " id="conts-7"><span>Magento Performance Optimization</span></a></li>
                    </ul>
                    <div class="clear"></div>
                    <div class="resp-tabs-container">
                        <div class="services-tabcontent" id="conts-1-1">
                            <div class="theme1_box"> <img src="<?php echo base_url('public/images/complete-magento-customization.png'); ?>" alt="complete magento customization">
                                <h3>Complete Magento Customization </h3>
                                <p>We don't just give you a mere online store but a full fledged selling machine integrated with payment solutions, order management system, shipping modules & SEO friendly architecture. Our development team deliver high-end Magento development services by offering total customization of your online store  from top quality store designs & customised development to API integration & traffic acquisition;  we have hands-on expertise in everything. </p>
                            </div>
                        </div>
                        <div class="services-tabcontent hide" id="conts-2-1">
                            <div class="theme2_box"> <img src="<?php echo base_url('public/images/theme-design-template.png'); ?>" alt="theme design template">
                                <h3>Theme Design & Template Integration Services</h3>
                                <p>We create fresh Magento themes or integrate pre-built Magento templates in your online store after optimizing them to yield best conversion rates. Our experts deliver a completely dynamic & interactive online store with right combination of exclusive features. </p>
                            </div>
                        </div>
                        <div class="services-tabcontent hide" id="conts-3-1">
                            <div class="theme3_box"> <img src="<?php echo base_url('public/images/magento-seo.png'); ?>" alt="magento seo">
                                <h3>Magento SEO</h3>
                                <p>Our experts give you measurable results by inviting quality traffic to your online store through well structured SEO campaigns tailored to specific business needs. We accurately assess the competitive landscape to give you a full SEO friendly online store that converts and pays due to its out-of-the-box optimization. </p>
                            </div>
                        </div>
                        <div class="services-tabcontent hide" id="conts-4-1">
                            <div class="theme4_box"> <img src="<?php echo base_url('public/images/magento-extensions-development.png'); ?>" alt="magento extension development">
                                <h3>Magento Extensions Development</h3>
                                <p>We can develop custom extensions to highlight the unique features of your Magento store. Whether you want an easier way to organize your product list, sort the menu by preferences, let your customers pick their own shipping dates, set reminders for wishlists or something else, you just ask for the functionality you need and we will get that in your store. </p>
                            </div>
                        </div>
                        <div class="services-tabcontent hide" id="conts-5-1">
                            <div class="theme5_box"> <img src="<?php echo base_url('public/images/magento-store-maintenance-support.png'); ?>" alt="magento store maintenance support">
                                <h3>Magento Store Maintenance & Support </h3>
                                <p>We keep your online store up & running by monitoring it 24 X 7. Our expert technical team offers top class maintenance & support services to troubleshoot any kind of frontend or backend issues, security hacks, implement version upgrades, code reviews, enhance performance, install new extensions, fix bugs, take daily off-site backups & provide monthly site analysis with best recommendations.</p>
                            </div>
                        </div>
                        <div class="services-tabcontent hide" id="conts-6-1">
                            <div class="theme6_box"> <img src="<?php echo base_url('public/images/psd-to-magento.png'); ?>" alt="psd to magento">
                                <h3>PSD to Magento Theme Conversion</h3>
                                <p>We convert your PSDs to highly functional pixel perfect Magento themes with clean HTML code that complies to W3C standards. Our themes are cross bowser compatible (chrome, mozilla, IE, and all mobile OS) & look good on multiple devices (desktops, tablets, mobiles) due to their responsive design and flexible UI. </p>
                            </div>
                        </div>
                        <div class="services-tabcontent hide" id="conts-7-1">
                            <div class="theme7_box"> <img src="<?php echo base_url('public/images/magento-performance.png'); ?>" alt="magento performance">
                                <h3>Magento Performance Optimization</h3>
                                <p>We tune your Magento store to handle high traffic in peak shopping hours. Not just this, we can sync new features to display 1000s of products within nanoseconds & deliver 99.9% uptime. For this, our expert developers optimize your store for SEO friendliness, page load speed, configure & install caches like Varnish,  Memcache etc. to maximize conversions & serve your visitors an exclusively amazing shopping experience. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="wpcont3-outer">
    <div class="container clearfix">
        <h3>Want to have a Magento Ecommerce Website</h3>
        <div class="request-seo"><a href="<?php echo base_url('hire-magento-developer'); ?>">Hire Magento Developer</a></div>
    </div>
</div>
<div id="wpcont4-outer">
    <div class="container clearfix">
        <h2>10 Exclusive features of a Magento Online Store</h2>
        <div class="wpcont5left">
            <ul>
                <li> <figure><img src="<?php echo base_url('public/images/ability.png'); ?>" alt="ability to upgrade"></figure>
                    <p>Ability to upgrade automatically without losing customization</p>
                </li>
                <li> <figure><img src="<?php echo base_url('public/images/unlimited.png'); ?>" alt="Unlimited product options"></figure>
                    <p>Unlimited product options with customizable options, grouped products, configurable bundles & layered browsing </p>
                </li>
                <li> <figure><img src="<?php echo base_url('images/customer.png'); ?>" alt="Customer assisted shopping"></figure>
                    <p>Customer assisted shopping with product comparisons</p>
                </li>
                <li> <figure><img src="<?php echo base_url('public/images/reviews.png'); ?>" alt="reviews and ratings"></figure>
                    <p>Reviews & Ratings ; multiple Wish lists etc for more customer engagement</p>
                </li>
                <li> <figure><img src="<?php echo base_url('public/images/retention.png'); ?>" alt="Retention & Loyalty Building programs"></figure>
                    <p>Retention & Loyalty Building programs</p>
                </li>
                <li> <figure><img src="<?php echo base_url('public/images/comes.png'); ?>" alt="Comes bundled with"></figure>
                    <p>Comes bundled with features like flexible coupons, gift cards, banners & store credits.</p>
                </li>
                <li> <figure><img src="<?php echo base_url('public/images/dynamic.png'); ?>" alt="Dynamic rule based product relations"></figure>
                    <p>Dynamic rule based product relations for up-sells, cross-sells & related products</p>
                </li>
                <li> <figure><img src="<?php echo base_url('public/images/automated.png');?>" alt="Automated Email-marketing reminder"></figure>
                    <p>Automated Email-marketing reminder for abandoned shopping carts & wish lists</p>
                </li>
                <li> <figure><img src="<?php echo base_url('public/images/restrict.png'); ?>" alt="Restrict catalog to specific customers"></figure>
                    <p>Restrict catalog to specific customers & create invitations for limited or inside sales</p>
                </li>
                <li> <figure><img src="<?php echo base_url('public/images/backup.png'); ?>" alt="Backup & rollback options"></figure>
                    <p>Backup & rollback options for testing new modules, performing customizations or transferring data. For e.g. system backup, database backup & media backup</p>
                </li>
            </ul>
        </div>
        <div class="wpcont5right">
            <ul>
                <li>15+<span>Magento developers</span></li>
                <li>50+<span>Websites developed</span></li>
                <li>300+<span>Bugs fixed</span></li>
                <li>100+<span>Custom Themes and Plugins created so far</span></li>
            </ul>
        </div>
    </div>
</div>
<div id="wpcont5-outer" class="clearfix">
    <div class="container clearfix">
        <h2>Why Magento is preferred over other ecommerce solutions </h2>
        <div class="cont4-left col_new1">
            <!--Wpsite-box-end-->
            <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/multisite-control.png'); ?>" alt="multisite control">
                <h3>Multisite Control from a single admin interface</h3>
                <p>One can set up a highly customized store in Magento by easily creating multiple shops & operating them under the same admin panel without any hassle. </p>
            </div>
            <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/single-page.png'); ?>" alt="single pack checkout">
                <h3>Single Page Checkout & Multiple ship-to addresses </h3>
                <p>With a Magento store, customers can shop for your products in just 3 simple steps & checkout as an anonymous guest even without signing in. Also, one order can be shipped to multiple addresses. </p>
            </div>
            <!--Wpsite-box-end-->
            <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/built-in-seo.png'); ?>" alt="build in seo">
                <h3>Built-in SEO & Advanced  Marketing Promotional Tools</h3>
                <p>Magento allows your store to rank higher in search engine results with powerful SEO. Not just this, the advanced built-in marketing tools add core functionalities within the ecommerce websites & thus boost up online sales. </p>
            </div>
            <!--Wpsite-box-end-->
        </div>
        <div class="cont4-right col_new1">
            <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/internationalization.png'); ?>" alt="internationalization">
                <h3>Internationalization Support</h3>
                <p>Magento has built-in support for localization, multiple currencies and tax rates. Plus, it has a configurable list of allowed countries for certain features including European Union VAT-ID validation, EU cookie notification, etc</p>
            </div>
            <!--Wpsite-box-end-->
            <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/device-compatible.png'); ?>" alt="device compatible">
                <h3>Device compatible applications with Magento Connect </h3>
                <p>Magento Connect is the biggest ecommerce application marketplace wherein all the apps developed on Magento application can be accessed flawlessly via multiple devices including desktops, tablets or cell phones. </p>
            </div>
            <!--Wpsite-box-end-->
            <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/enhanced-customer.png'); ?>" alt="device compatibles">
                <h3>Enhanced Customer Engagement with Magento Mobile Commerce</h3>
                <p>Magento automatically detects mobile browsers & accordingly displays the mobile optimized version of your website for better user experience on small screens as well. This boosts customer engagement to a great extent.</p>
            </div>
        </div>
    </div>
</div>
<div id="wpcont6-outer" class="clearfix">
    <div class="container clearfix">
        <h2>Our Magento Website Development Portfolio </h2>
        <p>Our clients love our work. Browse through some of our latest sample of websites developed on Magento.</p>
        <ul id="protfilio-list">
            <img class="loader" src="<?php echo base_url('public/images/loader.gif'); ?>" style="opacity:0.7;align:center;"/>
            <li class="view-portfolio-btn"><a href="<?php echo base_url('portfolio'); ?>">View other portfolio <img src="<?php echo base_url('public/images/arrow02.png'); ?>" alt="Tis India"></a></li>
        </ul>
    </div>
</div>
<div id="wpcont7-outer">
    <div class="container clearfix">
        <div class="wpcont7left col_new1">
            <h2>How our Magento development services make a difference</h2>
            <div class="Wpsite-box">
                <h3>We give you full control</h3>
                <p>With our Magento development services, you can control every facet of your store from merchandising to promotions on your own without any hassle.</p>
            </div>
            <div class="Wpsite-box">
                <h3>We increase your Return on spend & Return on relationship </h3>
                <p>Our Magento ecommerce business model reaps profits through a fully featured product catalog & exciting shopping cart functionalities that build a long term relationship with your customers.</p>
            </div>
            <div class="Wpsite-box">
                <h3>We improve your brand reputation by evolving its growth</h3>
                <p>We enable merchants to increase their brand value by managing their reputation amongst the target customers & right integration of online marketing channels.</p>
            </div>
            <div class="Wpsite-box">
                <h3>We deliver everything mobile for superb shopping experience</h3>
                <p>Being mobile friendly is our habit. Whatever we design & develop, our technical developers make sure to offer the best user experience on every screen may it be your PC, tablet or smartphone.</p>
            </div>
            <div class="Wpsite-box">
                <h3>Well be there 24 x 7 when you need support</h3>
                <p>In case your store breaks down in the odd hours, you can always count on us for dedicated technical support or any kind of assistance that helps you keep it up & running again. </p>
            </div>

        </div>
        <div class="wpcont7left col_new1">

            <h2>Magento Enterprise</h2>
            <ul class="col-left">
                <li>Targets mid-large size businesses.</li>
                <li>Licensed but paid Magento ecommerce solution </li>
                <li>Suited for both B2B & B2C businesses with full capabilities to drive sales & growth.</li>
                <li>Comes pre-packed with enterprise class features</li>
            </ul>
            <h2>Magento Community </h2>
            <ul class="col-left">
                <li>Targets developers & techsavvy merchants </li>
                <li>Licensed open source Magento ecommerce solution </li>
                <li>Most suited for B2C & requires 3rd party add-ons for B2B</li>
                <li>One can modify the code & even contribute to it for support & guidance</li>
            </ul>
        </div>
    </div>
</div>




<div class="clear"></div> 



<div style="display: none;" id="slidingDiv" >
    <div class="slidingDiv">
        <div class="toogle-close">
            <a href="#" onClick="showSlidingDiv();
                    return false;"><img src="<?php echo base_url('public/images/close-btn01.png'); ?>" alt="Close button"></a></div>
        <div class="slide-popup-box">
            <h4>We'll call you soon</h4>
            <p>leave your details</p>
            <div class="lets-talk-out"> </div>
            <div class="slide-popup-box-form-main">
                <div class="slide-popup-box-form">

                    <form id="lets-talk-frm" action="https://www.tisindia.com/portfolio/formhandel/contactusmini" method="post" >
                        <input type="text"  name="name" placeholder="Name:" >
                        <input type="text"  name="email" placeholder="Email:" >
                        <input type="text"  name="skype" placeholder="Skype" >
                        <input type="text"  name="mobile" placeholder="Mobile:" >
                        <input type="hidden" name="slider_unlock" value="02" >
                        <input type="text"  name="date" placeholder="Date:" id="ldate" >
                        <input type="text"  name="time" placeholder="Time:" id="ltime" >
                        <div id="slider_full_1"></div>
                        <input type="submit" id="lets-talk" value="submit" name="submit">
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="letstalk-wrap" class="clearfix">
    <div class="container clearfix">
        <div class="lets-talk-row clearfix">
            <h4>Let's Talk</h4>
            <p>We'd love to answer any questions you may have. Contact us and discuss your business objectives & we will let you know how we can help along with a Free Quote.</p>
            <ul>
                <li><span></span>
                    <figure><img alt="give us call" src="<?php echo base_url('public/images/roundphn.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>

                    <p><small>Give us a call</small>+91 9811747579</p>
                </li>
                <li><span></span>
                    <figure><img alt="write to us" src="<?php echo base_url('public/images/roundmsg.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>
                    <p><small>Write to us</small><a href="mailto:info@tisindia.com">info@tisindia.com</a></p>
                </li>
                <li><span></span>
                    <figure><img alt="video chat with us" src="<?php echo base_url('public/images/skype.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>
                    <p><small>Video chat with us</small>info_529965</p>
                </li>
                <li>
                    <div class="toogle-part">
                        <a href="#" onClick="showSlidingDiv();
                                return false;">
                            <span></span>
                            <figure><img alt="leave your details" src="<?php echo base_url('public/images/mobile.png'); ?>"></figure>
                            <div class="ani_bg-2"> </div>
                            <div class="ani_bg"></div>
                            <p><small>Have us call you</small>leave your detail</p>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<script>

    k("#lets-talk-frm").validate({
        rules: {
            name: "required",
            email: {
                required: true,
                email: true
            },

            //skype: "required",
            mobile: {
                required: true,
                digits: true,
                minlength: 7
            },
            date: "required",
            time: "required",

            /*captcha: {
             required: true,
             remote: {
             url: "portfolio/recaptcha/validate",
             
             }
             } */

        },
        messages: {
            name: '',
            email: '', skype: '', mobile: '', date: '', time: '', phone: '',
        },
    });

</script><!--excited lets talk-->
<div id="wpcont9-outer" class="clearfix">

    <div class="container clearfix">
        <h2>From the blog</h2>
        <p>Keep on track of latest news and updates</p>
        <figure><img src="<?php echo base_url('public/images/divider.png'); ?>" alt="divider"></figure>

        <div class="wpcont9left">
            <h3>6 E-Commerce Platforms That Can Make Your Business Fly</h3>	
            <p>

                “If your business is not on the internet, then your business will be out of business.” – Bill Gates.

                These simple words of wisdom are quite enough to set the tone for your E-Commerce vent ...<a href="<?php echo base_url('blog/ecommerce-platforms-that-can-make-business-fly/index'); ?>">FIND OUT MORE</a></p>
        </div>
        <div class="wpcont9left">
            <h3>Magento Web Design: Top 10 Game-Changing Trends to Look Out For</h3>	
            <p>

                Are you one among those who think that e-commerce store features are the only thing which impact the customers and boost conversion? Well, that might be partially true! In order to attract custome ...<a href="<?php echo base_url('blog/magento-web-design-trends-to-look-out-for/index'); ?>">FIND OUT MORE</a></p>
        </div>
    </div>

</div>
<!--From the blog section-->

<div id="social-share-wrap">
    <div class="addthis_toolbox addthis_default_style addthis_32x32_style">

        <div class="container clearfix">
            <ul>
                <li> <figure><img src="<?php echo base_url('public/images/fb-icon.png'); ?>" alt="fb icon"></figure>
                    <h5><a target="_blank" href="https://www.facebook.com/tisdigitalconsulting">
                            3517+
                        </a> </h5>
                    <h6>Fans on Facebook</h6>
                </li>
                <li> <figure><img src="<?php echo base_url('public/images/twitter-icon.png'); ?>"  alt="twitter icon"></figure>
                    <h5><a target="https://twitter.com/TIS_Digital" href="https://twitter.com/TIS_Digital" class="" rel="nofollow">
                            2777+
                        </a> </h5>
                    <h6>Followers on Twitter</h6>
                </li>
                <li> <figure><img src="<?php echo base_url('public/images/linkedin-icon.png'); ?>" alt="linkedin icon"></figure>
                    <h5> <a target="_blank" href="https://www.linkedin.com/company/tis-digital-strategy-partner/">1351+</a></h5>
                    <h6>TIS on Linkedin</h6>
                </li>
                <!-- <li> <figure><img src="/images/gplus-icon.png" alt="gplus icon"></figure>
                  <h5><a target="_blank" href="https://plus.google.com/+Tisindiaservices/posts">Add tis</a></h5>
                  <h6>to your Circle on G+</h6>
                </li> -->
            </ul>
        </div>
    </div>
</div>


<?php require_once(APPPATH . 'views/footer/footer.php'); ?>