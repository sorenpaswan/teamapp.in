<?php require_once(APPPATH . 'views/header/header.php'); ?>
<meta name="Description" content="Hire your own virtual team of full time PHP developers expert in PHP programming, Ajax, eCommerce solutions, custom website & apps development." />
<meta name="robots" content="index, follow" />
<?php require_once(APPPATH . 'views/header/headerpage.php'); ?>

<div id="wpcont-1-outer">
    <div class="container clearfix">
        <div class="wpcont-1-left-seo">
            <h1>Content Writing Services </h1>
            <h5><b>Delivering SEO Friendly, Interactive, Unique and Expert Content Writing Services</b> </h5>
            <p> Is your website's good ranking honeymoon-period over? Did your oponents pushed you back in ranking in just few time? </p>
            <p>Fed up of paying huge sums to SEO and Content Writers at daily intervals for refuelling rankings? Are you searching for Content that serves you for valuable website promotions? </p>
            <p>Here’s a Content Writing Service that is going to resolve all your problems! </p>
            <p>We at TECHMODE India know how to Write Content that can makeup your online presence Effectively and For Longer! Our Content Writing Services not only lift your rank but build your Brand Value in the market.</p>
            <p>TECHMODE India provides Content Writing Service that is completely based on business ethics, market experiences and dedicated research and analysis. Understanding your business objectives, we provide you Custom Content that gives the results you always wished for.</p>
        </div>
        <div class="wpcont-1-right g-search"><img src="<?php echo base_url('public/images/content-writing-banner.png'); ?>" alt="Content Writing Services"></div>
    </div>
</div>
<div id="wpcont3-outer">
    <div class="container clearfix">
        <div class="want-seo">
            <h3>For compelling; valuable content</h3>
            <div class="request-seo"> <a href="<?php echo base_url('/request-Quote'); ?>" class="green-bt">Request a quote</a> </div>
        </div>
    </div>
</div>
<div id="wpcont5-outer" class="considr-coustmazation">
    <div class="container clearfix">
        <h2>How is Content Writing different at TECHMODE India</h2>
        <h5>Search Engines like Google and Bing have got smarter! They hike up the content that is Relevant,
            Valuable and Unique! Feeding the spiders, we create Content with new ideas and concepts!</h5>
        <div class="cont4-left col_new1">
            <!--Wpsite-box-start-->
            <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/perfect-catalyst.png'); ?>">
                <div class="consider">
                    <h3>Perfect Expert for SEO</h3>
                    <p>Our web content writers create the most SEO friendly and readable. With SEO and user-friendly content, we embed perfect SEO tactic and strategies. Consequently, with unique and
                        object oriented SEO tactic and strategy grafted in content, your website ranks highest heights. With an effective SEO Content, your web page is exposed to target end users and other enthusiasts! However, we don't follow after keywords but fill them in adequate quantity! </p>
                </div>
            </div>
            <!--Wpsite-box-end-->
            <!--Wpsite-box-start-->
            <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/quality-analysis.png'); ?>">
                <div class="consider">
                    <h3>Quality Analysis</h3>
                    <p>We provide worthy content! We keep on upgrading our Content Management techniques as per market waves and customer demands. Thence, Our Content is Modern and easy-to-relate to the market trends. </p>
                </div>
            </div>
            <!--Wpsite-box-end-->
            <!--Wpsite-box-start-->
            <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/copywriting-services.png'); ?>">
                <div class="consider">
                    <h3>Professional Copywriting Services</h3>
                    <p>Our experts approach makes your things simpler. Our content writers write as per the clients' wish, satisfying him with each and every text. Website reviewing, rewriting and all other task of writing are provided here! </p>
                </div>
            </div>
            <!--Wpsite-box-end-->
        </div>
        <div class="cont4-right col_new1">
            <!--Wpsite-box-start-->
            <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/interactive-content.png'); ?>">
                <div class="consider">
                    <h3>Interactive Content</h3>
                    <p>We write Content that becomes the voice of the website and perfectly communicates to the users. “Talking Content is better choice for  promotions.”</p>
                </div>
            </div>
            <!--Wpsite-box-end-->
            <!--Wpsite-box-start-->
            <div class="Wpsite-box"> <img src="<?php echo base_url('public/images/appealing.png'); ?>">
                <div class="consider">
                    <h3>Appealing</h3>
                    <p>Originality is the key to attract the visitors.Our content writers write Content that really suits the reader and makes him visit your site again and again. Also, our website content writers access every topic with new angle, creating interest for your offerings and services.</p>
                </div>
            </div>
            <!--Wpsite-box-end-->
            <div class="content-writing-col clearfix">
                <p>TECHMODE India brings to you the most valuable Content Writing Services, perfect for local and international clients! We think globally; therefore our Content manages to execute a long-lasting impressions.</p>
            </div>
        </div>
    </div>
</div>
<div id="wpcont4-outer" class="clearfix">
    <div class="container clearfix">
        <h2>our content writing services include:</h2>
        <div class="wpcont5left good-reasons">
            <div class="sign-box">
                <ul>
                    <li> <strong>Business Writing</strong>- Make your business Professional and Unique </li>
                    <li> <strong>Web content Writing</strong>- Let your website Talk and Communicate </li>
                    <li> <strong>SEO Copywriting</strong>- Optimize and Promote with your words</li>
                    <li> <strong>Article Writing</strong>- Readable for readers..Informative....and much more </li>
                    <li> <strong>Newsletter Writing</strong>- Talk to your clients regulary; say the latest about your business </li>
                    <li> <strong>Ebook Writing</strong>- Bring all your expertise, working and much more into one...... </li>
                    <li> <strong>Blog Writing</strong>- Interesting...Entertaining...Let readers enjoy your style of communication </li>
                    <li> <strong>Technical Writing</strong>- Explain in Informative, Detailed, Easy-to-Interpret ways </li>
                    <li> <strong>Press Release</strong>- What's latest and What's upcoming..tell everyone. </li>
                </ul>
            </div>
        </div>
        <div class="wpcont5right good-images"> <img src="<?php echo base_url('public/images/our-content-writing.png'); ?>" alt="content writing checklist"> </div>
    </div>
</div>
<!--excited lets talk-->




<div class="clear"></div>

<script>
    var k = jQuery.noConflict();

    k(document).ready(function () {
        var options = {
            target: '#lets-talk-out', // target element(s) to be updated with server response 
        };
        k('.bxslider').bxSlider({
            minSlides: 2,
            maxSlides: 5,
            slideWidth: 200,
            slideMargin: 10
        });
        k('#ltime').datetimepicker({
            datepicker: false,
            format: 'H:i'
        });
        k('#ldate').datetimepicker({
            timepicker: false,
            format: 'Y/m/d',
            minDate: '-1970/01/01'
        });
        // bind form using 'ajaxForm' 
        k('#lets-talk-frm').ajaxForm({success: function (responseText, statusText, xhr, $form) {
                k('.slide-popup-box-form-main').prepend('<h4>' + responseText + '</h4>');
                k('.slide-popup-box-form').hide();

                //alert(responseText);
                //showSlidingDiv();
                //document.getElementById("lets-talk-out").html(responseText);
                //  k('#lets-talk-out').html(responseText);
            },
        });
    });
    k("#lets-talk-frm").validate({
        rules: {
            name: "required",
            email: {
                required: true,
                email: true
            },

            //skype: "required",
            mobile: {
                required: true,
                digits: true,
                minlength: 7
            },
            date: "required",
            time: "required",

            /*captcha: {
             required: true,
             remote: {
             url: "portfolio/recaptcha/validate",
             
             }
             } */

        },
        messages: {
            name: '',
            email: '', skype: '', mobile: '', date: '', time: '', phone: '',
        },
    });
    k('#slider_full_1').sliderCaptcha({
        type: "filled",
        textFeedbackAnimation: 'swipe_overlap',
        hintText: "Swipe to submit",
        width: '300px',
        height: '55px',
        styles: {
            knobColor: "#72ba1c",
            knobColorAfterUnlock: "#000000",
            backgroundColor: "#444",
            textColor: "#fff",
            textColorAfterUnlock: "#fff"
        },
        face: {
            top: 0,
            right: 9,
            icon: 'right-thin',
            textColor: '#ddd',
            textColorAfterUnlock: '#72ba1c',
            topAfterUnlock: 0,
            rightAfterUnlock: 9,
            iconAfterUnlock: 'flag'
        },
        events: {
            submitAfterUnlock: 0,
            validateOnServer: 1,
            validateOnServerParamName: "slider_unlock"
        }
    });
    var $ = jQuery.noConflict();

</script>


<div style="display: none;" id="slidingDiv" >
    <div class="slidingDiv">
        <div class="toogle-close">
            <a href="#" onClick="showSlidingDiv();
          return false;"><img src="<?php echo base_url('public/images/close-btn01.png'); ?>" alt="Close button"></a></div>
        <div class="slide-popup-box">
            <h4>We'll call you soon</h4>
            <p>leave your details</p>
            <div class="lets-talk-out"> </div>
            <div class="slide-popup-box-form-main">
                <div class="slide-popup-box-form">



                    <form method="POST" id="lets-talk-frm" action="<?php echo base_url('contactMe'); ?>">

                        <input type="text"  name="first_name" placeholder="First Name:" >
                        <input type="text"  name="last_name" placeholder="Last Name:"  >

                        <input type="text"  name="email" placeholder="Email:" >
                        <input type="text"  name="address" placeholder="Address" >
                        <input type="text"  name="mobile" placeholder="Mobile:" >
                        <input type="hidden" name="slider_unlock" value="02" >
                        <input type="text"  name="description" placeholder="Description" id="description" >
                        <div id="slider_full_1"></div>
                        <input type="submit" id="lets-talk" value="submit" name="submit">
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="letstalk-wrap" class="clearfix">
    <div class="container clearfix">
        <div class="lets-talk-row clearfix">
            <h4>Let's Talk</h4>
            <p>We'd love to answer any questions you may have. Contact us and discuss your business objectives & we will let you know how we can help along with a Free Quote.</p>
            <ul>
                <li><span></span>
                    <figure><img alt="give us call" src="<?php echo base_url('public/images/roundphn.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>

                    <p><small>Give us a call</small>+91 8800535588</p>
                </li>
                <li><span></span>
                    <figure><img alt="write to us" src="<?php echo base_url('public/images/roundmsg.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>
                    <p><small>Write to us</small><a href="mailto:info@techmode.in">info@techmode.in</a></p>
                </li>
                <li><span></span>
                    <figure><img alt="video chat with us" src="<?php echo base_url('public/images/skype.png'); ?>"></figure>
                    <div class="ani_bg-2">
                    </div>
                    <div class="ani_bg"></div>
                    <p><small>Video chat with us</small>info_529965</p>
                </li>
                <li>
                    <div class="toogle-part">
                        <a href="#" onClick="showSlidingDiv(); return false;">
                            <span></span>
                            <figure><img alt="leave your details" src="<?php echo base_url('public/images/mobile.png'); ?>"></figure>
                            <div class="ani_bg-2"> </div>
                            <div class="ani_bg"></div>
                            <p><small>Have us call you</small>leave your detail</p>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>


</body>
</html>
<?php require_once(APPPATH . 'views/footer/footer.php'); ?>