     </head> 
<body>
  <header class="header_newstyle" style="height: auto; top: 10px;">
        <div class="logo">
            <a class="logo-new" href="<?php echo base_url('/');?>">                  
                <img src="<?php echo base_url('public/images/logo/logo.png') ?>" height="50px" width="80px">
                <img alt="Web Design & Development Company" src="<?php echo base_url('public/images/logo/logo.png'); ?>"  height="50px" width="80px" class="sticky-logo">
            </a>  
        </div>        
        <!--logo-->
        <nav id="myslidemenu" class="jqueryslidemenu menu">  <a href="#" class="pull"></a>
            <ul>  
                <li><a class="" href="<?php echo base_url('/'); ?>">Home</a></li>
                <li class="services "><a class="" href="<?php echo base_url('services'); ?>">Services</a>
                    <ul class="submenu">
                        <li>
                            <ol>
                                <li><span>Software</span></li>
                                <!-- //<li><a href="web-development-services.html">Web Development</a></li>// -->
                                <li><a href="<?php echo base_url('php-web-development-service'); ?>">PHP</a></li>
                                <li><a href="<?php echo base_url('wordpress-development-services'); ?>">WordPress</a></li>
                                <!-- <li><a href="joomla-development-services.html">Joomla Development</a></li> -->
                                <li><a href="<?php echo base_url('ecommerce-website-development'); ?>">Ecommerce</a></li>
                                <!--  <li><a href="aspnet-development-services.html">Asp.net Development</a></li>                             
                               <li><a href="interspire-development-customization.html">Interspire Development</a></li> -->
                                <li><a href="<?php echo base_url('iphone-app-development'); ?>">Android App</a></li>
                                <li><a href="http://techmode.in/iosApp">IOS App</a></li> <!---- not available -->
                            </ol>
                        </li>
                        <li>
                            <ol>
                                <li><span>Digital Marketing</span></li>
                                <li><a href="<?php echo base_url('digital-marketing-agency'); ?>">Digital Marketing</a></li>
                                <li><a href="<?php echo base_url('seo-services'); ?>">SEO Services</a></li>
                                <li><a href="<?php echo base_url('pay-per-click-advertising'); ?>">PPC Advertising</a></li> <!---- not available -->
                                <li><a href="<?php echo base_url('email-marketing'); ?>">Email Marketing</a></li>
                                <li><a href="<?php echo base_url('Social-Media-Marketing'); ?>">Social Media Marketing</a></li>
                                <!--  <li><a href="web-analytics.html">Web Analytics</a></li> -->
                                <li><a href="<?php echo base_url('online-reputation-management'); ?>">Reputation Management</a></li>
                                <li><a href="<?php echo base_url('content-writing-services'); ?>">Content Writing</a></li>
                            </ol>
                        </li>
                        <li>
                            <ol>
                                <li><span>Desigining</span></li>
                                <li><a href="<?php echo base_url('Front-End_Design'); ?>">Front End Design</a></li>
                                <li><a href="<?php echo base_url('graphic-design-services'); ?>">Graphic Design</a></li>
                                <li><a href="<?php echo base_url('logo-design-services'); ?>">Logo Design</a></li>
                                <li><a href="<?php echo base_url('custom-web-design-services'); ?>">Custom web design</a></li>
                                <li><a href="<?php echo base_url('Responsive-Design-Application'); ?>">Responsive Design Application</a></li>
                            </ol>
                        </li>
                        <li>
                            <ol>
                                <li><span>Hosting</span></li>
                                <li><a href="https://www.secureserver.net/products/domain-registration?plid=557011"> Domain Registartion</a></li>
                                <li><a href="https://www.secureserver.net/products/website-builder?plid=557011">Website Builder</a></li>
                                <li><a href="https://www.secureserver.net/products/cpanel?plid=557011">Hosting Server</a></li>
                                <li><a href="https://www.secureserver.net/products/website-security?plid=557011">Website Security</a></li>
                                <li><a href="https://www.secureserver.net/products/email-marketing?plid=557011">Email Marketing</a></li>
                                <li><a href="https://www.secureserver.net/products/email?plid=557011">Workspace Email</a></li>
                            </ol>
                        </li>
                        <li>
                            <ol>
                                <li><span>IOT</span></li>
                                <li><a href="Internet of Things iot.html">The Internet of Things</a></li>
                                <li><a href="http://techmode.in/CCTV_Surveillance_system">CCTV surveillance system</a></li>
                                <li><a href="http://techmode.in/biometric">Biometric &amp; Access Control System</a></li>
                                <li><a href="http://techmode.in/video_and_audio">video &amp; Audio System</a></li>
                                <li><a href="http://techmode.in/anti_alarm">Fire Anti Alarm System</a></li>
                                <li><a href="http://techmode.in/gps_trackingsystem">GPS Tracking System</a></li>
                            </ol>
                        </li>  
                        <li class="tech-row">
                            <ol>
                                <li><a href="http://techmode.in/wordpress"><img src="<?php echo base_url('public/images/wp-icon.jpg');?>" alt="WordPress Design & Development"> <span class="tooltip">Wordpress</span> </a></li>
                                <li><a href="#"><img src="<?php echo base_url('public/images/html5-icon.jpg');?>" alt="Html5 Development"> <span class="tooltip">Html5</span> </a></li>
                                <li><a href="#"><img src="<?php echo base_url('public/images/zand-icon.jpg');?>" alt="Zend Application Development"> <span class="tooltip">Zend</span> </a></li>    
                                <li><a href="#"><img src="<?php echo base_url('public/images/icon2.jpg');?>" alt="CodeIgniter Development Services"> <span class="tooltip">CodeIgniter</span> </a></li>

                            </ol>

                        </li>

                    </ul>

                </li>

                <li><a class="" href="<?php echo base_url('about'); ?>">Company</a></li>

                <!-- <li><a class="" href="portfolio.html">portfolio</a></li> -->

                <li><a class="" href="<?php echo base_url('contact'); ?>">Contact Us</a></li>

            </ul>

            <div class="rgt">
              <!-- <span class="request-proposal rfq"><a href="rfq/index.html" target="_blank"><img src="images/request-img.gif" width="55" height="63" alt="">RFQ <img src="img/img/green-arrow.png" alt="Request For Quotation" title="Request For Quotation"></a></span> -->

                <span class="request-proposal"><a href="<?php echo base_url('contact');?>"><img src="<?php echo base_url('public/images/request-img.png');?>" width="55" height="63" alt="Request a Proposal">Get Free Quote<img src="<?php echo base_url('public/img/img/green-arrow.png');?>" alt=""></a></span>

                <span class="request-proposal phon-no"><a href="tel:++91 9250134134"><img src="images/phone-icon1.gif" width="55" height="63" alt=""></a>
                    <img src="<?php echo base_url('public/images/phone-icon.png');?>" alt="" class="phn-ico">
                    <img src="<?php echo base_url('public/img/img/phn-icon.png');?>" alt="">

                    +91 92501 34134  
                </span>

            </div>

        </nav>

        <!--navigation--> 

    </header>
    <script src="<?php echo base_url(); ?>/public/js-home/jquery-1.11.3.min.js"></script>         
    <script src="<?php echo base_url(); ?>/public/js/jquery-migrate-1.2.1.js"></script>
    <script src="<?php echo base_url(); ?>/public/js-home/jquery-ui.js"></script>
    <script src="<?php echo base_url(); ?>/public/js-home/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?php echo base_url(); ?>/public/js-home/jquery.bxslider.js"></script>
    <script>
        $(document).ready(function () {
            //  alert('asd');
            $('.bxslider').bxSlider({
                mode: 'fade',
            });
            $('.bxslider-2').bxSlider({});
            $('.bxslider-3').bxSlider({
                mode: 'fade',
            });
            $('#tabs').tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
        });
    </script>

    <script src="<?php echo base_url(); ?>/public/js/menu.js"></script>
    <script src="<?php echo base_url(); ?>/public/js/waypoints.min.js"></script>
    <script src="<?php echo base_url(); ?>/public/js/navbar2.js"></script>



    <script >
        $(document).ready(function () {
            $(this).scrollTop(0);
        });
    </script>