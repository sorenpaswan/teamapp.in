<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'TechMode') }}</title>

    <!-- Scripts -->
   <script src="{{ asset('public/js/app.js') }}" defer></script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
<!--     <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('public/css/all.css') }}" rel="stylesheet">

<title>PHP Web Development Company India - PHP Programming Services</title>



<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <link href="{{ asset('public/css/normalize.css') }}" rel="stylesheet">
        <link href="{{ asset('public/js/zoomer/jquery.fs.zoomer.css') }}" rel="stylesheet">
            <link href="{{ asset('public/css/main.css') }}" rel="stylesheet">
   <script src="{{ asset('public/js/modernizr-2.6.2.min.js') }}" defer></script>
      <script src="{{ asset('public/js/jquery-1.10.2.min.js') }}" defer></script>
      <script src="{{ asset('public/js/jquery-migrate-1.2.1.js') }}" defer></script>

</head> 
<body>
   <div id="main-wrapper">
  <header>
 

    
    <div class="logo">

<a class="" href="{{ url('/') }}"><img src="public/img/img/techmode-web-logo-final.png" alt="logo"  height="50px" width="50px"></a>

      </div>

    <!--logo-->

    <nav id="myslidemenu" class="jqueryslidemenu menu">  <a href="#" class="pull"></a>

      <ul>  
       <li><a class="" href="{{ url('/') }}">Home



      </a></li>

              <li class="services "><a class="" href="services.html">Services</a>

          <ul class="submenu">

         

            <li>

              <ol>

                <li><span>Software</span></li>

                <!-- //<li><a href="web-development-services.html">Web Development</a></li>// -->

                <li><a href="{{ url('php-web-development-service') }}">PHP </a></li>

                <li><a href="{{url('wordpress')}}">WordPress </a></li>

                <!-- <li><a href="joomla-development-services.html">Joomla Development</a></li> -->

                <li><a href="{{url('e-commerce')}}">Ecommerce</a></li>

               <!--  <li><a href="aspnet-development-services.html">Asp.net Development</a></li>

                <li><a href="interspire-development-customization.html">Interspire Development</a></li> -->
    <li><a href="{{url('androidApp')}}">Android App</a></li>

                <li><a href="{{url('iosApp')}}">IOS App</a></li>

              </ol>

            </li>
        
            <li>

              <ol>

                   <li><span>Digital Marketing</span></li>

        <li><a href="{{url('/digital-marketing')}}">Digital Marketing</a></li>

                <li><a href="{{url('/Seo-Services')}}">SEO Services</a></li>

                <li><a href="{{url('/ppc-advertising')}}">PPC Advertising</a></li>

                <li><a href="{{url('/email-marketing')}}">Email Marketing</a></li>

        <li><a href="{{url('/Social-Media-Marketing')}}">Social Media Marketing</a></li>

       <!--  <li><a href="web-analytics.html">Web Analytics</a></li> -->

                <li><a href="{{url('/Reputation-Management')}}">Reputation Management</a></li>

        <li><a href="{{url('/Content-Writing')}}">Content Writing</a></li>
       

              </ol>

            </li>
              <li>

              <ol>

                <li><span>Desigining</span></li>

                <li><a href="{{url('/Front-End_Design')}}">Front End Design</a></li>

      
                <li><a href="{{url('/Graphic_Design')}}">Graphic Design</a></li>

                <li><a href="{{url('/Logo-Design')}}">Logo Design</a></li>

                <li><a href="{{url('/Custom-Web-Design')}}">Custom web design</a></li>
                <li><a href="{{url('/Responsive-Design-Application')}}">Responsive Design Application</a></li>

                
              </ol>

            </li>



            </li>
            
            <li>

              <ol>

                <li><span>Hosting</span></li>

                <li><a href="https://www.secureserver.net/products/domain-registration?plid=557011"> Domain Registartion</a></li>

      
                <li><a href="https://www.secureserver.net/products/website-builder?plid=557011">Website Builder</a></li>

                <li><a href="https://www.secureserver.net/products/cpanel?plid=557011">Hosting Server</a></li>

                <li><a href="https://www.secureserver.net/products/website-security?plid=557011">Website Security</a></li>
                <li><a href="https://www.secureserver.net/products/email-marketing?plid=557011">Email Marketing</a></li>
                <li><a href="https://www.secureserver.net/products/email?plid=557011">Workspace Email</a></li>

                
              </ol>

            </li>
             <li>

              <ol>

               <li><span>IOT</span></li>


                <li><a href="{{url('/iot')}}">The Internet of Things</a></li>
               
                 <li><a href="{{url('/CCTV_Surveillance_system')}}">CCTV surveillance system</a></li>
                <li><a href="{{url('/biometric')}}">Biometric & Access Control System</a></li>

               <li><a href="{{url('/video_and_audio')}}">video & Audio System</a></li>

              <li><a href="{{url('/anti_alarm')}}">Fire Anti Alarm System</a></li>

             <li><a href="{{url('/gps_trackingsystem')}}">GPS Tracking System</a></li>

              </ol>

            </li>  
            
   

            <li class="tech-row">

              <ol>
   <li><a href="{{ url('wordpress') }}"><img src="public/img/wp-icon.jpg" alt="WordPress Design & Development"> <span class="tooltip">Wordpress</span> </a></li>

                

            

                <li><a href="#"><img src="/public/img/html5-icon.jpg" alt="Html5 Development"> <span class="tooltip">Html5</span> </a></li>

                <li><a href="#"><img src="/public/img/zand-icon.jpg" alt="Zend Application Development"> <span class="tooltip">Zend</span> </a></li>

                <li><a href="#"><img src="/public/img/icon2.jpg" alt="CodeIgniter Development Services"> <span class="tooltip">CodeIgniter</span> </a></li>

              </ol>

            </li>

          </ul>

        </li>

        <li><a class="" href="{{url('/about-us')}}">Company</a></li>


        <li><a class="" href="{{url('/contact')}}">Contact Us</a></li>

      </ul>

      <div class="rgt"><!--<span class="request-proposal req"><a href="rfq/index.html" target="_blank"><img src="public/img/request-img.gif" width="55" height="63" alt="">RFQ <img src="public/img/green-arrow.jpg" alt="Request For Quotation" title="Request For Quotation"></a></span>-->
         <span class="request-proposal"><a href="{{url('/request-Quote')}}"><img src="public/img/request-img.gif" width="55" height="63" alt="">Request Pricing <img src="public/img/green-arrow.jpg" alt="Request a Free Quote"></a></span>

      <span class="request-proposal phon-no"><a href="tel:+92501 34134"><img src="public/img/phone-icon1.gif" width="55" height="63" alt=""></a><img src="public/img/phone-icon.png" alt="Call US">

    +91 92501 34134
    </span>

        </div>

    </nav>

    <!--navigation--> 

  </header>
  
            @yield('content')
     </div>
</body>
</html>

