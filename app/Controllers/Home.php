<?php namespace App\Controllers;

class Home extends BaseController
{
    
    public function index()
    {

        return view('home');
    }

    public function about()
    {

        return view('about');
    }
    public function contact()
    {

        return view('contact');
    }

    public function attendance()
    {
        return view('attendance');
    }

    public function leave()
    {
        return view('leave');
    }


    public function payment_made()
    
    {
        return view('payment_made');
    }

    public function payment_match()
    
    {
        return view('payment_match');
    }
    
    public function assets()
    
    {
        return view('assets');
    }
    
    public function match_invoice()
    
    {
        return view('match_invoice');
    }

    public function team_statement_soa()
    
    {
        return view('team_statement_soa');
    }

    public function petty_cash()
    
    {
        return view('petty_cash');
    }
    
    
    public function support()
    {
        return view('support');
    }

    public function teamchat()
    {
        return view('teamchat');
    }

    public function user_guide()
    {
        return view('user_guide');
    }

    public function files()
    {
        return view('files');
    }
 
    public function targets_and_sales()
    {
        return view('targets_and_sales');
    }

    public function customer_accounts()
    {
        return view('customer_accounts');
    }

    public function opportunities()
    {
        return view('opportunities');
    }

    public function prospects()
    {
        return view('prospects');
    }

    public function leads()
    {
        return view('leads');
    }

    public function cold_calls()
    {
        return view('cold_calls');
    }

    public function campaigns()
    {
        return view('campaigns');
    }
    public function offers_catalogue()
    {
        return view('offers_catalogue');
    }

    public function employee_self_service()
    {
        return view('employee_self_service');
    }

    public function monthly_payroll()
    {
        return view('monthly_payroll');
    }

    public function project_expenses()
    {
        return view('project_expenses');
    }

    public function contract_freelancer_expenses()
    {
        return view('contract_freelancer_expenses');
    }

    public function booking_payments()
    {
        return view('booking_payments');
    }

    public function purchases()
    {
        return view('purchases');
    }

    public function marketing_and_sales()
    {
        return view('marketing_and_sales');
    }


    public function vendor()
    {
        return view('vendor');
    }

    public function performance()
    {
        return view('performance');
    }

    public function project()
    {
        return view('project');
    }

    public function activity()
    {
        return view('activity');
    }

    public function task()
    {
        return view('task');
    }



    public function invoices()
    {
        return view('invoices');
    }

    public function payments_received()
    {
        return view('payments_received');
    }

    public function billable_expenses()
    {
        return view('billable_expenses');
    }
    public function monthly_billable_expenses()
    {
        return view('monthly_billable_expenses');
    }

    public function salary_and_benefits()
    {
        return view('salary_and_benefits');
    }

    public function settings()
    {
        return view('settings');
    }

    public function timesheet()
    {
        return view('timesheet');
    }    
    public function trips()
    {
        return view('trips');
    }
    public function bookings()
    {
        return view('bookings');
    }

    public function talent_recruitment()
    {
        return view('talent_recruitment');
    }
    
    public function managing_payrolls()
    {
        return view('managing_payrolls');
    }
    public function feedbacks()
    {
        return view('feedbacks');
    }


}
