<?php 
namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
} 

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true); 

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

$routes->get('home', 'Home::index');
$routes->get('about', 'Home::about');
$routes->get('/attendance-attendance', 'Home::attendance');
$routes->get('/attendance-leave', 'Home::leave');
$routes->get('/books-payment-made', 'Home::payment_made');
$routes->get('/books-payment-match', 'Home::payment_match');
$routes->get('/books-assets', 'Home::assets');
$routes->get('/books-match-invoice', 'Home::match_invoice');
$routes->get('/books-team-statement-soa', 'Home::team_statement_soa');
$routes->get('/books-petty-cash', 'Home::petty_cash');
$routes->get('/collaborate-support', 'Home::support');
$routes->get('/collaborate-team-chat', 'Home::teamchat');
$routes->get('/collaborate-user-guide', 'Home::user_guide');
$routes->get('/collaborate-files', 'Home::files');
$routes->get('/crm-targets-and-sales', 'Home::targets_and_sales');
$routes->get('/crm-customer-accounts', 'Home::customer_accounts');
$routes->get('/crm-opportunities', 'Home::opportunities');
$routes->get('/crm-prospects', 'Home::prospects');
$routes->get('/crm-leads', 'Home::leads');
$routes->get('/crm-cold-calls', 'Home::cold_calls');
$routes->get('/crm-campaigns', 'Home::campaigns');
$routes->get('/crm-offers-catalogue', 'Home::offers_catalogue');
$routes->get('/employee-self-service', 'Home::employee_self_service');
$routes->get('/expenses-monthly-payroll', 'Home::monthly_payroll');
$routes->get('/expenses-project-expenses', 'Home::project_expenses');
$routes->get('/expenses-contract-freelancer-expenses', 'Home::contract_freelancer_expenses');
$routes->get('/expenses-booking-payments', 'Home::booking_payments');
$routes->get('/expenses-purchases', 'Home::purchases');
$routes->get('/expenses-marketing-and-sales', 'Home::marketing_and_sales');
$routes->get('/expenses-vendor', 'Home::vendor');
$routes->get('/performance', 'Home::performance');
$routes->get('/project_and_activity-project', 'Home::project');
$routes->get('/project_and_activity-activity', 'Home::activity');
$routes->get('/project_and_activity-task', 'Home::task');
$routes->get('/revenue-invoices', 'Home::invoices');
$routes->get('/revenue-payments-received', 'Home::payments_received');
$routes->get('/revenue-billable-expenses', 'Home::billable_expenses');
$routes->get('/revenue-monthly-billable-expenses', 'Home::monthly_billable_expenses');
$routes->get('/salary-and-benefits', 'Home::salary_and_benefits');
$routes->get('/settings', 'Home::settings');
$routes->get('/timesheet', 'Home::timesheet');
$routes->get('/travel-trips', 'Home::trips');
$routes->get('/travel-bookings', 'Home::bookings');
$routes->get('/hcm-talent-recruitment', 'Home::talent_recruitment');
$routes->get('/hcm-managing-payroll', 'Home::managing_payrolls');
$routes->get('/hcm-feedbacks', 'Home::feedbacks');






$routes->get('/employee-self-service', 'Home::employee_self_service');
$routes->get('/expenses', 'Home::expenses');
$routes->get('/performance', 'Home::performance');
$routes->get('/project-and-activity', 'Home::project_and_activity');
$routes->get('/revenue', 'Home::revenue');
$routes->get('/salary-and-benefits', 'Home::salary_and_benefits');
$routes->get('/settings', 'Home::settings');
$routes->get('/timesheet', 'Home::timesheet');
$routes->get('/travel', 'Home::travel');
$routes->get('/expenses-vendors', 'Home::vendor');
$routes->get('/contact.html', 'Home::contact');


/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
