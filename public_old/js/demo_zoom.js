$(function() {
    "use strict";

    $('.ppc').on('click', function ( e ) {
        $.fn.custombox( this, {
            effect: 'fadein',
			complete: function(){
				$(".zoomer_basic").zoomer( { 
										marginMax: 10,
										marginMin: 10,
								///		increment: .1,
										});
				$(".zoomer_basic").zoomer("resize");
			},
			open: function(){
				$(".zoomer_basic").zoomer( { 
										marginMax: 10,
										marginMin: 10,
									//increment: .1,
										});
			
			}
			,
			close : function(){
				$(".zoomer_basic").zoomer("destroy");
			},
        });
        e.preventDefault();
    });
});