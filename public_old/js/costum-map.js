window.onload = function () {  
    var styles = [  
        {  
            featureType: 'water',  
            elementType: 'geometry.fill',  
            stylers: [  
                { color: '#adc9b8' }  
            ]  
        },{  
            featureType: 'landscape.natural',  
            elementType: 'all',  
            stylers: [  
                { hue: '#809f80' },  
                { lightness: -35 }  
            ]  
        }  ,{  
            featureType: 'poi',  
            elementType: 'geometry',  
            stylers: [  
                { hue: '#f9e0b7' },  
                { lightness: 30 }  
            ]  
        },{  
            featureType: 'road',  
            elementType: 'geometry',  
            stylers: [  
                { hue: '#d5c18c' },  
                { lightness: 14 }  
            ]  
        },{  
            featureType: 'road.local',  
            elementType: 'all',  
            stylers: [  
                { hue: '#ffd7a6' },  
                { saturation: 100 },  
                { lightness: -12 }  
            ]  
        }              
    ]; 

    var style_1 = [
        {
            featureType: "road",
            stylers: [
                {
                    visibility: "on"
                }, {
                    color: "#ffffff"
                }
            ]
        }, {
            featureType: "road.arterial",
            stylers: [
                {
                    visibility: "on"
                }, {
                    color: "#fee379"
                }
            ]
        }, {
            featureType: "road.highway",
            stylers: [
                {
                    visibility: "on"
                }, {
                    color: "#fee379"
                }
            ]
        }, {
            featureType: "landscape",
            stylers: [
                {
                    visibility: "on"
                }, {
                    color: "#f3f4f4"
                }
            ]
        }, {  
            featureType: 'water',  
            elementType: 'geometry.fill',  
            stylers: [  
                { color: '#7fc8ed' }  
            ]  
        }, {
            featureType: "poi.park",
            elementType: "geometry.fill",
            stylers: [
                {
                    visibility: "on"
                }, {
                    color: "#bfdfae"
                }
            ]
        }, {
            featureType: "landscape.man_made",
            elementType: "geometry",
            stylers: [
                {
                    weight: 0.9
                }, {
                    visibility: "on"
                }
            ]
        }
    ];

    var myLatlng = new google.maps.LatLng(28.596015,77.327417); 
    
    var options = {  
        mapTypeControlOptions: {  
            mapTypeIds: ['Styled']  
        },  
        center: myLatlng,  
        zoom: 17,  
        scaleControl: true,
        panControl: true,
        zoomControl: true,
        mapTypeId: 'Styled',
        sensor: 'true'  
    };  
    var div = document.getElementById('map');  
    var map = new google.maps.Map(div, options);  
    var styledMapType = new google.maps.StyledMapType(style_1, { name: 'Styled' });  
    map.mapTypes.set('Styled', styledMapType);
    
    var infowindow = new google.maps.InfoWindow({
            content: document.getElementById("infobox"),
            disableAutoPan: false,
            maxWidth: 395,
            //pixelOffset: new google.maps.Size(-140, 0),
            zIndex: null,
            boxStyle: {
                width: 395,
            },
            closeBoxMargin: "12px 4px 2px 2px",
            closeBoxURL: "https://www.google.com/intl/en_us/mapfiles/close.gif",
            infoBoxClearance: new google.maps.Size(1, 1)
    });
    
    var image = {
        url: 'images/map-icon.png',
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(12, 59)
    };
    var shadow = {
        url: 'https://dl.dropboxusercontent.com/u/814783/fiddle/shadow.png',
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(-2, 36)
    };
    var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            icon: image,
            shadow: shadow,
            
    });
    
    google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map,marker);
            map.panTo(myLatlng);
    });
}  

